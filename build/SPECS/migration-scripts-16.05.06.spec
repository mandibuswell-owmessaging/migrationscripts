Summary:        Mail migration scripts
Name:           owm-migration-scripts
Version:        16.05.06
Release:        0
License:        Commercial
Group:          Synchronoss/Migration
prefix:         /home/imail
Requires:       %{name}-language-libs
Requires:       %{name}-files
Requires:       %{name}-emails
Requires:       perl > 5.10
Requires:       python > 2.6
AutoReqProv:	no

%define __jar_repack %{nil}
%global _enable_debug_package 0
%global debug_package %{nil}
%global __os_install_post /usr/lib/rpm/brp-compress %{nil}

%description
Mail migration scripts

%package        files
Summary:        Support files for OWM mail migration scripts
Group:          Synchronoss/Migration
License:        Commercial
AutoReqProv:	no

%description    files
Support files for OWM mail migration scripts.

%package        language-libs
Summary:        Perl and python libraries for OWM mail migration scripts
Group:          Synchronoss/Migration
License:        Commercial
AutoReqProv:    no

%description    language-libs
Perl and python libraries for OWM mail migration scripts.

%package        emails
Summary:        Final and welcome email messages
Group:          Synchronoss/Migration
License:        Commercial
AutoReqProv:    no

%description    emails
Final and welcome email messages.

%prep

git status > /dev/null 2>&1
if [ $? -ne 0 ]; then
    set +x # turn off sh tracing so the error is easier to read
    echo 'Need to be run from inside a checked out copy of the source code.'
    echo '"git status" returned an error for this directory'
    echo ''
    git status
    exit 1
fi

BRANCH_NAME=`git name-rev --name-only HEAD`
if [ "x${BRANCH_NAME}" != "x${GIT_BRANCH}" ]; then
    set +x # turn off sh tracing so the error is easier to read
    echo 'Not on the correct branch to build this release'
    echo "Required branch: ${GIT_BRANCH}"
    echo "Actual branch: ${BRANCH_NAME}"
    exit 1
fi

if [ "x${CHECK_CLEAN_CHECKOUT}" = "xYES" -o "x${CHECK_CLEAN_CHECKOUT}" = "xyes" ]; then
    if [[ -n $(git status -s) ]]; then
        set +x # turn off sh tracing so the error is easier to read
        echo 'Repository has uncommitted changes.  Please stash or commit them'
        echo 'to leave a clean copy of the source code'
        exit 1
    fi
fi

%build

%install
rm -rf %{buildroot}/%{prefix}
mkdir -p %{buildroot}%{prefix}
tar -C ${RPM_BUILD_DIR}/../../msft -cf - . | tar -C %{buildroot}/%{prefix} -xf -
chmod -R o-rwx %{buildroot}/%{prefix}/*
for file in util/checkFolder.py \
            util/checkFolder.sh \
            util/imap-check.plx \
            directorySync.plx \
            preloadContacts.plx \
            reverseSync.plx \
            manualReverseSync.plx \
            manualSync.plx; do
    if [ -f "%{buildroot}/%{prefix}/${file}" ]; then
        chmod ug+x "%{buildroot}/%{prefix}/${file}"
    fi
done

%files
# in the language-libs package
%exclude /%{prefix}/migperl/
%exclude /%{prefix}/files/python-libs/
# in the files package
%exclude /%{prefix}/files
# in the emails package
%exclude /%{prefix}/lib/com/owm/*rfc822
# config is now managed by Telstra and should not be included
%exclude /%{prefix}/conf
/%{prefix}/*plx
/%{prefix}/*py
/%{prefix}/lib
/%{prefix}/util

%files files
%exclude /%{prefix}/files/python-libs/
/%{prefix}/files/cert/
/%{prefix}/files/bps/key.data.readme
/%{prefix}/files/zoneinfo/
/%{prefix}/files/MyInboxMigrationService.war
/%{prefix}/files/mailboxstatus.war

%files language-libs
/%{prefix}/migperl/
/%{prefix}/files/python-libs/

%files emails
/%{prefix}/lib/com/owm/*rfc822

%clean
rm -rf %{buildroot}

%pre
echo "Running 'pre-install' for %{name}..."
usage()
{
    echo ""
    echo "Available environment variables:"
    echo "HELP=<0|1> default:0  print usage message if 1"
    echo "INSTALL_USER=<user> default: migration"
    echo "INSTALL_GROUP=<group> default: imail"
    echo ""
    exit 1
}

echo_err()
{
    echo "######################################################################"
    echo ""
    echo "ERROR: $*"
    echo ""
    echo "######################################################################"
}

if [ "${HELP}" = "1" ]; then
    usage
fi

if [ -z "${INSTALL_USER}" ]; then
    INSTALL_USER=migration
fi

if ! id -u ${INSTALL_USER} >/dev/null 2>&1 ; then
    echo_err "The user '${INSTALL_USER}' does not exist. Please check the 'INSTALL_USER' environment variable."
    usage
fi

if [ -z "${INSTALL_GROUP}" ]; then
    INSTALL_GROUP=imail
fi

if ! id ${INSTALL_USER} | cut -d ' ' -f3 | cut -d '=' -f2 | grep -q -w "${INSTALL_GROUP}" ; then
    echo_err "The group '${INSTALL_GROUP}' does not exist. Please check the 'INSTALL_GROUP' environment variable."
    usage
fi

if [ ! -d "${RPM_INSTALL_PREFIX0}" ]; then
    echo_err "The directory '${RPM_INSTALL_PREFIX0}' does not exist. Did you specify a valid '--prefix'?"
    usage
fi

OWNER=`stat -c %U:%G ${RPM_INSTALL_PREFIX0}`
if [[ "${OWNER}" != "${INSTALL_USER}:${INSTALL_GROUP}" ]]; then
    echo_err "The directory '${RPM_INSTALL_PREFIX0}' is not owned by '${INSTALL_USER}:${INSTALL_GROUP}'."
    usage
fi

echo "Installing in '${RPM_INSTALL_PREFIX0}' as '${INSTALL_USER}:${INSTALL_GROUP}'."

%pre files
echo "Running 'pre-install' for %{name}-files..."
usage()
{
    echo ""
    echo "Available environment variables:"
    echo "HELP=<0|1> default:0  print usage message if 1"
    echo "INSTALL_USER=<user> default: migration"
    echo "INSTALL_GROUP=<group> default: imail"
    echo ""
    exit 1
}

echo_err()
{
    echo "######################################################################"
    echo ""
    echo "ERROR: $*"
    echo ""
    echo "######################################################################"
}

if [ "${HELP}" = "1" ]; then
    usage
fi

if [ -z "${INSTALL_USER}" ]; then
    INSTALL_USER=migration
fi

if ! id -u ${INSTALL_USER} >/dev/null 2>&1 ; then
    echo_err "The user '${INSTALL_USER}' does not exist. Please check the 'INSTALL_USER' environment variable."
    usage
fi

if [ -z "${INSTALL_GROUP}" ]; then
    INSTALL_GROUP=imail
fi

if ! id ${INSTALL_USER} | cut -d ' ' -f3 | cut -d '=' -f2 | grep -q -w "${INSTALL_GROUP}" ; then
    echo_err "The group '${INSTALL_GROUP}' does not exist. Please check the 'INSTALL_GROUP' environment variable."
    usage
fi

if [ ! -d "${RPM_INSTALL_PREFIX0}" ]; then
    echo_err "The directory '${RPM_INSTALL_PREFIX0}' does not exist. Did you specify a valid '--prefix'?"
    usage
fi

OWNER=`stat -c %U:%G ${RPM_INSTALL_PREFIX0}`
if [[ "${OWNER}" != "${INSTALL_USER}:${INSTALL_GROUP}" ]]; then
    echo_err "The directory '${RPM_INSTALL_PREFIX0}' is not owned by '${INSTALL_USER}:${INSTALL_GROUP}'."
    usage
fi

echo "Installing in '${RPM_INSTALL_PREFIX0}' as '${INSTALL_USER}:${INSTALL_GROUP}'."

%pre language-libs
echo "Running 'pre-install' for %{name}-language-libs..."
usage()
{
    echo ""
    echo "Available environment variables:"
    echo "HELP=<0|1> default:0  print usage message if 1"
    echo "INSTALL_USER=<user> default: migration"
    echo "INSTALL_GROUP=<group> default: imail"
    echo ""
    exit 1
}

echo_err()
{
    echo "######################################################################"
    echo ""
    echo "ERROR: $*"
    echo ""
    echo "######################################################################"
}

if [ "${HELP}" = "1" ]; then
    usage
fi

if [ -z "${INSTALL_USER}" ]; then
    INSTALL_USER=migration
fi

if ! id -u ${INSTALL_USER} >/dev/null 2>&1 ; then
    echo_err "The user '${INSTALL_USER}' does not exist. Please check the 'INSTALL_USER' environment variable."
    usage
fi

if [ -z "${INSTALL_GROUP}" ]; then
    INSTALL_GROUP=imail
fi

if ! id ${INSTALL_USER} | cut -d ' ' -f3 | cut -d '=' -f2 | grep -q -w "${INSTALL_GROUP}" ; then
    echo_err "The group '${INSTALL_GROUP}' does not exist. Please check the 'INSTALL_GROUP' environment variable."
    usage
fi

if [ ! -d "${RPM_INSTALL_PREFIX0}" ]; then
    echo_err "The directory '${RPM_INSTALL_PREFIX0}' does not exist. Did you specify a valid '--prefix'?"
    usage
fi

OWNER=`stat -c %U:%G ${RPM_INSTALL_PREFIX0}`
if [[ "${OWNER}" != "${INSTALL_USER}:${INSTALL_GROUP}" ]]; then
    echo_err "The directory '${RPM_INSTALL_PREFIX0}' is not owned by '${INSTALL_USER}:${INSTALL_GROUP}'."
    usage
fi

echo "Installing in '${RPM_INSTALL_PREFIX0}' as '${INSTALL_USER}:${INSTALL_GROUP}'."

%pre emails
echo "Running 'pre-install' for %{name}-emails..."
usage()
{
    echo ""
    echo "Available environment variables:"
    echo "HELP=<0|1> default:0  print usage message if 1"
    echo "INSTALL_USER=<user> default: migration"
    echo "INSTALL_GROUP=<group> default: imail"
    echo ""
    exit 1
}

echo_err()
{
    echo "######################################################################"
    echo ""
    echo "ERROR: $*"
    echo ""
    echo "######################################################################"
}

if [ "${HELP}" = "1" ]; then
    usage
fi

if [ -z "${INSTALL_USER}" ]; then
    INSTALL_USER=migration
fi

if ! id -u ${INSTALL_USER} >/dev/null 2>&1 ; then
    echo_err "The user '${INSTALL_USER}' does not exist. Please check the 'INSTALL_USER' environment variable."
    usage
fi

if [ -z "${INSTALL_GROUP}" ]; then
    INSTALL_GROUP=imail
fi

if ! id ${INSTALL_USER} | cut -d ' ' -f3 | cut -d '=' -f2 | grep -q -w "${INSTALL_GROUP}" ; then
    echo_err "The group '${INSTALL_GROUP}' does not exist. Please check the 'INSTALL_GROUP' environment variable."
    usage
fi

if [ ! -d "${RPM_INSTALL_PREFIX0}" ]; then
    echo_err "The directory '${RPM_INSTALL_PREFIX0}' does not exist. Did you specify a valid '--prefix'?"
    usage
fi

OWNER=`stat -c %U:%G ${RPM_INSTALL_PREFIX0}`
if [[ "${OWNER}" != "${INSTALL_USER}:${INSTALL_GROUP}" ]]; then
    echo_err "The directory '${RPM_INSTALL_PREFIX0}' is not owned by '${INSTALL_USER}:${INSTALL_GROUP}'."
    usage
fi

echo "Installing in '${RPM_INSTALL_PREFIX0}' as '${INSTALL_USER}:${INSTALL_GROUP}'."

%post 
echo "Running 'post-install' for %{name}..."
if [ -z "${INSTALL_USER}" ]; then
    INSTALL_USER=migration
fi
if [ -z "${INSTALL_GROUP}" ]; then
    INSTALL_GROUP=imail
fi

chown -R ${INSTALL_USER}:${INSTALL_GROUP} ${RPM_INSTALL_PREFIX0}/*plx
chown -R ${INSTALL_USER}:${INSTALL_GROUP} ${RPM_INSTALL_PREFIX0}/*py
chown -R ${INSTALL_USER}:${INSTALL_GROUP} ${RPM_INSTALL_PREFIX0}/lib
chmod o-rwx ${RPM_INSTALL_PREFIX0}/lib
chown -R ${INSTALL_USER}:${INSTALL_GROUP} ${RPM_INSTALL_PREFIX0}/util
chmod o-rwx ${RPM_INSTALL_PREFIX0}/util

echo "Post install complete for %{name}"

%post files
echo "Running 'post-install' for %{name}-files..."
if [ -z "${INSTALL_USER}" ]; then
    INSTALL_USER=migration
fi
if [ -z "${INSTALL_GROUP}" ]; then
    INSTALL_GROUP=imail
fi

chown -R ${INSTALL_USER}:${INSTALL_GROUP} ${RPM_INSTALL_PREFIX0}/files
chmod o-rwx ${RPM_INSTALL_PREFIX0}/files
echo "Post install complete for %{name}-files"

%post language-libs
echo "Running 'post-install' for %{name}-language-libs..."
if [ -z "${INSTALL_USER}" ]; then
    INSTALL_USER=migration
fi
if [ -z "${INSTALL_GROUP}" ]; then
    INSTALL_GROUP=imail
fi

chown -R ${INSTALL_USER}:${INSTALL_GROUP} ${RPM_INSTALL_PREFIX0}/migperl
chmod o-rwx ${RPM_INSTALL_PREFIX0}/migperl
chown -R ${INSTALL_USER}:${INSTALL_GROUP} ${RPM_INSTALL_PREFIX0}/files/python-libs
chown ${INSTALL_USER}:${INSTALL_GROUP} ${RPM_INSTALL_PREFIX0}/files
chmod o-rwx ${RPM_INSTALL_PREFIX0}/files/python-libs
echo "Post install complete for %{name}-language-libs"

%post emails
echo "Running 'post-install' for %{name}-emails..."
if [ -z "${INSTALL_USER}" ]; then
    INSTALL_USER=migration
fi
if [ -z "${INSTALL_GROUP}" ]; then
    INSTALL_GROUP=imail
fi

chown -R ${INSTALL_USER}:${INSTALL_GROUP} ${RPM_INSTALL_PREFIX0}/lib/com/owm/*rfc822
chown ${INSTALL_USER}:${INSTALL_GROUP} ${RPM_INSTALL_PREFIX0}/lib
chown ${INSTALL_USER}:${INSTALL_GROUP} ${RPM_INSTALL_PREFIX0}/lib/com
chown ${INSTALL_USER}:${INSTALL_GROUP} ${RPM_INSTALL_PREFIX0}/lib/com/owm
chmod o-rwx ${RPM_INSTALL_PREFIX0}/lib/com/owm/*rfc822
echo "Post install complete for %{name}-emails"
