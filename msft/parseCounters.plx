#!/usr/bin/perl
###########################################################################
#
#  Copyright 2014 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Description: This script parses provided mail migration trace logs and process counters for
#  Number of messages read from source
#  Number of messages written to mOS and migrated
#  Total size in bytes of messages written to mOS
#
###########################################################################

###########################################################################
# 'use' modules
###########################################################################
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl"; # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";

use strict;
use warnings;
use DBI;

use Log::Log4perl qw(get_logger :levels);
use Log::Log4perl::Level;
use Log::Dispatch::FileRotate;

use Config::Simple;


use com::owm::mosApi;
use com::owm::migBellCanada;

######################################################
# USAGE
######################################################
sub usage {

    print STDERR qq(

    Utility to parse migration counters and populate the migration database.

    The script is called at the end of the mail migration for each user. It reads all the trace.log files that are produced by the mail migration process
    and parses certain logs entries to calculate summary totals which are added to the migration database.

    Command line to run script:

    parseCounters.plx log_directory jobid email [timers] [debug]

    where:
        log_directory 	- the directory containing all the mail migration trace.logs for this user
        jobid			- the jobid for this batch run
        email			- the email address of the user being migrated
        timers          - parse the summary log file and collect timers
        debug			- run the script in debug mode optional

    );

    exit(0);
}


######################################################
# ---------------------------------------------------
# Global Variables
# ---------------------------------------------------
######################################################
$| = 1;    ## Makes STDOUT flush immediately
my $migrated_count = 0;
my $read_count = 0;
my $migrated_bytes = 0;
my $read_messages = 0;
my $num_messages_after = 0;
my $num_messages_before = 0;
my $LOGSTRING = "SCRIPT|JOB-UNK999";
my $SDEBUG;
my $configPath = 'conf/migrator.conf';
my $configLog = 'conf/log.conf';
my $JOBID = "UNK999";
my $EMAIL = "<email>";
my $TRACELOGDIR;
my $MIGRATION_TRACELOGDIR;
my $TIMERS;
my $SCREEN;
my $DBH;
my $PROCESSID = $$;
my $SCRIPTNAME = "parseCounter";
my $LOGMODE = "BATCHP";

# Log Object
Log::Log4perl->init($configLog);
our $log = get_logger("MIGRATE");

# Config object.
# And read the config file so all subs have access to params.
my $cfg  = new Config::Simple();
$cfg->read($configPath)
or $log->logdie("SCRIPT|FATAL: ".$cfg->error().". Please fix the issue and restart the migration program. Exiting.");

# Configuration Parameters
$SDEBUG = $cfg->param('log_debug');
$MIGRATION_TRACELOGDIR = $cfg->param('debug_output_path');


######################################################
# ---------------------------------------------------
# Sub Routines
# ---------------------------------------------------
######################################################

####################################################
# - collectCounters -                              #
# Parses trace.log files in the directory          #
#  and calculates counters from log lines          #
####################################################
sub collectCounters
{
    my $dirname = $TRACELOGDIR;
    my $email = $EMAIL;
    my ($finalEventId,$finalEventName);
    
    opendir(DIR, $dirname) or die "Could not open $dirname\n";

    my $filename;
    while ($filename = readdir(DIR)) {
        # ignore "." and ".."
        next if($filename =~ /^\.$/);
        next if($filename =~ /^\.\.$/);

        # ignore non trace.log files
        next if($filename =~ /^(?!trace.log)(.*)$/);

        my $TRACELOGFILE = "$dirname/$filename";
        $log->debug("Parsing file: $TRACELOGFILE");

        if (-e $TRACELOGFILE) {
            open (my $fh, "<", $TRACELOGFILE) or $log->logdie("$LOGSTRING: Unable to parse final mail migration totals. Can't open file $TRACELOGFILE: $! ");

            foreach my $line (<$fh>) {
                # Get the email address from the first line of the trace log
                #TBD Migration trace logging for $email is initialized...
                if (index($line, "Migration trace logging for") != -1){
                   $line =~ /Migration trace logging for (\S*) is initialized/;
                   $email = $1;
                }
                # Count migrated messages and the size sum
                if (index($line, "Calling mOS API to create message with attributes") != -1) {
                    $migrated_count++;
                    $line =~ /size\=\[(\d*)/;
                    $migrated_bytes += $1 if ($1);
                }

                # Count messages on source
                if (index($line, "Message count for folder") != -1) {
                    $line =~ /is (\d+)$/;
                    $read_messages += $1;
                }

                # Count messages read from source
                if ($line =~ /[^DEBUG] UID FETCH (.*?) \(BODY\.PEEK\[\]/) {
                    my @UIDs = split(/\,/,$1); # split at comma
                    foreach (@UIDs) {
                        if (index($_, ":") != -1) { # colon indicates a range
                            my $from = substr($_, 0, index($_, ":"));
                            my $to = substr($_, index($_, ":")+1, length($_));
                            $read_count = $read_count + ($to - $from) + 1;
                        } else {
                            $read_count++;
                        }
                    }
                }
            }
            close $fh;
        } else {
            $log->error("$LOGSTRING: Unable to parse final mail migration totals. $TRACELOGFILE does not exist");
            exit 0;
        }
    }
    closedir(DIR);
    return($email);
}

#################################################################
# - writeCountersToDatabase -                                   #
# Writes collected counter to their respective database tables  #
#################################################################
sub writeCountersToDatabase {
    if (defined $SCREEN ) {
        print "Counters: $JOBID, '$EMAIL', 'messagesMigrated', $migrated_count , $migrated_bytes \n";
        print "Counters: $JOBID, '$EMAIL', 'messagesRead', $read_messages \n";
    } else {
        my $query_test = $DBH->prepare("INSERT INTO counters (job_id, email, counter_name, total, size) VALUES (?, ?, ?, ?, ?)");
        $query_test->execute($JOBID, $EMAIL, 'messagesMigrated', $migrated_count , $migrated_bytes) or die $query_test->err_str;
        $query_test->execute($JOBID, $EMAIL, 'messagesRead', $read_messages, undef) or die $query_test->err_str;
        $query_test->execute($JOBID, $EMAIL, 'messagesReadFromSource', $read_count, undef) or die $query_test->err_str;

        $log->info("$LOGSTRING: Updated counters in database");
    }
}

sub collectTimers {
    my $filename = $TRACELOGDIR.'pl-migration-summary.log';
    unless(-f $filename) {
        $filename = $TRACELOGDIR.'owm-migration-summary.log';
        unless(-f $filename) {
            $log->logdie($LOGSTRING.':Migration summary log file "'.$filename.'" not found');
        }
    }
    my $processCount = 0;
    my ($migrationStatus, $migrationMessage, $migStatus, $finalEventId, $finalEventName);
    
    if (-e $filename) {
        open (my $fh, "<", $filename) or $log->logdie("Can't open file $filename: $! ");

        foreach my $line (<$fh>) {
            #lines beginning MigrationSummary:run indicate totals for a migration user
            #Lines beginning MigrationSummary:performMigration are the statistics headers
            my $runDate = substr($line, 0, index($line, ","));  # Use everything before the first comma is the run timestamp
            if (index($line, "MigrationSummary:run") != -1) { 
                $line =~ /mailbox\:(\S*)/;
                my $email = $1;
                #Time entry is time:t_start/t_stop/t_total. 
                #t_total is formated as 00h:00m:00s  Convert this to seconds to record in DB
                $line =~ /time:(\S+?\/\S+\/)(\d+).\:(\d+).\:(\d+)/;
                my $total = $4 + ($3 * 60) + ($2 * 60 * 60);

                #When the migration is terminated with CTRL-C it appears this timer is added to the summary entry. Skip these.
                #ToDo: not sure if this is the same number each time.
                writeTimersToDatabase($email, $total) unless ($total == 1456402631 || defined $SCREEN);
                #Set account migration status to indicate migration has been run
                $migStatus = com::owm::migBellCanada::setMigstatus($email,'',$cfg,$log,$migStatus,'z') unless (defined $SCREEN ); 
                com::owm::migBellCanada::logEventsToDatabase($DBH,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::EVENT_MIGRATION_START,'MigrationStart',$LOGMODE,$email,'','',"Start migration for this user via batch mail migration at $runDate") unless (defined $SCREEN );
    
                #Status of run is status:success/fail followed by reason:failure_reason
                #Extract the status, and if failure extract the reason
                $line =~ /status\:(\S*)/;
                my $migrationStatus=$1;
                $migrationMessage = " ";
                if ($migrationStatus eq "fail") {
                    $line =~ /reason\:(.*) time\:/;
                    $migrationMessage = $1;
                    #Set account migration status failed preload, and failed migration run
                    $migStatus = com::owm::migBellCanada::setMigstatus($email,'',$cfg,$log,$migStatus,'d') unless (defined $SCREEN );
                    $migStatus = com::owm::migBellCanada::setMigstatus($email,'',$cfg,$log,$migStatus,'m') unless (defined $SCREEN );  
                    $finalEventId = $com::owm::migBellCanada::STATUS_GENERAL_ERROR;
                } else {
                    #Set account migration status successful preload, and failed migration run
                    $migStatus = com::owm::migBellCanada::setMigstatus($email,'',$cfg,$log,$migStatus,'D') unless (defined $SCREEN );
                    $migStatus = com::owm::migBellCanada::setMigstatus($email,'',$cfg,$log,$migStatus,'M') unless (defined $SCREEN );  
                    $finalEventId = $com::owm::migBellCanada::EVENT_MIGRATION_END;
                }
                my $logstring2 = $LOGSTRING;
                $logstring2 =~ s/SCRIPT/$email/;
                if (defined $SCREEN){                
                    print "$JOBID\t$email \t\t Migration $migrationStatus in $total seconds. $migrationMessage \n";
                    $processCount++;
                } else { 
                    $log->info("$logstring2: Migration $migrationStatus , took $total seconds. $migrationMessage") unless ($total == 1456402631);
                    $finalEventName = $com::owm::migBellCanada::EVENT_NAMES{"$finalEventId"};
                    com::owm::migBellCanada::logEventsToDatabase($DBH,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$finalEventId,$finalEventName,$LOGMODE,$email,'',$migStatus,$migrationMessage . " at $runDate");
                }
            }
        }

        close $fh;
        print ("\nTotal Processed: $processCount \n") if (defined $SCREEN);
        $log->info("$LOGSTRING: Updated timers in database");
    } else {
        $log->error("$filename does not exist");
    }
}

sub writeTimersToDatabase {
    my $email = shift;
    my $total = shift;

    my $query_test = $DBH->prepare("INSERT INTO timers (job_id, email, timer_name, total) VALUES ($JOBID, '$email', 'mbox', $total)");
    $query_test->execute() or die $query_test->err_str;
}


######################################################
# ---------------------------------------------------
# Start Main
# ---------------------------------------------------
######################################################

if ($#ARGV < 1) {
    $log->error("$LOGSTRING: Missing command arguments only $#ARGV provided. Unable to parse final mail migration totals.");
    print "Missing command arguments only $#ARGV provided. Unable to parse final mail migration totals.\n";
    usage();
}

($TRACELOGDIR, $JOBID, $EMAIL, $TIMERS, my $opt1, my $opt2) = @ARGV;

if ( ((defined $opt1) && $opt1 eq 'd') || ((defined $opt2) && $opt2 eq 'd') || ((defined $TIMERS) && $TIMERS eq 'd')) {
    $SDEBUG = 1;
    $log->level($DEBUG);
}

if (defined $opt1 && $opt1 eq 'screen' ) {
    $SCREEN = 'screen';
}

$LOGSTRING =~ s/UNK999/$JOBID/;
$LOGSTRING = $LOGSTRING . " to screen" if (defined $SCREEN);

$DBH = com::owm::migBellCanada::connectEventDB("none", $cfg, $log);
if ($DBH eq $com::owm::migBellCanada::STATUS_GENERAL_ERROR) {
    $log->error("$LOGSTRING: Database error exiting, unable to parse final mail migration totals.");
    exit 1;
} else {
    $log->debug("$LOGSTRING: Connected to event log database");
}

#Process either the migration summary file or the trace logs 
if ((defined $TIMERS) && ($TIMERS eq 'timers')) {
    collectTimers();
} else {
    $EMAIL = collectCounters();
    $LOGSTRING =~ s/SCRIPT/$EMAIL/;
    $log->debug("$LOGSTRING: Migrated Count: $migrated_count Migrated Bytes: $migrated_bytes Read Messages: $read_messages");
    writeCountersToDatabase();
}

$DBH->disconnect;
exit 0;
