#!/usr/bin/perl
###########################################################################
#
#  Copyright 2014 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Mandi Buswell <mandi.buswell@owmessaging.com>
#  Date:        July/2015
#  Version:     1.0.0 - July 11 2015
#
###########################################################################

###########################################################################
# 'use' modules
###########################################################################
#use lib "/opt/owm/imail/perl/lib";
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl"; # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";

use strict;
use warnings;
use Getopt::Long;
use Log::Log4perl qw(get_logger :levels);
use Log::Log4perl::Level;  
use com::owm::mosApi;
use com::owm::migBellCanada;
use Net::LDAP;

# A package that provides an easy way to have a config file
use Config::Simple;

# A package that provides an easy way to launch multiple threads, with returns.
use Parallel::Fork::BossWorker; 

# A package that provides some status for our program
use Term::ProgressBar;

# Needed for LDAP search.
use Net::LDAP;

###########################################################################
# Global variables
###########################################################################

$| = 1;    ## Makes STDOUT flush immediately
my $SDEBUG;
my $PROCESSID = $$;
my $SCRIPTNAME = "preparePUID";

our $configPath = 'conf/migrator.conf';
our $logPath = 'conf/log.conf';

# Varables for progress bar
my $progress;
my $count      = 0;
my $startcount = 0;

# Logging object.
Log::Log4perl->init($logPath);
our $log = get_logger("PREP.PUID");
our $conflictLog = get_logger("PREP.CNFL");

# Boss Working object.
my $bw;

# Config object.
# And read the config file so all subs have access to params.
my $cfg  = new Config::Simple();
$cfg->read($configPath)
  or $log->logdie("SCRIPT|FATAL: ".$cfg->error().". Please fix the issue and restart the migration program. Exiting.");
  
  
#Set up connection to migration database and all cglobal database variables
my $DBMESSAGE="";
my $DBSTATUS;
my $DBSUCCESS = 0;
my $CONTINUE = "true";  #if a critical error occurs, allow script to exit gracefully and log event to DB.
my $JOBID = $com::owm::migBellCanada::DEFAULT_JOBID;
our $eventLog = get_logger("EVENT");
my $start = time;
my $SUCCESS_COUNT=0;
my $FAILED_COUNT=0;
my $statsRecord = "active";

######################################################
# USAGE
######################################################
sub usage {

	print STDERR qq(

  Utility to apply microsoft account IDs or PUID to email entry in SUR.

  Command line to run script:

    preparePUID.plx -puidFile <puidFile> 
                                          [-d]

    where:
    <puidFile>     is the path to a file containing PUIDs of accounts to be migrated.

    Switches:
    -d              turn debug on.

);

	exit(0);
}

############################################################
## - Progress Init -                                       #
##   Initialize our progress bar based on addrs read in    #
############################################################
sub progress_init($) {
  my $size = shift;
  $count = 0;
  $progress = Term::ProgressBar->new( { count => $size, term_width => '100', ETA => 'linear',} );
  $progress->minor(0);
  $progress->max_update_rate(2);
}

######################################################################
## - worker -                                                       ##
######################################################################
##MCB UDPATE
sub worker {
  my $work = shift;
  my $status;
  my $retEmail;
  my $retPUID;
  my $errorCode;
  # Declare variables needed for migBellCanada methods, but are not required by this script.
  my $mailboxstatus;
  my $migstatus;
  my $consentstatus;
  my $maillogin;

  # Input file is list of at least email address and PUIDs (one per line)
  # Accept input file with 2 or three columns
  my @workerArray = split(/\,/,$work->{data_line});
  my $email = $workerArray[0];
  my $puid = $workerArray[1];

 # ($puid, $email) = $work->{data_line};

  ##################################################################
  # General setup needed
  ##################################################################
  if (!$puid) {
	$log->error("SCRIPT|No PUIDs found in file. check input file and try again.\n");
	print ("\nNo PUIDs found in file. check input file and try again.\n");
    goto END;
  }
  if ($email eq "EmailAddress") {
	$log->debug("SCRIPT|Skipping first line of file as a header - $email");
	goto END;
  }
  $log->debug("$email|$puid: Start processing this entry ...");

  # Lookup account info using PUID.
  ($status,$retEmail,$mailboxstatus,$migstatus,$consentstatus,$maillogin) = com::owm::migBellCanada::lookupPUID($puid,$cfg,$log);

  if ( $status eq $com::owm::migBellCanada::STATUS_SUCCESS ) {
	#Record was found using the PUID. Check it is as expected.
	#use lc() here to normalise them both to lower case
	if ( lc($email) eq lc($retEmail) ) {
		#PUID and EMAIL are set in LDAP correctly
		print STATSFILE ("skipped \n") if ($statsRecord eq "active" );
		$log->debug("$email|$puid: PUID is set correctly. Go to next record. [retEMAIL: $retEmail ]");
	} else {
		#PUID is assigned to a different email address. Try to find if a PUID is assigned to the expected email address.
		$log->error("$email|$puid: Mismatch records, try lookup by email. [retEMAIL: $retEmail ] ");
		my $errorString = ("$email|$puid: inPUID: $puid inEMAIL: $email retEMAIL: $retEmail");
		#Find PUID by email address
		my ($retPUID,$retMboxID) = com::owm::migBellCanada::lookupPuidFromEmail($email,$cfg,$log);
		if ( !$retPUID ) {
			$log->error("$email|$puid: Conflicting inEMAIL address does not have a PUID assigned. ");
			$errorString .= " retPUID: None ";
		} elsif ($retPUID eq "many") {
			$log->error("$email|$puid: PUID lookup by email returned more than one result.");
			$errorString .= " retPUID: many ";
		} else {
			if ( ! $retPUID eq $puid ) {
				$log->error("$email|$puid: PUID lookup by email returned non matching result. [retPUID: $retPUID ]" );
				$errorString .= " retPUID: retPUID ";
			} else {
				#PUID and EMAIL are set in LDAP correctly do nothing. Should never get here because the IF for this ELSE is this case
			}
		} #end get PUID by email address
		$conflictLog->error($errorString);
		print STATSFILE ("fail \n") if ($statsRecord eq "active" );
		$log->error("$email|$puid: Processing skipped due to errors ...");
	}		
    goto END;
  } elsif ( $status eq $com::owm::migBellCanada::STATUS_FAIL_PUID_EMAIL_MISMATCH ){
	my $errorString = ("$email|$puid: inPUID: $puid inEMAIL: $email ");
	$errorString .= " 2 or more accounts had matching PUID.";
    $log->error("$email|$puid:2 or more accounts had matching PUID.");
	$conflictLog->error($errorString);
	print STATSFILE ("fail \n") if ($statsRecord eq "active" );
  } elsif ( $status eq $com::owm::migBellCanada::STATUS_FAIL_AS_CONNECT) {
	print STATSFILE ("connectfail \n") if ($statsRecord eq "active" );
	print RECONFILE "$email,$puid\n";
  } else {
	#PUID is not found, assign it
	$status = "";
	$log->debug("$email|$puid: Adding to LDAP record.");
	$status = setPUID($email,$puid);
	if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
			$log->error("$email|$puid: An error occurred status $status . PUID not updated.");
	}
  }

  END:
  if ($puid) {
	$log->debug("$email|$puid: Stop processing ...");
  }
  	print ""; #forces the progress bar to update. Don't know why, but without it will not update till the end.
}

######################################################################
# setPUID
#
# Subroutine to add the PUID for an account.
#
# Args:
# - email of user whose migstatus is being updated
# - puid of user whose migstatus is being updated
#
######################################################################
sub setPUID($$)
{
  my $email = shift;
  my $puid = shift;
  my $mx_dirserv_host = $cfg->param('mx_dirserv_host');
  my $mx_dirserv_port = $cfg->param('mx_dirserv_port');
  my $mx_dirserv_pwd  = $cfg->param('mx_dirserv_pwd');
  my $retry = 0;
  my $ldap;


  # Set up connection to dirserver.
 while ($retry < 5 ) {
  $ldap = Net::LDAP->new($mx_dirserv_host, port => $mx_dirserv_port);
  if ($ldap) {
	$retry = 6;
  } else {
	if ($retry > 3 ) {
		$log->error("Unable to connect to LDAP server $mx_dirserv_host: $@  after $retry times now exiting");
		print STATSFILE ("connectfail \n") if ($statsRecord eq "active" );
		print RECONFILE "$email,$puid\n";
		return($com::owm::migBellCanada::STATUS_FAIL_AS_CONNECT);
	} else {
		$log->warn("Unable to connect to LDAP server $mx_dirserv_host: $@  Retrying. Attempt $retry ...");
	}
    $retry++;
  }
 }
  my $mesg = $ldap->bind ( "cn=root",
                           password => $mx_dirserv_pwd,
                           version => 3 );          # use for changes/edits

  # Search for the account using the puid as the key.
  my $result = $ldap->search( filter => "maillogin=$email" );
  my @entries = $result->entries;
  my $numEntries = $#entries + 1;

  # Recheck search result.
  if ($result->code) {
    $log->error("$email|$puid setPUID: LDAP Lookup failed. " . $result->code );
    $ldap->unbind;
    $ldap->disconnect();
	print STATSFILE ("fail \n") if ($statsRecord eq "active" );
	print RETRY "$email,$puid\n";
    return($com::owm::migBellCanada::STATUS_FAIL_MAILLOGIN_SETTING);
  } elsif ($numEntries != 1) {
    $log->error("$email|$puid setPUID: Either 0 or 2+ accounts matching puid. Can't determine which account to update.");
    $ldap->unbind;
    $ldap->disconnect();
	print STATSFILE ("fail \n") if ($statsRecord eq "active" );
    return($com::owm::migBellCanada::STATUS_FAIL_MAILLOGIN_SETTING);
  }

 # my $ldapMaillogin = ${$entries[0]->get('maillogin')}[0];

  $entries[0]->replace(puid => $puid);

  $result = $entries[0]->update($ldap);
  if ($result->code) {
    $log->error("$email|$puid setPUID: Account not updated [inPUID: $puid inEMAIL: $email ] ".$result->code);
    $ldap->unbind;
    $ldap->disconnect();
	print RETRY "$email,$puid\n";
	print STATSFILE ("fail \n") if ($statsRecord eq "active" );
    return($com::owm::migBellCanada::STATUS_FAIL_MAILLOGIN_SETTING);
  }

  $ldap->unbind;
  $ldap->disconnect();
  print STATSFILE ("success \n") if ($statsRecord eq "active" );
  $log->info("$email|$puid setPUID: success");
  return($com::owm::migBellCanada::STATUS_SUCCESS);

}

######################################################################
## - Cleanup -                                                      ##
##   This routine is used to update the progress meter              ##
######################################################################
sub cleanup {
  $progress->update( ++$count );  # Update the counter
}

###################################################################
## - BuildQueue -                                                 #
##   Get the email addrs, puid and populate the processing queue  #
###################################################################
sub buildQueue {
  my $in_file = shift;
  $log->debug("SCRIPT|Input file set to \'$in_file\'");

  print("Main processing progress...\n");

  open my $file, "<", $in_file
    or $log->logdie("SCRIPT|Unable to open file $in_file: $!");

  while ( my $data_line = <$file>) 
  {
    $startcount++;
    chomp($data_line);
    $bw->add_work( { data_line => $data_line } );
  }

  close($file);

  $log->debug("SCRIPT|Processing $startcount records read from input file.");
  print("Processing $startcount records.\n");

  return ($startcount);
}

######################################################
# ---------------------------------------------------
# Start Main
# ---------------------------------------------------
######################################################

$SDEBUG = $cfg->param('log_debug');

my $numThreads = $cfg->param('prepare_threads');
my $threadTimeout = $cfg->param('thread_timeout');
my $sleep = $cfg->param('sleepWait') ? $cfg->param('sleepWait') : 5;
my $redoFile = defined $cfg->param('prepRedoFile') ? $cfg->param('prepRedoFile') : "./log/prepRedoFile-<processid>.list";
my $reconnectFile = defined $cfg->param('prepReconnectFile') ? $cfg->param('prepReconnectFile') : "./log/prepReconnectFile-<processid>.list";
$redoFile =~ s/<processid>/$$/;
$reconnectFile =~ s/<processid>/$$/;
my $puidFile = '';

GetOptions( '-d',\$SDEBUG,               # Override debug in config file.
            '-puidFile=s',\$puidFile,
            ) || usage();

print("\n\n");

if ($SDEBUG) {
  print("Running in DEBUG mode\n");
  $log->level($DEBUG);               #Override debug in log file when -d or debug set in config file
}

if ( $puidFile eq "all" ) {
  $log->info("SCRIPT|******BEGIN Processing PUID input file: ALL. Will generate from MSFT files");
  
  $puidFile="files/allpuid.csv";
  #$puidFile=$cfg->param('default_puid_in');
  
 }
# Check input file exists
if ( -e $puidFile ) {
  $log->info("SCRIPT|******BEGIN Processing PUID input file: $puidFile");

} else {
  $log->error("SCRIPT|******BEGIN PUID input file is missing: $puidFile");
  usage();
} 
#Start log for recording statistics
my $statsLog = "./log/pp-$$.stat";

open STATSFILE, ">", $statsLog 
    or  $log->error("SCRIPT|Unable to open file $statsLog: $! . Will not record statistics to file.");

open RETRY, ">", $redoFile or $log->error("SCRIPT|Unable to open redo file $redoFile: $! .");
open RECONFILE, ">", $reconnectFile or $log->error("SCRIPT|Unable to open redo file $reconnectFile: $! .");

# Setup a boss to send out the work
$bw = Parallel::Fork::BossWorker->new(
  work_handler   => \&worker,       # Routine that does the work.
  result_handler => \&cleanup,      # Routine called after work is finish (optional).
  global_timeout => $threadTimeout, # In seconds.
  worker_count   => $numThreads );  # Number of worker threads.

# Build queue that boss will handout to worker threads.
my $rc = &buildQueue($puidFile);

# Initialize the progess counters.
&progress_init($rc);
$DBMESSAGE = "Total records: $rc";

# Set boss to start handing out work to threads.
$bw->process() if ($CONTINUE eq "true");

$progress->update($rc);

print "\n";    # This just makes things look clean
close STATSFILE;
$FAILED_COUNT = `grep 'fail' $statsLog | wc -l`;
$SUCCESS_COUNT =  `grep 'success' $statsLog | wc -l`;
my $CONNECT_COUNT =  `grep 'connectfail' $statsLog | wc -l`;
my $SKIP_COUNT =  `grep 'skipped' $statsLog | wc -l`;
unlink $statsLog;
chomp $FAILED_COUNT;
chomp $SUCCESS_COUNT;
chomp $CONNECT_COUNT;
chomp $SKIP_COUNT;
$DBMESSAGE .= " Failed: $FAILED_COUNT Success: $SUCCESS_COUNT Connection Failures: $CONNECT_COUNT Skipped: $SKIP_COUNT";

# Exit script gracefully.
print "\n";    # This just makes things look clean
my $time_taken = time - $start;
my $dbh = com::owm::migBellCanada::connectEventDB($eventLog,$cfg,$log);
if ($dbh eq $com::owm::migBellCanada::STATUS_GENERAL_ERROR) {
        $log->error("SCRIPT|Database error exiting");
        exit 0;
} else {
        $log->debug("SCRIPT|Connected to event log database");
}
com::owm::migBellCanada::logEventsToDatabase($dbh,$cfg,$log,$PROCESSID,$SCRIPTNAME,__LINE__,$JOBID,$com::owm::migBellCanada::EVENT_PREPARE_PUID,'PreparePUID','',$puidFile,'','Finished','Took '.$time_taken.' seconds: '.$DBMESSAGE);
com::owm::migBellCanada::disconnectDB($dbh,$cfg,$log);
$log->info("SCRIPT|Completed took $time_taken seconds. $DBMESSAGE");

exit 0;
######################################################
# ---------------------------------------------------
# End Main
# ---------------------------------------------------
######################################################

######################################################
# ---------------------------------------------------
# BEGIN POD Documentation
# ---------------------------------------------------
######################################################

__END__



