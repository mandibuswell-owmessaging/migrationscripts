#!/usr/bin/env python

#############################################################################
#
#  Copyright 2015 Openwave Messaging.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:                 myinbox-calendar-export.py
#  Description:             Export MyInbox calendar data to ICS file
#  Author:                  Gary Palmer - Openwave Messaging
#  Date:                    Nov, 2015
#  Customer:                Telstra
#  Version:                 1.0.0 - Nov 2015
#
#############################################################################
#
#  Version Control
#
#  Version: 1.0.0
#  Changes:
#
#############################################################################

# pylint: disable=C0103
# pylint: disable=W0703
# pylint: disable=W0232

"""
Contact the Telstra MyInbox SOAP service and extract a users calendar
to write to an ical format file
"""
import sys
import signal

from base64 import urlsafe_b64decode
from calendar import monthrange
import codecs
from copy import copy
import datetime
from dateutil.rrule import rrule, rrulestr, MONTHLY
from httplib import BadStatusLine
from icalendar import Calendar, Event, Alarm, vCalAddress, vText, vDuration
import json
from optparse import OptionParser, Option, OptionValueError
from pprint import pformat
import pytz
import re
# requests objects to running with a non-default signal handler for SIGCHLD
# temporarily reset it and then put it back
oldSigChld = signal.getsignal(signal.SIGCHLD)
signal.signal(signal.SIGCHLD, signal.SIG_DFL)
import requests
signal.signal(signal.SIGCHLD, oldSigChld)
from retrying import retry
from suds.client import Client
from suds.plugin import MessagePlugin
import time
import traceback

import retrying_config

class UnicodeFilter(MessagePlugin):
    '''
    The MyInbox SOAP API seems to return UTF-16 code points (two 4 character
    hex entites together) in a stream declared to be UTF-8.  This tries
    to deal with it by transcoding the two entities to UTF-8, and if there
    are any left over entities in the 0xD800 to 0xDFFF range, replace them
    with ASCII question marks
    '''
    def received(self, context):
        reply = context.reply
        codepoint = '&#x(D[89A-Fa-f][0-9A-Fa-f][0-9A-fa-f]);'
        search = '('+codepoint+codepoint+')'
        # log_trace('search: ' + search)
        match = re.search(search, reply)
        while match and len(match.groups()) == 3:
            try:
                # log_trace('str1: '+ m.group(2))
                # log_trace('str2: '+ m.group(3))
                char = unichr(int(match.group(2), 16)) + \
                       unichr(int(match.group(3), 16))
                # log_trace(u'char: ' + pformat(char))
                char = char.encode('utf-8')
                # log_trace(u'char: ' + pformat(char))
                replace = match.group(1)
                # log_trace(u'replace: ' + replace)
                reply = re.sub(replace, char, reply)
                log_info(u'Found UTF-16 code points in SOAP data, replacing ' +
                         replace + ' with ' + pformat(char))
                match = re.search(search, reply)
            except Exception, e:
                log_error('OOPS! ' + str(e))
                log_error(traceback.format_exc())
                exit(1)
        try:
            match = re.search(r'(&#xD[89A-Fa-f][0-9A-Fa-f][0-9A-Fa-f];)', reply)
            while match and len(match.groups() == 1):
                log_info(u'Found UTF-16 code points in SOAP data, replacing ' +
                         match.group(1) + ' with "?"')
                reply = re.sub(match.group(1), '?', reply)
        except Exception, e:
            log_error('OOPS! ' + str(e))
            log_error(traceback.format_exc())
            exit(1)
        context.reply = reply

def log_trace(data):
    """
    Format trace events.  Normally multi-line so need a tag on each line

    :param data: Tracing data
    """
    print u'TRACE:{0}'.format('-' * 40)
    print u'TRACE:{0}'.format(data.replace("\r\n", "\n").
                              replace("\n", "\nTRACE:"))
    sys.stdout.flush()

def log_statistic(stat, value):
    """
    Send a statistic to the calling process

    :param event: Data to log
    """
    print u'STAT:{0}:{1}'.format(stat, value)
    sys.stdout.flush()

def log_debug(event):
    """
    Format an debug level log event

    :param event: Data to log
    """
    print u'DEBUG:{0}'.format(event)
    sys.stdout.flush()

def log_info(event):
    """
    Format an info level log event

    :param event: Data to log
    """
    print u'INFO:{0}'.format(event)
    sys.stdout.flush()

def log_warn(event):
    """
    Format an warning level log event

    :param event: Data to log
    """
    print u'WARN:{0}'.format(event)
    sys.stdout.flush()

def log_error(event):
    """
    Format an error level log event

    :param event: Data to log
    """
    print u'ERROR:{0}'.format(event)
    sys.stdout.flush()

def log_fatal(event):
    """
    Format an fatal level log event

    :param event: Data to log
    """
    print u'FATAL:{0}'.format(event)
    sys.stdout.flush()

def cb_check_datetime(option, opt, value):
    """
    try to validate that the option value is a valid datetime in the requested format
    """
    try:
        utc = pytz.UTC
        dt_object = datetime.datetime.strptime(value, '%Y-%m-%d').replace(tzinfo=utc).date()
        return dt_object
    except:
        errstr = 'Invalid date value as it does not match YYYY-MM-DD format'
        raise OptionValueError('Option %s: %s: %s' % (opt, errstr, value))

class DateTimeOption(Option):
    """
    Add a date/time type to OptionParser
    """
    TYPES = Option.TYPES + ('datetime',)
    TYPE_CHECKER = copy(Option.TYPE_CHECKER)
    TYPE_CHECKER["datetime"] = cb_check_datetime

@retry(wait_fixed=retrying_config.CAL_GETCATEGORIES_WAIT_FIXED,
       stop_max_attempt_number=retrying_config.CAL_GETCATEGORIES_MAX_RETRY)
def callGetEventCategories(client, options, categories_query):
    '''
    :param client: A SUDS client object for the calendar WSDL
    :param options: The command line options return value
    :param query: Query structure
    :returns: result object
    '''
    try:
        categories_result = client.service.getEventCategories(categories_query)
    except Exception, e:
        if options.tracelogs:
            req = u'{0}: getEventCategories SOAP request: {1}'
            resp = u'{0}: getEventCategories SOAP error: {1}'
            log_trace(req.format(options.email, client.last_sent()))
            log_trace(resp.format(options.email, str(e)))
        raise
    if options.tracelogs:
        req = u'{0}: getEventCategories SOAP request: {1}'
        resp = u'{0}: getEventCategories SOAP response: {1}'
        log_trace(req.format(options.email, client.last_sent()))
        log_trace(resp.format(options.email, client.last_received()))
    return categories_result

def populateCategories(client, options):
    """
    Create a dict of category ID -> category name for the users calendar

    :param client: A SUDS client object for the calendar WSDL
    :param options: The command line options return value
    :returns: a dict mapping of category ID to category name
    """
    categories = {}
    categories[-1] = u'General'
    categories_query = {
        'userIdentifierVO': {
            'email': options.email,
            'login': {
                'username': options.email,
                'password': options.password
            }
        },
    }

    if options.userid:
        categories_query['userProfileVO'] = {
            'uid': long(options.userid),
        }

    try:
        categories_result = callGetEventCategories(client, options, categories_query)
        for category in categories_result['categories']:
            if category['display'].lower() == 'friends':
                categories[category['id']] = 'General'
            elif category['display'].lower() == 'personal':
                categories[category['id']] = 'General'
            elif category['display'].lower() == 'other':
                categories[category['id']] = 'General'
            else:
                categories[category['id']] = category['display'].encode('utf-8')
        if 'sessionVO' in categories_result:
            if 'jsessionID' in categories_result['sessionVO']:
                sessionId = categories_result['sessionVO']['jsessionID']
            else:
                raise Exception('No jsessionID returned by SOAP API')
            if 'jsessionIDLastAccessTime' in categories_result['sessionVO']:
                sessionAccess = categories_result['sessionVO']['jsessionIDLastAccessTime']
            else:
                raise Exception('No jsessionIDLastAccessTime returned by SOAP API')
        else:
            raise Exception('No session returned by SOAP API')
        return categories, sessionId, sessionAccess
    except BadStatusLine, e:
        log_fatal(u'HTTP error fetching calendar categories from SOAP server: {0}'.format(str(e)))
        raise
    except Exception, e:
        log_error(u'Exception while processing calendar categories: {0}'.format(str(e)))
        raise

def display(cal):
    """
    Turn an icalendar object into a unicode string

    :param cal: an icalendar object
    :returns: string version of calendar
    """
    return unicode(cal.to_ical().replace('\r\n', '\n').replace(';VALUE=DATE-TIME', ''), 'utf-8')

@retry(wait_fixed=retrying_config.CAL_GETINVITEES_WAIT_FIXED,
       stop_max_attempt_number=retrying_config.CAL_GETINVITEES_MAX_RETRY)
def callGetEventInvitees(client, options, query):
    '''
    :param client: A SUDS client object for the calendar WSDL
    :param options: The command line options return value
    :param query: Query structure
    :returns: result object
    '''
    try:
        inviteesResult = client.service.getEventInvitees(query)
    except Exception, e:
        if options.tracelogs:
            req = u'{0}: getEventInvitees SOAP request: {1}'
            resp = u'{0}: getEventInvitees SOAP error: {1}'
            log_trace(req.format(options.email, client.last_sent()))
            log_trace(resp.format(options.email, str(e)))
        raise
    if options.tracelogs:
        req = u'{0}: getEventInvitees SOAP request: {1}'
        resp = u'{0}: getEventInvitees SOAP response: {1}'
        log_trace(req.format(options.email, client.last_sent()))
        log_trace(resp.format(options.email, client.last_received()))
    return inviteesResult

def findInvitees(iCalEvent, client, eid, options, sessionId, sessionAccess):
    """
    Create an attendee structure and add it to the iCalEvent

    :param iCalEvent: The iCal event structure to have invitees added to
    :param client: A SUDS client object for the calendar WSDL
    :param eid: An event ID
    :param options: The command line options return value
    :param sessionId: The SOAP API session ID
    :param sessionAccess: The SOAP API session access info
    """
    MYINBOX_PARTSTAT_MAP = {
        'needsaction': 'NEEDS-ACTION',
        'accepted': 'ACCEPTED',
        'tentative': 'TENTATIVE',
        'declined': 'DECLINED',
        }

    query = {
        'userIdentifierVO': {
            'email': options.email,
            'JSessionID': sessionId,
            'jsessionIDLastAccessTime': sessionAccess,
        },
        'eid': eid,
    }
    if options.userid:
        query['userProfileVO'] = {
            'uid': long(options.userid),
        }

    try:
        inviteesResult = callGetEventInvitees(client, options, query)
        if 'sessionVO' in inviteesResult:
            if 'jsessionIDLastAccessTime' in inviteesResult['sessionVO']:
                sessionAccess = inviteesResult['sessionVO']['jsessionIDLastAccessTime']
            else:
                raise Exception('No jsessionIDLastAccessTime returned by SOAP API')
        else:
            raise Exception('No session returned by SOAP API')
    except BadStatusLine, e:
        fatal = u'HTTP error fetching event invitees for eid {0} from SOAP server: {1}'
        log_fatal(fatal.format(eid, str(e)))
        exit(1)
    except Exception, e:
        fatal = u'Error fetching event invitees for eid {0} from SOAP server: {1}'
        log_fatal(fatal.format(eid, str(e)))
        log_trace(fatal.format(eid, str(e)))
        log_trace(traceback.format_exc())
        exit(1)
    for invitee in inviteesResult['invitees']:
        attendee = vCalAddress(u'MAILTO:' + invitee['email'])
        if invitee['status'] in MYINBOX_PARTSTAT_MAP:
            attendee.params['PARTSTAT'] = vText(MYINBOX_PARTSTAT_MAP[invitee['status']])
        if invitee['name']:
            attendee.params['cn'] = vText(invitee['name'])
        iCalEvent.add('attendee', attendee, encode=0)
    return iCalEvent, sessionAccess

def makeRrule(event, local_tz, utc):
    """
    Parse the eventRecurrence elements of an XML event and create a
    RRULE structure from them

    :param event: XML structure representing an individual event
    :returns: RRULE structure
    """
    recur = event['eventRecurrence']
    event_rrule = {
        'freq': recur['recurrenceType'],
        'interval': recur['recurrenceFrequency'],
    }
    if recur['daysOfWeek']:
        event_rrule_byday = []
        for day in recur['daysOfWeek']:
            event_rrule_byday.append(day[:2].upper())
        if event_rrule_byday:
            event_rrule['wkst'] = 'SU'
            event_rrule['byday'] = list(event_rrule_byday)
    if recur['recurrenceEndVO']:
        if recur['recurrenceEndVO']['recurrenceEndType']:
            if recur['recurrenceEndVO']['recurrenceEndType'] == 'bynboccurrence':
                if recur['recurrenceEndVO']['numOfRecurrences'] and \
                  isinstance(recur['recurrenceEndVO']['numOfRecurrences'], (int, long)):
                    numRecur = recur['recurrenceEndVO']['numOfRecurrences']
                    event_rrule['count'] = numRecur
                    # TELSTRA-281 - UxSuite considers number of instances,
                    # MyInbox is number of weeks
                    if recur['recurrenceType'].upper() == 'WEEKLY':
                        daysEachWeek = len(event_rrule['byday'])
                        event_rrule['count'] = numRecur * daysEachWeek
            if recur['recurrenceEndVO']['recurrenceEndType'] == 'bydate':
                if recur['recurrenceEndVO']['recurrenceEndDate']:
                    end = recur['recurrenceEndVO']['recurrenceEndDate']
                    delta = datetime.timedelta(seconds=time.timezone)
                    until = end + delta
                    until = until.replace(tzinfo=utc).astimezone(local_tz)
                    event_rrule['until'] = until
    # TELSTRA-280 - try to mimic MyInbox UI behaviour
    if recur['recurrenceType'].upper() == 'MONTHLY':
        tzdelta = datetime.timedelta(seconds=time.timezone)
        startDate = event['startDate'] + tzdelta
        startDate = startDate.replace(tzinfo=utc).astimezone(local_tz)
        event_rrule['bymonthday'] = startDate.day
        last_day_of_month = monthrange(startDate.year, startDate.month)[1]
        # TELSTRA-509 - events on 29 Feb(leap year) only show on leap years
        # Events on last day of month are always on the last day of month.
        # Events that start towards the end of a month but not on the last
        # day of the month end up with a range of dates they can start on
        # so the system will allow the event to repeat on months with fewer
        # days.
        # Else just the day the event starts on
        if startDate.day == 29 and startDate.month == 2:
            event_rrule['bymonthday'] = 29
        elif startDate.day == last_day_of_month:
            event_rrule['bymonthday'] = -1
        elif startDate.day > 28:
            event_rrule['bymonthday'] = list(range(28, startDate.day + 1))
            event_rrule['bysetpos'] = -1
    if recur['recurrenceType'].upper() == 'YEARLY':
        tzdelta = datetime.timedelta(seconds=time.timezone)
        startDate = event['startDate'] + tzdelta
        startDate = startDate.replace(tzinfo=utc).astimezone(local_tz)
        event_rrule['bymonth'] = startDate.month
        last_day_of_month = monthrange(startDate.year, startDate.month)[1]
        # TELSTRA-509 - events on 29 Feb(leap year) only show on leap years
        # Events on last day of month are always on the last day of month.
        # Else just the day the event starts on
        if startDate.day == 29 and startDate.month == 2:
            event_rrule['bymonthday'] = 29
        elif startDate.day == last_day_of_month:
            event_rrule['bymonthday'] = -1
    return event_rrule

def makealert(event, email):
    """
    Parse the event structure and generate an Alarm() object from the 'alert' data

    :param event:An XML event structure from MyInbox
    :param email:The users email address to send the alarm to
    :returns: A populated Alarm object
    """
    alert = event['alert']
    alarm = Alarm()
    alarm.add('action', alert['alertMedia'].upper())
    alarm.add('summary', u'Calendar reminder')
    alarm.add('description', u'Calendar reminder')
    alarm.add('attendee', u'mailto:{0}'.format(email))
    if alert['alertUnitType'] == 'minute':
        unit = alert['alertUnit']
        trigger = vDuration(datetime.timedelta(minutes=-unit))
    elif alert['alertUnitType'] == 'hour':
        unit = alert['alertUnit']
        if unit > 2:
            info = u'Reminder was set to {0} hours, changed to 2 hours'
            log_info(info.format(unit))
            unit = 2
        trigger = vDuration(datetime.timedelta(hours=-unit))
    elif alert['alertUnitType'] == 'day':
        unit = alert['alertUnit']
        if unit >= 7:
            info = u'Reminder was set to {0} days, changed to 1 week'
            log_info(info.format(unit))
            trigger = vDuration(datetime.timedelta(weeks=-1))
        if unit > 2 and unit < 7:
            info = u'Reminder was set to {0} days, changed to 2 days'
            log_info(info.format(unit))
            unit = 2
        if unit < 7:
            trigger = vDuration(datetime.timedelta(days=-unit))
    elif alert['alertUnitType'] == 'week':
        unit = alert['alertUnit']
        if unit > 1:
            info = u'Reminder was set to {0} weeks, changed to 1 week'
            log_info(info.format(unit))
            unit = 1
        trigger = vDuration(datetime.timedelta(weeks=-unit))
    else:
        error = 'Unexpected alert unit type of "{0}" when generating VALARM for EID{1}'
        raise Exception(error.format(alert['alertUnitType'], event['eid']))
    trigger.params[u'value'] = u'DURATION'
    trigger.params[u'related'] = u'START'
    alarm.add('trigger', trigger, encode=0)
    return alarm

def xml2ical(client, eid, event, options, categories, sessionId, sessionAccess):
    """
    Parse the XML event structure returned by SOAP and feed it into
    a new icalendar event object

    :param client: A SUDS client object for the calendar WSDL
    :param eid: The event ID for this event
    :param event: The event structure
    :param options: The command line options return value
    :param categories: The categories structure
    :param sessionId: The SOAP API session ID
    :param sessionAccess: The SOAP API session access info
    :returns: iCal event structure, sessionAccess value
    """
    iCalEvent = Event()
    mx9_email = options.email
    if mx9_email.endswith('@telstra.com'):
        suffix_length = len('@telstra.com')
        mx9_email = mx9_email[:-suffix_length] + '@bigpond.com'
    iCalEvent.add('organizer', 'mailto:{0}'.format(mx9_email))
    if event['eid']:
        if event['eid'] != eid:
            warn = u'Event ID "{0}" has a different EID of "{1}" in the ' + \
                   'returned SOAP data to the value from getCalendarViewAndEvents'
            log_warn(warn.format(eid, event['eid']))
        else:
            iCalEvent.add('comment', event['eid'])
    icalMap = {
        'title': 'summary',
        'location': 'location',
        'description': 'description',
        'uid': 'uid',
    }
    for key in icalMap.keys():
        if event[key]:
            iCalEvent.add(icalMap[key], event[key].encode('utf-8'))
    local_tz = pytz.timezone(options.timezone)
    utc = pytz.UTC
    if event['allDay']:
        # the way to signal to icalendar that the event is all day is to remove the time
        # of day and just leave the date
        startDate = event['startDate'].replace(tzinfo=utc).date()
        endDate = event['endDate'].replace(tzinfo=utc).date()
        # KOALA Ux Suite version needs the end date to be 1 day later than
        # we get from SOAP
        delta = datetime.timedelta(days=1)
        endDate = endDate + delta
    else:
        delta = datetime.timedelta(seconds=time.timezone)
        startDate = event['startDate'] + delta
        startDate = startDate.replace(tzinfo=utc).astimezone(local_tz)
        endDate = event['endDate'] + delta
        endDate = endDate.replace(tzinfo=utc).astimezone(local_tz)
    iCalEvent.add('dtstart', startDate)
    iCalEvent.add('dtend', endDate)
    if event['categoryId'] in categories:
        iCalEvent.add('categories', categories[event['categoryId']])
    if not event['uid']:
        log_warn(u'Event ID "{0}" does not have a UID value'.format(eid))
    if 'hasInvitees' in event and event['hasInvitees']:
        iCalEvent, sessionAccess = findInvitees(iCalEvent, client, event['eid'],
                                                options, sessionId, sessionAccess)
    if event['eventRecurrence']['recurrenceType']:
        try:
            rruleData = makeRrule(event, local_tz, utc)
            iCalEvent.add('rrule', rruleData)
        except Exception, e:
            log_error(u'Error generating RRULE for EID {0}: {1}'.
                      format(eid, str(e)))
            raise
    if event['alert']:
        if event['alert']['alertMedia'] == 'email':
            try:
                alert = makealert(event, mx9_email)
                iCalEvent.add_component(alert)
            except Exception, e:
                log_error(u'Error generating ALARM for EID {0}: {1}'.
                          format(eid, str(e)))
                raise
    return iCalEvent, sessionAccess

def walkChild(calobject):
    """
    Recursive function that builds a dict of event IDs (eids) from
    a getCalendarViewAndEvents SOAP response

    :param calobject: part of the SOAP response to examine
    :returns: A dictionary of event IDs
    """
    eids = dict()
    if 'children' in calobject:
        if calobject['children'] != None:
            for child in calobject['children']:
                neweids = walkChild(child['children'])
                eids.update(neweids)
            return eids
    for item in calobject:
        if 'events' in item:
            if item['events']:
                for event in item['events']:
                    eids[event['eid']] = 1
    return eids

def icalDtToDate(event, item, local_tz, tzdelta):
    """
    Fetch a specific date or datetime item from an iCal object
    and convert it to a datetime.date object
    :param event:
    :param item:
    :param local_tz:
    :param tzdelta:
    :returns:
    """
    if isinstance(event.get(item).dt, datetime.datetime):
        dateTime = event.get(item).dt + tzdelta
        dateTime = dateTime.replace(tzinfo=pytz.utc).astimezone(local_tz)
        date = dateTime.date()
    else:
        date = event.get(item).dt
    return date

def filterEvents(options, iCalEvent):
    """
    Check that events fall within the requested date range.

    If the event is not recuring then the end date for the event must
    be before the requested end date

    If the event is recuring, then do not enforce a start/end date
    restriction UNLESS the enforceEnd command line parameter was used.
    If enforceEnd was requested, then the last instance in a series of
    recuring events must end before the requested end date

    :param options: The command line options return value
    :param event: the event to check
    :returns: true or false if the event falls within the requested range
    """
    rruleval = iCalEvent.get('rrule')
    local_tz = pytz.timezone(options.timezone)
    tzdelta = datetime.timedelta(seconds=time.timezone)
    if rruleval:
        # always test this, not just in enforceEnd, in case the
        # requested start date is not the first of a month, and
        # therefore the getCalendarViewAndEvents call could return
        # data before the start date
        dtstart = 'DTSTART:' + iCalEvent.get('dtstart').to_ical()
        value = 'RRULE:' + rruleval.to_ical()
        if not 'COUNT' in value and not 'UNTIL' in value:
            value = '{0};UNTIL=20181231T235959'.format(value)
        value = value + "\n" + dtstart
        dates = list(rrulestr(value))

        lastOccurrence = dates[-1].replace(tzinfo=pytz.UTC)
        if lastOccurrence.date() < options.startdate:
            info = u'Excluding recurring event id "{0}" because rrule ' + \
                   'end "{1}" is before requested start "{2}"'
            log_info(info.format(iCalEvent.get('uid'), lastOccurrence.date(),
                                 options.enddate))
            return False
        # enforce that the last instance of an event MUST be before the
        # requested end date?
        if options.enforceEnd:
            if lastOccurrence.date() > options.enddate:
                info = u'Excluding recurring event id "{0}" because rrule' + \
                       ' end "{1}" is after requested end "{2}" and' + \
                       ' enforceEnd is requested'
                log_info(info.format(iCalEvent.get('uid'), lastOccurrence.date(),
                                     options.enddate))
                return False
        else:
            # make sure the event starts before the END of the requested range
            # else the migration will not skip events that are in the future.
            dtstart = icalDtToDate(iCalEvent, 'dtstart', local_tz, tzdelta)
            if dtstart > options.enddate:
                info = u'Excluding recurring event id "{0}" because ' + \
                       'start "{1}" is after requested end "{2}"'
                log_info(info.format(iCalEvent.get('uid'), dtstart,
                                     options.enddate))
                return False
    else:
        dtstart = icalDtToDate(iCalEvent, 'dtstart', local_tz, tzdelta)
        if dtstart < options.startdate:
            info = u'Excluding event id "{0}" because start "{1}" ' + \
                   'is before requested start "{2}"'
            log_info(info.format(iCalEvent.get('uid'), dtstart,
                                 options.startdate))
            return False
        if dtstart > options.enddate:
            info = u'Excluding event id "{0}" because start "{1}" ' + \
                   'is after requested end "{2}"'
            log_info(info.format(iCalEvent.get('uid'), dtstart,
                                 options.enddate))
            return False
        dtend = icalDtToDate(iCalEvent, 'dtend', local_tz, tzdelta)
        if dtend > options.enddate:
            info = u'Excluding event id "{0}" because end "{1}" ' + \
                   'is after requested end "{2}"'
            log_info(info.format(iCalEvent.get('uid'), dtend,
                                 options.enddate))
            return False

    # Default - include the event if one of the tests does not exclude it
    # explicitly
    return True

def printMonthlyInfo(stats, date):
    """
    Simple routine to print out some info from the recently downloaded calendar month

    :param stats: Statistics structure containing 'alreadyseen' and 'count' elements
    :param date: Date object for the requested month
    """
    seen = ''
    if stats['alreadyseen'] == 1:
        seen = u', and {0} event already seen in previous months'.format(stats['alreadyseen'])
    elif stats['alreadyseen'] > 1:
        seen = u', and {0} events already seen in previous months'.format(stats['alreadyseen'])

    if stats['count'] == 0:
        info = u'Found no new events in {0}'
        log_info(info.format(date.strftime('%b %Y'), seen))
    else:
        info = u'Found {0} new event{1} in {2}'
        if stats['count'] == 1:
            log_info(info.format(stats['count'], '',
                                 date.strftime('%b %Y'), seen))
        else:
            log_info(info.format(stats['count'], 's',
                                 date.strftime('%b %Y'), seen))

@retry(wait_fixed=retrying_config.CAL_GETVIEWANDEVENTS_WAIT_FIXED,
       stop_max_attempt_number=retrying_config.CAL_GETVIEWANDEVENTS_MAX_RETRY)
def callGetViewAndEvents(client, options, query):
    '''
    :param client: A SUDS client object for the calendar WSDL
    :param options: The command line options return value
    :param query: Query structure
    :returns: result object
    '''
    try:
        result = client.service.getCalendarViewAndEvents(query)
    except Exception, e:
        if options.tracelogs:
            req = u'{0}: getCalendarViewAndEvents SOAP request: {1}'
            resp = u'{0}: getCalendarViewAndEvents SOAP error: {1}'
            log_trace(req.format(options.email, client.last_sent()))
            log_trace(resp.format(options.email, str(e)))
        raise
    if options.tracelogs:
        req = u'{0}: getCalendarViewAndEvents SOAP request: {1}'
        resp = u'{0}: getCalendarViewAndEvents SOAP response: {1}'
        log_trace(req.format(options.email, client.last_sent()))
        log_trace(resp.format(options.email, client.last_received()))
    return result

def fetchCalendars(client, options, sessionId, sessionAccess):
    """
    Iterate back about <history> months and use getCalendarViewAndEvents
    to get calendar events in those months

    :param client: A SUDS client object for the calendar WSDL
    :param options: The command line options return value
    :param sessionId:
    :param sessionAccess:
    :returns: A list with two members: a dict of EIDs and a dict of event structures
    """
    query = {
        'userIdentifierVO': {
            'email': options.email,
            'JSessionID': sessionId,
            'jsessionIDLastAccessTime': sessionAccess,
        },
        'view': 'month'
    }

    today = datetime.datetime.now(pytz.UTC)
    eventsWithInvitees = dict()
    eids = dict()
    if options.startdate and not options.enddate and options.startdate < today:
        daterange = list(rrule(MONTHLY, cache=False, dtstart=options.startdate.replace(day=1),
                               interval=1, until=today.replace(day=1)))
    elif options.enddate and not options.startdate and options.enddate > today:
        daterange = list(rrule(MONTHLY, cache=False, dtstart=today.replace(day=1),
                               interval=1, until=options.enddate.replace(day=1)))
    elif options.startdate and options.enddate and options.startdate < options.enddate:
        daterange = list(rrule(MONTHLY, cache=False, dtstart=options.startdate.replace(day=1),
                               interval=1, until=options.enddate.replace(day=1)))
    else:
        errmsg = u'Invalid startdate/enddate option(s).  startdate="{0}", enddate="{1}"'
        log_fatal(errmsg.format(options.startdate, options.enddate))
        exit(1)
    globalStats = {'alreadyseen': 0}
    for date in daterange:
        query['id'] = date.strftime('%d%m%Y')
        query['userIdentifierVO']['jsessionIDLastAccessTime'] = sessionAccess
        stats = {'count': 0, 'notfound': 0, 'alreadyseen': 0}
        log_info(u'Downloading calendar for the month of {0}'.format(date.strftime('%b %Y')))
        result = callGetViewAndEvents(client, options, query)
        neweids = walkChild(result['calendarView'])
        eids.update(neweids)
        if 'eventsWithInvitees' in result and result['eventsWithInvitees']:
            for event in result['eventsWithInvitees']:
                if event['eid'] in eventsWithInvitees:
                    stats['alreadyseen'] += 1
                else:
                    eventsWithInvitees[event['eid']] = event
                    stats['count'] += 1
        printMonthlyInfo(stats, date)
        globalStats['alreadyseen'] += stats['alreadyseen']
        if 'sessionVO' in result:
            if 'jsessionIDLastAccessTime' in result['sessionVO']:
                sessionAccess = result['sessionVO']['jsessionIDLastAccessTime']
            else:
                raise Exception('No jsessionIDLastAccessTime returned by SOAP API')
        else:
            raise Exception('No sessionVO returned by SOAP API')
    newEventsWithInvitees = dict()
    stats = {'count': 0, 'notfound': 0}
    for eid in eids.keys():
        if not eid in eventsWithInvitees:
            error = u'Event id "{0}" not found in eventsWithInvitees results'
            log_error(error.format(eid))
            stats['notfound'] += 1
            continue
        newEventsWithInvitees[eid] = eventsWithInvitees[eid]
        stats['count'] += 1
    if stats['notfound'] > 0:
        info = u'Number of unique events listed in day view but not found in ' + \
               'eventsWithInvitees: {0}'
        log_info(info.format(stats['notfound']))
    log_statistic(u'eventsRetrieved', stats['count'])
    log_statistic(u'eventsNotFound', stats['notfound'])
    log_statistic(u'eventExists', globalStats['alreadyseen'])
    return(eids, newEventsWithInvitees, sessionAccess)

@retry(wait_fixed=retrying_config.CAL_GETSETTINGS_WAIT_FIXED,
       stop_max_attempt_number=retrying_config.CAL_GETSETTINGS_MAX_RETRY)
def callGetSettings(client, options, settings_query):
    '''
    :param client: A SUDS client object for the calendar WSDL
    :param options: The command line options return value
    :param query: Query structure
    :returns: result object
    '''
    try:
        settings_result = client.service.getCalendarSettings(settings_query)
    except Exception, e:
        if options.tracelogs:
            req = u'{0}: getCalendarSettings SOAP request: {1}'
            resp = u'{0}: getCalendarSettings SOAP error: {1}'
            log_trace(req.format(options.email, client.last_sent()))
            log_trace(resp.format(options.email, str(e)))
        raise
    if options.tracelogs:
        req = u'{0}: getCalendarSettings SOAP request: {1}'
        resp = u'{0}: getCalendarSettings SOAP response: {1}'
        log_trace(req.format(options.email, client.last_sent()))
        log_trace(resp.format(options.email, client.last_received()))
    return settings_result

def migrateCalSettings(client, options, sessionId, sessionAccess):
    """
    Copy the calendar settings to MX9 via mOS

    :param client: A SUDS client object for the calendar WSDL
    :param options: The command line options return value
    :param sessionId:
    :param sessionAccess:
    """
    settings_query = {
        'userIdentifierVO': {
            'email': options.email,
            'JSessionID': sessionId,
            'jsessionIDLastAccessTime': sessionAccess,
        },
    }
    if options.userid:
        settings_query['userProfileVO'] = {
            'uid': long(options.userid),
        }

    try:
        mx9_email = options.email
        if mx9_email.endswith('@telstra.com'):
            suffix_length = len('@telstra.com')
            mx9_email = mx9_email[:-suffix_length] + '@bigpond.com'
        settings_result = callGetSettings(client, options, settings_query)
        mosUrl = 'http://{0}:{1}/mxos/dataStore/v2/{2}/15'
        mosUrl = mosUrl.format(options.mosServer, options.mosPort, mx9_email)
        result = requests.get(mosUrl)
        if options.tracelogs:
            req = u'HTTP request to {0} gets status code {1} in response'
            log_trace(req.format(mosUrl, result.status_code))
            log_trace(u'Response JSON: {0}'.format(result.json()))
        if result.status_code == 200:
            wrapper = result.json()
            if 'data' in wrapper and wrapper['data'] != '':
                data = json.loads(wrapper['data'])
            else:
                data = dict()
        elif result.status_code == 500:
            if 'code' not in result.json():
                log_fatal(u'Unknown error fetching Ux suite preferences')
                exit(1)
            if result.json()['code'] != 'DST_KEY_NOT_EXIST':
                fatal = u'Error fetching Ux suite preferences: {0}'
                log_fatal(fatal.format(result.json()['code']))
                exit(1)
            data = dict()
            wrapper = dict()
        else:
            fatal = u'Error fetching Ux suite preferences: {0} {1}'
            log_fatal(fatal.format(result.status_code, result.reason))
        if 'sdk.calendar' not in data:
            data[u'sdk.calendar'] = dict()
        if options.disableClientTimezone:
            if 'sdk.prefs' not in data:
                data[u'sdk.prefs'] = dict()
            data[u'sdk.prefs']['attr.user.timezone.UseDeviceTimeZone'] = 'false'
        if 'dayStartsAt' in settings_result and \
          settings_result['dayStartsAt'] >= 0 and \
          settings_result['dayStartsAt'] < 24:
            data[u'sdk.calendar']['dayStart'] = settings_result['dayStartsAt']
            log_info(u'Setting calendar day start to {0}'.format(settings_result['dayStartsAt']))
        if 'dayEndsAt' in settings_result and \
          settings_result['dayEndsAt'] >= 0 and \
          settings_result['dayEndsAt'] < 24:
            data[u'sdk.calendar']['dayEnd'] = settings_result['dayEndsAt']
            log_info(u'Setting calendar day end to {0}'.format(settings_result['dayEndsAt']))
        if 'defaultView' in settings_result and \
          (settings_result['defaultView'] == 'day' or \
           settings_result['defaultView'] == 'week' or \
           settings_result['defaultView'] == 'month'):
            data[u'sdk.calendar']['defaultView'] = settings_result['defaultView']
            log_info(u'Setting calendar default view to {0}'.format(settings_result['defaultView']))
        if 'eventLength' in settings_result and \
          (settings_result['eventLength'] > 29 and
           settings_result['eventLength'] < 121):
            data[u'sdk.calendar']['eventDurationTime'] = settings_result['eventLength']
            data[u'sdk.calendar']['eventDurationUnits'] = u'minutes'
            dur = u'{0} {1}'.format(data[u'sdk.calendar']['eventDurationTime'],
                                    data[u'sdk.calendar']['eventDurationUnits'])
            log_info(u'Setting calendar default duration to {0}'.format(dur))
        data[u'sdk.calendar']['weekStart'] = 1
        log_info(u'Setting calendar week start day to Monday')
        wrapper[u'data'] = json.dumps(data, sort_keys=True)
        if options.tracelogs:
            log_trace(u'JSON settings: {0}'.format(wrapper['data']))
        result = requests.post(mosUrl, data=wrapper)
        if result.status_code != 200:
            fatal = u'Cannot set calendar preferences in mOS: {0} {1}'
            log_fatal(fatal.format(result.status_code, result.reason))
            exit(1)
        log_info(u'Successfully set calendar preferences')
    except BadStatusLine, e:
        log_fatal(u'HTTP error fetching calendar settings from SOAP server: {0}'.format(str(e)))
        raise
    except Exception, e:
        log_error(u'Exception while processing calendar settings: {0}'.format(str(e)))
        raise

def parser_configure():
    '''
    :returns: A parser object
    '''
    usage = 'usage: %prog --email=EMAIL --password=PASSWORD ' + \
            '--userid=USERID --startdate=YYYY-MM-DD --enddate=YYYY-MM-DD ' + \
            '--serviceURL=URL --output=FILENAME --mostHost=HOSTNAME ' + \
            '--mosPort=PORT [--enforceend]'
    parser = OptionParser(option_class=DateTimeOption, usage=usage)
    parser.add_option('--email', action='store', type='string', dest='email',
                      help='fully qualified email address for the user')
    parser.add_option('--password', action='store', type='string', dest='password',
                      help='the users password encoded in Base64 for URL applications')
    parser.add_option('--userid', action='store', type='string', dest='userid',
                      help='numeric user identifier for the user')
    parser.add_option('--startdate', action='store', type='datetime', dest='startdate',
                      help='optional start date, before which events are not fetched')
    parser.add_option('--enddate', action='store', type='datetime', dest='enddate',
                      help='optional end date, after which events are not fetched')
    parser.add_option('--serviceURL', action='store', type='string', dest='serviceURL',
                      help='URL to fetch the WSDL for the service from')
    parser.add_option('--output', action='store', dest='output',
                      help='Path to write exported icalendar data to')
    parser.add_option('--enforceend', action='store_true', dest='enforceEnd',
                      help='Enforce RRULE must end before --enddate', default=False)
    parser.add_option('--tracelogs', action='store_true', dest='tracelogs',
                      help='Enable trace logging of SOAP calls to STDOUT',
                      default=False)
    parser.add_option('--timezone', action='store', dest='timezone',
                      help='Timezone to export data in')
    parser.add_option('--mosHost', action='store', dest='mosServer',
                      help='IP address or hostname of the mOS server used for migration')
    parser.add_option('--mosPort', action='store', dest='mosPort',
                      help='TCP port number for the mOS server used for migration')
    parser.add_option('--disableClientTimezone', action='store_true',
                      dest='disableClientTimezone',
                      help='Turn off the "Use Client Timezone" flag in Ux Suite',
                      default=False)
    parser.add_option('--cacheDays', action='store', dest='cacheDays',
                      help='Number of days to cache WSDLs for')
    return parser

def handleOptions():
    """
    Define / check command line options
    """
    parser = parser_configure()
    try:
        (options, args) = parser.parse_args()
    except Exception, e:
        log_fatal(u'Internal error processing options: {0}'.format(str(e)))
        exit(1)
    requiredOptions = [
        'email',
        'password',
        'serviceURL',
        'output',
        'timezone',
        'mosServer',
        'mosPort',
        'cacheDays',
    ]
    for option in requiredOptions:
        if not getattr(options, option):
            log_fatal(u'--{0} argument must be supplied'.format(option))
            exit(1)
    if '@' not in options.email:
        log_fatal(u'--email argument must be supplied and must have an @ sign in it')
        parser.print_help()
        exit(1)
    try:
        password = urlsafe_b64decode(options.password)
    except TypeError, e:
        fatal = u'Cannot decode base64 argument to --password: {0}: {1}'
        log_fatal(fatal.format(str(e), options.password))
        exit(1)
    except Exception, e:
        fatal = u'Cannot decode argument to --password: {0}: {1}'
        log_fatal(fatal.format(str(e), options.password))
        exit(1)
    options.password = password
    if options.startdate or options.enddate:
        if options.startdate and not options.enddate:
            fatal = u'Require both --startdate and --enddate options to ' + \
                'be specified if --startdate is specified'
            log_fatal(fatal)
            exit(1)
        if not options.startdate and options.enddate:
            fatal = u'Require both --startdate and --enddate options to ' + \
                'be specified if --enddate is specified'
            log_fatal(fatal)
            exit(1)
        if options.enddate < options.startdate:
            fatal = u'Invalid --startdate and --enddate options: requested ' +\
                'end date of "{0}" is before the requested start date of "{1}"'
            log_fatal(fatal.format(options.enddate, options.startdate))
            exit(1)
        if options.enddate == options.startdate:
            fatal = u'Invalid --startdate and --enddate options: requested ' +\
                'end date of "{0}" is equal to the requested start date of "{1}"'
            log_fatal(fatal.format(options.enddate, options.startdate))
            exit(1)
        info = u'Migrating calendar events starting no earlier than {0} ' + \
            'and ending no later than {1}'
        log_info(info.format(options.startdate, options.enddate))
    if args:
        log_fatal(u'Unexpected command line arguments.  Aborting')
        parser.print_help()
        exit(1)
    log_info('Using mOS server {0}:{1}'.format(options.mosServer,
                                               options.mosPort))
    log_info('Using time zone {0}'.format(options.timezone))
    return options

def fatal_trace(msg):
    '''
    common code to simplify main()
    '''
    log_fatal(msg)
    log_trace(msg)
    log_trace(traceback.format_exc())
    exit(1)

def getClient(options):
    '''
    Get a SOAP client
    '''
    try:
        client = Client(options.serviceURL, plugins=[UnicodeFilter()])
        cache = client.options.cache
        cache.setduration(days=options.cacheDays)
    except BadStatusLine, e:
        msg = u'HTTP error fetching WSDL from SOAP server: {0}'.format(str(e))
        fatal_trace(msg)
    except Exception, e:
        msg = u'Cannot instantiate SOAP client: {0}'.format(str(e))
        fatal_trace(msg)
    return client

def main():
    '''
    The main function.
    '''
    options = handleOptions()

    client = getClient(options)

    try:
        categories, sessionId, sessionAccess = populateCategories(client, options)
    except Exception, e:
        msg = u'Error populating categories: {0}'.format(str(e))
        fatal_trace(msg)

    cal = Calendar()
    cal.add(u'prodid', u'-//Openwave Messaging//Calendar exporter for Telstra MyInbox//EN')
    cal.add(u'version', u'2.0')

    try:
        (eids, eventsWithInvitees, sessionAccess) = fetchCalendars(client, options,
                                                                   sessionId, sessionAccess)
    except Exception, e:
        msg = u'Error fetching calendar from SOAP server: {0}'.format(str(e))
        fatal_trace(msg)
    eventsExported = eventsOutsideRequestedDates = 0
    for eid in eids.keys():
        try:
            iCalEvent, sessionAccess = xml2ical(client, eid, eventsWithInvitees[eid],
                                                options, categories, sessionId,
                                                sessionAccess)
            if filterEvents(options, iCalEvent):
                cal.add_component(iCalEvent)
                eventsExported += 1
            else:
                eventsOutsideRequestedDates += 1
        except Exception, e:
            msg = u'Error turning XML calendar into icalendar and checking date ranges: {0}'
            fatal_trace(msg.format(str(e)))
    log_statistic('eventsExported', eventsExported)
    log_statistic('eventsOutsideRequestedDates', eventsOutsideRequestedDates)

    try:
        with codecs.open(options.output, 'w', 'utf-8') as output:
            output.write(display(cal))
    except IOError, e:
        msg = u'Cannot open icalendar file for writing: {0}'.format(str(e))
        fatal_trace(msg)
    except Exception, e:
        msg = u'Error writing icalendar file: {0}'.format(str(e))
        fatal_trace(msg)

    try:
        migrateCalSettings(client, options, sessionId, sessionAccess)
    except Exception, e:
        msg = u'Error migrating calendar settings: {0}'.format(str(e))
        fatal_trace(msg)

if __name__ == '__main__':
    main()
