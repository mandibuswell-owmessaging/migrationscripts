#!/usr/bin/env python

#############################################################################
#
#  Copyright 2016 Synchronoss.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Synchronoss
#  support and operations guidelines.
#
#  Program:                 python-config.py
#  Description:             Export MyInbox contact data to ICS file
#  Author:                  Gary Palmer - Synchronoss Messaging
#  Date:                    Oct, 2016
#  Customer:                Telstra
#  Version:                 1.0.0 - Nov 2016
#
#############################################################################
#
#  Version Control
#
#  Version: 1.0.0
#  Changes:
#
#############################################################################

"""
Read the retrying JSON and allow it to be queried by the retrying stanzas in
the various python scripts

Default to 3 second fixed wait and a max of 3 retries if not configured otherwise
"""

import os.path
import json

CONFIG = dict()
if os.path.isfile('conf/retrying_config.json'):
    with open('conf/retrying_config.json') as jsonfile:
        CONFIG = json.loads(jsonfile.read())

WAIT_FIXED = int(CONFIG.get('WAIT_FIXED', 3000))
MAX_RETRY = int(CONFIG.get('MAX_RETRY', 3))
PAB_GETALL_WAIT_FIXED = int(CONFIG.get('PAB_GETALL_WAIT_FIXED', WAIT_FIXED))
PAB_GETALL_MAX_RETRY = int(CONFIG.get('PAB_GETALL_MAX_RETRY',
	                                  MAX_RETRY))
PAB_GETSETTINGS_WAIT_FIXED = int(CONFIG.get('PAB_GETSETTINGS_WAIT_FIXED', WAIT_FIXED))
PAB_GETSETTINGS_MAX_RETRY = int(CONFIG.get('PAB_GETSETTINGS_MAX_RETRY',
                                           MAX_RETRY))
CAL_GETCATEGORIES_WAIT_FIXED = int(CONFIG.get('CAL_GETSCATEGORIES_WAIT_FIXED', WAIT_FIXED))
CAL_GETCATEGORIES_MAX_RETRY = int(CONFIG.get('CAL_GETCATEGORIES_MAX_RETRY',
                                             MAX_RETRY))
CAL_GETINVITEES_WAIT_FIXED = int(CONFIG.get('CAL_GETINVITEES_WAIT_FIXED', WAIT_FIXED))
CAL_GETINVITEES_MAX_RETRY = int(CONFIG.get('CAL_GETINVITEES_MAX_RETRY',
                                           MAX_RETRY))
CAL_GETSETTINGS_WAIT_FIXED = int(CONFIG.get('CAL_GETSETTINGS_WAIT_FIXED', WAIT_FIXED))
CAL_GETSETTINGS_MAX_RETRY = int(CONFIG.get('CAL_GETSETTINGS_MAX_RETRY',
                                           MAX_RETRY))
CAL_GETVIEWANDEVENTS_WAIT_FIXED = int(CONFIG.get('CAL_GETVIEWANDEVENTS_WAIT_FIXED', WAIT_FIXED))
CAL_GETVIEWANDEVENTS_MAX_RETRY = int(CONFIG.get('CAL_GETVIEWANDEVENTS_MAX_RETRY',
                                                MAX_RETRY))
