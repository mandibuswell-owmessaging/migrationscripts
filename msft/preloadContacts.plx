#!/usr/bin/perl
###########################################################################
#
#  Copyright 2015 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Huanwen Jiao <huanwen.jiao@owmessaging.com>
#  Date:        Dec/2015
#  Version:     1.0.0 - DEC 14 2015

#
###########################################################################

###########################################################################
# 'use' modules
###########################################################################
#use lib "/opt/owm/imail/perl/lib";
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl"; # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";

use strict;
use warnings;
use Getopt::Long;
use Log::Log4perl qw(get_logger :levels);
use Log::Log4perl::Level;
use Log::Dispatch::FileRotate;  
use XML::Simple;
use DateTime;
use Time::Local;
use POSIX qw(strftime);
use DBI;
use JSON;

# A package that provides an easy way to have a config file
use Config::Simple;

# A package that provides an easy way to launch multiple threads, with returns.
use Parallel::Fork::BossWorker; 

# A package that provides some status for our program
use Term::ProgressBar;

###########################################################################
# Global variables
###########################################################################

$| = 1;    ## Makes STDOUT flush immediately
my $SDEBUG;
my $SUCCESS_COUNT=0;
my $FAILED_COUNT=0;
my $SCRIPTNAME="preloadContacts";
my $PROCESSID=$$;

our $configPath = 'conf/migrator.conf';
our $logPath = 'conf/log.conf';

# Varables for progress bar
my $progress;
my $count      = 0;
my $startcount = 0;

# Logging object.
Log::Log4perl->init($logPath);
our $log = get_logger("LOGFILE3");

# Boss Working object.
my $bw;

# Config object.
# And read the config file so all subs have access to params.
my $cfg  = new Config::Simple();
$cfg->read($configPath)
  or $log->logdie("SCRIPT|FATAL: ".$cfg->error().". Please fix the issue and restart the migration program. Exiting.");

my $start = time;

#Create db connection
my $mysql_user = $cfg->get_param("mysql_user");
my $mysql_pass = $cfg->get_param("mysql_pass");
my $mysql_db = $cfg->get_param("mysql_db_contacts");
my $mysql_host = $cfg->get_param("mysql_host");
my $table = "contacts";
my $dbi_str = "DBI:mysql:$mysql_db;host=$mysql_host";
my $dbh = DBI->connect($dbi_str,$mysql_user,$mysql_pass, {RaiseError => 1}) ;

##log fail

  


############################################################
## - Progress Init -                                       #
##   Initialize our progress bar based on addrs read in    #
############################################################
sub progress_init($) {
  my $size = shift;
  $count = 0;
  $progress = Term::ProgressBar->new( { count => $size, term_width => '100', } );
  $progress->minor(0);
}

######################################################################
## - worker -                                                       ##
######################################################################
##MCB UDPATE
sub worker {
    my $work = shift;
    my $file = $work->{'contact_file'};
    my $mysqlcmd = "load data local infile '$file' into table contacts fields terminated by ',' lines terminated by '\r\n' IGNORE 1 LINES";
    my $sth = $dbh->prepare($mysqlcmd)  or die "Error:" .  $dbh->{'mysql_error'} ."\n";
    $sth->execute or  die "Error:" . $sth->errstr . "\n";
    my $rowsReturned = $sth->rows;
    $log-> info( "SCRIPT|INFO : $file - $rowsReturned \n");
    $sth->finish();
}


######################################################################
## - Cleanup -                                                      ##
##   This routine is used to update the progress meter              ##
######################################################################
sub cleanup {
  $progress->update( ++$count );  # Update the counter
}

###################################################################
## - BuildQueue -                                                 #
##   Get the email addrs, puid and populate the processing queue  #
###################################################################
sub buildQueue {
    my $contactFilePath = shift;
    $log->debug("SCRIPT|Input file path set to \'$contactFilePath\'");
    print("Main processing progress...\n");

    my @path;
    $path[0] = $contactFilePath.'BLU/';
    $path[1] = $contactFilePath.'CO1/';
    $path[2] = $contactFilePath.'DM2/';

    foreach my $dir (@path) {
        opendir(my $dirh, $dir) or die "can't opendir $dir: $!";
        while (my $file = readdir($dirh)) {
            next if ($file =~ m/^\./);
            $startcount++;
            $file = $dir.$file;
            $bw->add_work( { 'contact_file' => $file } );
        }
        closedir($dirh);
    }
    
    $log->debug("SCRIPT|Processing $startcount records read from input file.");
    print("Processing $startcount records.\n");
    return ($startcount);

}

######################################################
# USAGE
######################################################
sub usage {

	print STDERR qq(
        Command line to run script:
        preloadContacts.plx
    );

	exit(0);
}
######################################################
# ---------------------------------------------------
# Start Main
# ---------------------------------------------------
######################################################

$SDEBUG = $cfg->param('log_debug');

my $numThreads = $cfg->param('preload_threads');
$numThreads = 1; #tricky
my $threadTimeout = $cfg->param('thread_timeout');

print("\n\n");

if ($SDEBUG) {
  print("Running in DEBUG mode\n");
  $log->level($DEBUG);               #Override debug in log file when -d or debug set in config file
}

my $contactFilePath = $cfg->get_param("contact_file_path");
#$contactFilePath = '/opt/j-sandbox/msft/migrationscripts/msft/files/liveContacts/'; #test
# Check input file exists

if ( -e $contactFilePath ) {
  $log->info("SCRIPT|******BEGIN Processing  $contactFilePath");
} else {
  $log->error("SCRIPT|******BEGIN The flat files path is invalid: $contactFilePath");
  usage();
} 

# Setup a boss to send out the work
$bw = Parallel::Fork::BossWorker->new(
  work_handler   => \&worker,       # Routine that does the work.
  result_handler => \&cleanup,      # Routine called after work is finish (optional).
  global_timeout => $threadTimeout, # In seconds.
  worker_count   => $numThreads );  # Number of worker threads.

# Build queue that boss will handout to worker threads.
my $rc = &buildQueue($contactFilePath);

# Initialize the progess counters.
&progress_init($rc);

# Set boss to start handing out work to threads.
$bw->process();

# Exit script gracefully.
print "\n";    # This just makes things look clean
my $time_taken = time - $start;


$dbh->disconnect();
exit 0;
######################################################
# ---------------------------------------------------
# End Main
# ---------------------------------------------------
######################################################

######################################################
# ---------------------------------------------------
# BEGIN POD Documentation
# ---------------------------------------------------
######################################################

__END__



