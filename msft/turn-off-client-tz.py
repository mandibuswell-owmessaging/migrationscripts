#!/usr/bin/env python

#############################################################################
#
#  Copyright 2015 Openwave Messaging.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:                 turn-off-client-tz.py
#  Description:             Contact mOS to turn off the Ux Suite "Use client
#                           timezone" option so that the migrated TZ can be
#                           used instead.
#  Author:                  Gary Palmer - Openwave Messaging
#  Date:                    Dec, 2015
#  Customer:                Telstra
#  Version:                 1.0.0 - Dec 2015
#
#############################################################################
#
#  Version Control
#
#  Version: 1.0.0
#  Changes:
#
#############################################################################

# pylint: disable=C0103
# pylint: disable=W0703

"""
Contact mOS to turn off the Ux Suite "Use client timezone" option so that the
migrated TZ can be used instead.
"""

import sys
import signal

from httplib import BadStatusLine
import json
from optparse import OptionParser
# requests objects to running with a non-default signal handler for SIGCHLD
# temporarily reset it and then put it back
oldSigChld = signal.getsignal(signal.SIGCHLD)
signal.signal(signal.SIGCHLD, signal.SIG_DFL)
import requests
signal.signal(signal.SIGCHLD, oldSigChld)

def log_trace(data):
    """
    Format trace events.  Normally multi-line so need a tag on each line

    :param data: Tracing data
    """
    print u'TRACE:{0}'.format('-' * 40)
    print u'TRACE:{0}'.format(data.replace("\r\n", "\n").
                              replace("\n", "\nTRACE:"))
    sys.stdout.flush()

def log_statistic(stat, value):
    """
    Send a statistic to the calling process

    :param event: Data to log
    """
    print u'STAT:{0}:{1}'.format(stat, value)
    sys.stdout.flush()

def log_debug(event):
    """
    Format an debug level log event

    :param event: Data to log
    """
    print u'DEBUG:{0}'.format(event)
    sys.stdout.flush()

def log_info(event):
    """
    Format an info level log event

    :param event: Data to log
    """
    print u'INFO:{0}'.format(event)
    sys.stdout.flush()

def log_warn(event):
    """
    Format an warning level log event

    :param event: Data to log
    """
    print u'WARN:{0}'.format(event)
    sys.stdout.flush()

def log_error(event):
    """
    Format an error level log event

    :param event: Data to log
    """
    print u'ERROR:{0}'.format(event)
    sys.stdout.flush()

def log_fatal(event):
    """
    Format an fatal level log event

    :param event: Data to log
    """
    print u'FATAL:{0}'.format(event)
    sys.stdout.flush()

def disableClientTz(options):
    """
    Get the UxSuite preferences, if any, and then turn off the flag that
    tells UxSuite to use the browsers timezone instead of the configured
    timezone
    """
    mosUrl = u'http://{0}:{1}/mxos/dataStore/v2/{2}/15'
    mosUrl = mosUrl.format(options.mosServer, options.mosPort, options.email)
    result = requests.get(mosUrl)
    if options.tracelogs:
        req = u'HTTP request to {0} gets status code {1} in response'
        log_trace(req.format(mosUrl, result.status_code))
        log_trace(u'Response JSON: {0}'.format(result.json()))
    if result.status_code == 200:
        wrapper = result.json()
        if 'data' in wrapper and wrapper['data'] != '':
            data = json.loads(wrapper['data'])
        else:
            data = dict()
    elif result.status_code == 500:
        if 'code' not in result.json():
            log_fatal(u'Unknown error fetching Ux suite preferences')
            exit(1)
        if result.json()['code'] != 'DST_KEY_NOT_EXIST':
            fatal = u'Error fetching Ux suite preferences: {0}'
            log_fatal(fatal.format(result.json()['code']))
            exit(1)
        data = dict()
        wrapper = dict()
    else:
        fatal = u'Error fetching Ux suite preferences: {0} {1}'
        log_fatal(fatal.format(result.status_code, result.reason))
    if 'sdk.prefs' not in data:
        data[u'sdk.prefs'] = dict()
    if options.reverse:
        data[u'sdk.prefs'][u'attr.user.timezone.UseDeviceTimeZone'] = 'true'
    else:
        data[u'sdk.prefs'][u'attr.user.timezone.UseDeviceTimeZone'] = 'false'
    wrapper[u'data'] = json.dumps(data, sort_keys=True)
    if options.tracelogs:
        log_trace(u'JSON settings: {0}'.format(wrapper['data']))
    result = requests.post(mosUrl, data=wrapper)
    if result.status_code != 200:
        fatal = u'Cannot set use client timezone preference in mOS: {0} {1}'
        log_fatal(fatal.format(result.status_code, result.reason))
        exit(1)
    if options.reverse:
        log_info(u'Successfully set use client timezone preference')
    else:
        log_info(u'Successfully set do not use client timezone preference')

def handleOptions():
    """
    Define / check command line options
    """
    usage = 'usage: %prog --email=EMAIL --mosHost=HOSTNAME ' + \
            '--mosPort=PORT'
    parser = OptionParser(usage=usage)
    parser.add_option('--email', action='store', type='string', dest='email',
                      help='fully qualified email address for the user')
    parser.add_option('--tracelogs', action='store_true', dest='tracelogs',
                      help='Enable trace logging of SOAP calls to STDOUT',
                      default=False)
    parser.add_option('--mosHost', action='store', dest='mosServer',
                      help='IP address or hostname of the mOS server used for migration')
    parser.add_option('--mosPort', action='store', dest='mosPort',
                      help='TCP port number for the mOS server used for migration')
    parser.add_option('--reverse', action='store_true', dest='reverse',
                      help='Enable, rather than disable, use client TZ',
                      default=False)

    try:
        (options, args) = parser.parse_args()
    except Exception, e:
        log_fatal(u'Internal error processing options: {0}'.format(str(e)))
        exit(1)
    requiredOptions = [
        'mosServer',
        'mosPort',
    ]
    for option in requiredOptions:
        if not getattr(options, option):
            log_fatal(u'--{0} argument must be supplied'.format(option))
            exit(1)
    if '@' not in options.email:
        log_fatal(u'--email argument must be supplied and must have an @ sign in it')
        parser.print_help()
        exit(1)
    if args:
        log_fatal(u'Unexpected command line arguments.  Aborting')
        parser.print_help()
        exit(1)
    return options

def main():
    """
    The main function.
    """
    options = handleOptions()
    try:
        disableClientTz(options)
    except Exception, e:
        fatal = u'Fatal error disabling UxSuite setting: {0}'
        log_fatal(fatal.format(str(e)))

if __name__ == '__main__':
    main()
