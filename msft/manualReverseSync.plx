#! /usr/bin/perl
# Description: Performs a manual sync from Mx9 to Mx8
# 
# Release Version: Kent.Dang@synchronoss 2016-April-22
#
############################################################################
### 'use' modules
#############################################################################
#use lib "/opt/owm/imail/perl/lib";
use bigint;
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl";
use lib "$FindBin::Bin/migperl/lib/perl5";
use strict;
use Net::LDAP;
use Getopt::Long;
use Config::Simple;
use Log::Log4perl qw(get_logger :levels);
#use Switch;

###########################################################################
### Global variables
#############################################################################

$| = 1;    ## Makes STDOUT flush immediately

my $configPath = 'conf/migrator.conf';
my $logPath    = 'conf/log.conf';

# Logging object.
Log::Log4perl->init($logPath);
my $logger = get_logger("MANUALREVERSESYNC");

#Initialise the configuration file
my $cfg = new Config::Simple();
$cfg->read($configPath)
or $logger->logdie( "\nFATAL: " . $cfg->error() . "\nINFO: Please fix the issue and restart the manual reverse sync program. Exiting.\n\n" );

#reverse sync syncs from Mx9 - Mx8 so swap the keys around

my $srcMasterHost = $cfg->param('mx_dirserv_host');
my $srcMasterPort = $cfg->param('mx_dirserv_port');
my $srcMasterPass = $cfg->param('r_destMasterPass');
my $srcMasterBind = $cfg->param('r_destMasterBind');

my $destMasterHost = $cfg->param('mx_dirserv_host_src');
my $destMasterPort = $cfg->param('mx_dirserv_port_src');
my $destMasterPass = $cfg->param('mx_nonroot_dirserv_pwd_src');
my $destMasterBind = $cfg->param('mx_nonroot_dirserv_bind_src');

my $popproxy  = $cfg->param('r_popProxyHost');
my $imapproxy = $cfg->param('r_imapProxyHost');
my $smtprelay = $cfg->param('r_smtpProxyHost');
my $mstore    = $cfg->param('r_defaultMsgStoreHost');
my $useDefaultCos    = defined $cfg->param('r_useDefaultCos') ? $cfg->param('r_useDefaultCos') : "false";
my $defaultCos = $cfg->param('r_defaultCos');

my $manualReverseSyncDir = defined $cfg->param("manualreversesyncDir") ? $cfg->param('manualreversesyncDir') : "manual-reverse-sync";
my $SDEBUG = $cfg->param('log_debug');
my $manualSyncInputFile;
mkdir $manualReverseSyncDir unless -d $manualReverseSyncDir;
my ($successFileName, $failureFileName) = generateLogFileNames();

######################################################
### USAGE
########################################################
sub usage {
	print STDERR qq(
	Utility to update attributes in SUR
	Command line to run script:
	manualReverseSync.plx -inputfile
    where the file containing email addresses (one per line) of accounts exist only in Mx9 but not Mx8.
	);

	exit(0);
}

sub generateLogFileNames {
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
    $year += 1900;
    $mon += 1;
    my $timeStamp = sprintf("%04d%02d%02d%02d%02d%02d", $year, $mon, $mday, $hour, $min, $sec);
    return ($manualReverseSyncDir . "/success." . $timeStamp, $manualReverseSyncDir . "/failed." . $timeStamp);
}

sub fileAppend {
    my ($changeLogLine,$changeLogFileLocation) = @_;
    open(my $changeLogFile, ">>$changeLogFileLocation");
    print $changeLogFile "$changeLogLine\n";
    close $changeLogFile;
}

sub printAddLdifArray {
    my ($newLdapArray_ref, $fullDn, $filenamePrefix) = @_;
    my @newLdapArray = @{$newLdapArray_ref};
    my $ldifString = $fullDn . "\n";

    while (@newLdapArray) {
        my $newLdapArrayElement = shift (@newLdapArray);
        if ($newLdapArrayElement =~ /senderscontrol/) { # ignore
        } elsif ($newLdapArrayElement =~ /objectclass/) {
            $_ = shift (@newLdapArray);
            my @objectClass = @{$_};
            foreach (@objectClass) {
                #print "objectclass: $_\n";
                $ldifString .= "objectclass: $_\n";
            }
        } else {
            my $next = shift (@newLdapArray);
            #print ("$newLdapArrayElement: $next\n");
            $ldifString .= "$newLdapArrayElement: $next\n";
        }
    }

    fileAppend($ldifString, $filenamePrefix.".ldif");
}

# Descript: Testing for the original mailboxstatus
# Input:    Original mailboxstatus
# Return:   P
sub process_mailboxstatus {
    my ($line) = @_;
    $logger->info("SCRIPT|original mailboxstatus value = '$line' and returning 'P'");
    return "P" ;
}

# Descript: Verify the password type.
# Input:    line from change log or LDAP record
# Return:   the correct password type for the target system.
sub process_mailpasswordtype {
    my ($line) = @_;
    if ( $line =~ /^2$/i ) {
        $logger->info("SCRIPT|Custom password type to SSHA");
        return "S";
    } else {
        return $line;
    }
}

# Descript: Process the Class of Services (CoS)
# Input:    line from change log or LDAP record
# Return:   the correct password type for the target system.
sub process_CoS {
    my ($line) = @_;
    if ( $useDefaultCos eq "true" ) {
        #Set all COSs to the default as per the migration.conf regardless.
        $line = $defaultCos;
    } 
    return $line;
}

sub ldapBind {
    my ($host,$port,$bindID,$bindPass) = @_;
        my $ldap = Net::LDAP->new ( $host, port => $port );
    if (! $ldap) {
        $logger->warn("Connect to host = '$host' on port '$port' failed.");
        return 0;
    }

    my $mesg = $ldap->bind ( $bindID, password => $bindPass );

    if ( $mesg->code ne '0' ) {
        my $code = $mesg->code;    
           $logger->warn("Connect to host = '$host' on port '$port' with bindID = '$bindID' failed.");  # Do not include the password in the log file.      
        return 0;
    } else {
        return $ldap;
    }
}

sub processLdap {
    my ($ldapSource, $ldapDest, $email) = @_;   
    my $attrsList = [ ]; # get all attributes
    my ($msgStoreHost, $autoReplyHost, $passwordTypeSave, $passwordSave);
    my $targetdn;
    my @newLdapRecord;
    my @objectClass;
    
    my $mesg = $ldapSource->search( # perform a search
                    base   => "",
                    scope   => "sub",                        
                    filter => "(mail=$email)",
                    attrs => $attrsList
                  );

    if ($mesg->code) {
        my $errMsg = $mesg->error;
        $logger->info("SCRIPT|Error = '$errMsg' received while ldapsearch on '$email'");
        return 1;
    } elsif ($mesg->count == 0) {
        $logger->info("SCRIPT|Error no results received while ldapsearch on '$email'");
        return 1;
    } else {
        my ( $msgStoreHost, $autoReplyHost);
        $msgStoreHost = $mstore;
        $autoReplyHost = $mstore;
        
        my $href = $mesg->as_struct;
        my @arrayOfDNs  = keys %$href;        # use DN hashes
        foreach ( @arrayOfDNs ) {
            $targetdn = $_;
            #print "DN = $targetdn\n";
            my $valref = $$href{$_};            
            # get an array of the attribute names passed for this one DN.
            my @arrayOfAttrs = sort keys %$valref; #use Attr hashes
            my $attrName;        
            foreach $attrName (@arrayOfAttrs) {
                next if ( $attrName =~ /;binary$/ );
                # get the attribute value (pointer) using the attribute name as the hash
                my $attrVal =  @$valref{$attrName};
                #print "\t $attrName: @$attrVal \n";                  
                if ( $attrName =~ /^adminpolicydn/i ) {
                    $_ = process_CoS(@$attrVal);
                    push (@newLdapRecord, $attrName);
                    push (@newLdapRecord, $_);                 
                } elsif ( $attrName =~ /^mailautoreplyhost/i ) {
                    push (@newLdapRecord, $attrName);
                    push (@newLdapRecord, $autoReplyHost);                     
                } elsif ( $attrName =~ /^mailmessagestore/i ) { 
                    push (@newLdapRecord, $attrName);
                    push (@newLdapRecord, $msgStoreHost);                
                } elsif ( $attrName =~ /^mailboxstatus/i ) {
                    my $mailboxstatus = process_mailboxstatus(@$attrVal);
                    push (@newLdapRecord, $attrName);
                    push (@newLdapRecord, $mailboxstatus);
                } elsif ( $attrName =~ /^mailpasswordtype/i ) {
                    ($passwordTypeSave) = @$attrVal;
                } elsif ( $attrName =~ /^mailpassword$/i ) {
                    ($passwordSave) = @$attrVal;
                } elsif ( $attrName =~ /^netmailmsgviewprivacyfilter/i ) {
                    # Ignore
                } elsif ( $attrName =~ /^mailrealm/i ) { 
                    # Ignore                   
                } elsif ( $attrName =~ /^objectclass/i ) {
                    foreach ( @$attrVal ) {
                        # Use all objectclasses except the followings
                        if (($_ =~ /^pabuser$/i) ||
                            ($_ =~ /^pabuserprefs$/i) ||
                            ($_ =~ /^caluser$/i) ||
                            ($_ =~ /^caluserprefs$/i) ||
                            ($_ =~ /^smsuserprefs$/i)) {
                            # Ignore
                        } else {
                            push (@objectClass, $_);
                        }
                    }
                } elsif ( $attrName =~ /^cn$/i ) { 
                    push (@newLdapRecord, $attrName);
                    push (@newLdapRecord, @$attrVal);
                    push (@newLdapRecord, "mailfrom");
                    push (@newLdapRecord, @$attrVal);
                } elsif (( $attrName =~ /^sn/i ) || ( $attrName =~ /^mail$/i ) || ( $attrName =~ /^mailalternateaddress/i ) | ( $attrName =~ /^mailautoreplymode/i ) || ( $attrName =~ /^maildeliveryoption/i ) || ( $attrName =~ /^maillogin/i ) || ( $attrName =~ /^mailboxid/i )) {
                    push (@newLdapRecord, $attrName);
                    push (@newLdapRecord, @$attrVal);     
                }
                
            }

            if ( $passwordTypeSave =~ /b/i ) { # If the password type is "bcrypt" type
                my $clearTextPassword = `/opt/owm/migration/migration_tool/getAuth.plx $email`;
            
                if ($clearTextPassword) {
                    my $ssha1Password = `/opt/owm/imail8/Mx8.1/bin/impwdhash -a ssha1 $clearTextPassword`;
                    $logger->info("SCRIPT|Successfully found and convert a text password for '$email'.");
                    push (@newLdapRecord, "mailpasswordtype");
                    push (@newLdapRecord, "2");
                    push (@newLdapRecord, "mailpassword");
                    push (@newLdapRecord, $ssha1Password);
                } else {
                    $logger->info("SCRIPT|Error cannot find a text password for '$email'.");
                    push (@newLdapRecord, "mailpasswordtype");
                    push (@newLdapRecord, "2");
                    push (@newLdapRecord, "mailpassword");
                    push (@newLdapRecord, "abcdefghijklmnopqrstuvwxyz1234567890123456789012");  # a known invalid password
                }
            } else {
                $logger->info("SCRIPT|Password type of '$passwordTypeSave' has been detected.  Use the same password for '$email'.");
                push (@newLdapRecord, "mailpasswordtype");
                push (@newLdapRecord, $passwordTypeSave);
                push (@newLdapRecord, "mailpassword");
                push (@newLdapRecord, $passwordSave);
            }


        }  
        #foreach ( @newLdapRecord ) {
        #    print "\t$_\n";
        #}
        #unless ( $mailboxstatusFlag == 1 ) {
        #create the new mailbox here
        unshift (@newLdapRecord, \@objectClass);
        unshift (@newLdapRecord, "objectclass");    
        push (@newLdapRecord, "mailpopproxyhost");
        push (@newLdapRecord, $popproxy);
        push (@newLdapRecord, "mailimapproxyhost");
        push (@newLdapRecord, $imapproxy);
        push (@newLdapRecord, "mailsmtprelayhost");
        push (@newLdapRecord, $smtprelay);
        push (@newLdapRecord, "netmailcurrentver");
        push (@newLdapRecord, "4.0.5");
        push (@newLdapRecord, "description");
        push (@newLdapRecord, "created on Mx9");
       
        my $mesg = $ldapDest->add ($targetdn,
                attrs => \@newLdapRecord
                );
            
        my ($errMsg, $logMessage);        
        if ($mesg->code) {
            $errMsg = $mesg->error;
            $logMessage = "Received '$errMsg' error while trying to create $targetdn";
            $logger->info("SCRIPT|$logMessage");
            fileAppend($email . " " . $logMessage,$failureFileName);            
            printAddLdifArray(\@newLdapRecord, $targetdn, $failureFileName);
            return 1
        } else {
            $logMessage = "Successfully created '$email' on the destination server";
            fileAppend($email . " " . $logMessage,$successFileName); 
            #printAddLdifArray(\@newLdapRecord, $targetdn, $successFileName);
            $logger->info("SCRIPT|$logMessage");
            return 0;
        }        
        #}
    } # end ELSE statement
}

GetOptions( '-d', \$SDEBUG,    # Override debug in config file.
    '-inputfile=s', \$manualSyncInputFile # kent
) || usage();


$logger->info("SCRIPT|Opening '$manualSyncInputFile' input file.");
unless ( open( CHG, "< $manualSyncInputFile" ) ) {
    my $errMessage = "Could not open file '$manualSyncInputFile' and now exiting!";
    print "$errMessage\n";
    $logger->info("$errMessage");
    exit 1;
}

my $ldapSource = ldapBind($srcMasterHost,$srcMasterPort,$srcMasterBind,$srcMasterPass);
my $ldapDest = ldapBind($destMasterHost,$destMasterPort,$destMasterBind,$destMasterPass);
exit 2 unless ($ldapSource && $ldapDest);

my ($email, $ldapResults);
    
while (<CHG>) { ### Main loop ###
    chomp;
    $email = $_;
    
    #print "email=>$email<=\n";
    my $ldapStatus = processLdap($ldapSource, $ldapDest, $email);
   
}

$ldapSource->unbind;
$ldapDest->unbind;
exit 0;
