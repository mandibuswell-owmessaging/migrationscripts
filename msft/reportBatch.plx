#!/usr/bin/perl
###########################################################################
#
#  Copyright 2014 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Andrew Perkins <andrew.perkins@owmessaging.com>
#  Date:        4Q/2014
#  Version:     1.0.0 - Sept 17, 2014
#
#  Version History:
#  1.1 Jul 6 2015 : Mandi Buswell : Modify to use Log4perl for this scripts logging.
#                   Have not modified the logging of the other modules
#
#
#  Supported Statuses
#  P/p � Account data load success / fail
#  V/v � Vacation message data load success / fail
#  S/s � Signature data load success / fail
#  A/a � Address book data migrated success / fail
#  M/m � Mail migration success / fail
###########################################################################

###########################################################################
# 'use' modules
###########################################################################
#use lib "/opt/owm/imail/perl/lib";
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use lib "$FindBin::Bin/migperl"; # The location of the cpan perl modules for migration
use lib "$FindBin::Bin/migperl/lib/perl5";

use strict;
use warnings;
use Getopt::Long;   
use com::owm::migBellCanada;
use Log::Log4perl qw(get_logger :levels);
use Log::Dispatch::FileRotate;
use Log::Log4perl::Level;

# A package that provides an easy way to have a config file
use Config::Simple;

# Needed for LDAP search.
use Net::LDAP;

# A package that provides some status for our program
use Term::ProgressBar;

###########################################################################
# Globel variables
###########################################################################

$| = 1;    ## Makes STDOUT flush immediately

if ( -e '/var/tmp/myscript.pid' ) {
	print "dumpsetup is running";
	exit 1;
}

my $SDEBUG; #MCB: use SDEBUG so as not to interfer with imported log4perl scalars 

our $configPath = 'conf/migrator.conf';
our $configLog = 'conf/log.conf';

# Logging object.
Log::Log4perl->init($configLog);
our $log = get_logger("MSFT.REPORT");

# Config object.
# And read the config file so all subs have access to params.
my $cfg  = new Config::Simple();
$cfg->read($configPath)
  or $log->logdie("SCRIPT|FATAL: ".$cfg->error()."INFO: Please fix the issue and restart the migration program. Exiting.");

# Catch Terminate and interupt signals
$SIG{INT}  = sub { $log->logdie("SCRIPT|SCRIPT INTERRUPT SIGNAL HAS BEEN CAUGHT... processing stopped before completion."); };
$SIG{TERM} = sub { $log->logdie("SCRIPT|SCRIPT INTERRUPT SIGNAL HAS BEEN CAUGHT... processing stopped before completion."); };

# Varables for progress bar
my $progress;
my $count      = 0;
my $startcount = 0;

######################################################
# USAGE
######################################################
sub usage {

	print STDERR qq(

  Utility to report on a batch of migrated user's from MSFT to Email Mx platform.

  Command line to run script:

    reportBatch.plx -puidFile <puidFile> -batchID <batchID> [-d]

    where:
    <puidFile>     is the file of PUIDs whose accounts will be reported on.
	<batchID>		is the unique batch id 

    Switches:
    -d              turn debug on.

);

	exit(0);
}
############################################################
# - writeFile -                                            #
# Setup report dir and open a write file                   #
############################################################
sub writeFile($$$) 
{
	my $reportFile = shift;
	my $reportString = shift;
	my $batchID = shift;
	my $reportDir = $cfg->get_param('report_directory');

  # Create our report dir if it doesn't exist
  my $reportFileDir = $reportDir . "/" . $batchID;
  unless ( -e $reportFileDir ) {
    my $err = mkdir( $reportFileDir, 0777 );
    if ( $err != 1 ) {
      $log->error( "Couldn't create report dir, $reportDir\n");
    }
  }

    my $reportFullname = $reportFileDir. "/" . $reportFile;
	open (FH, ">$reportFullname") or $log->logdie("$reportFullname | can't open file  $!");
	print FH $reportString;
	close FH;

}
###############################################################
# makeReport
###############################################################
sub makeReport($$$$)
{
  my $puidFile = shift;
  my $batchID = shift;
  my $cfg = shift;
  my $log = shift;

  # Counts...
  my $countSuccess = 0;
  my $countTotal = 0;
  my $countUserListFail = 0;
  my $countForwardingFail = 0;
  my $countVacationFail = 0;
  my $countSignatureFail = 0;
  my $countAcountFail = 0;
  my $countMailboxFail = 0;
  my $countContactsFail = 0;
  my $countCalendarFail = 0;
  my $countNameFail = 0;
  my $countMailFail = 0;
  my $countMismatchFail = 0;
  my $countInterruptFail = 0;
  my $countMigstatusNotDefined = 0;

  # Strings of emails and puids for each catagory.
  my $stringSuccess = '';
  my $stringUserListFail = '';
  my $stringForwardingFail = '';
  my $stringVacationFail = '';
  my $stringSignatureFail = '';
  my $stringAccountFail = '';
  my $stringMailboxFail = '';
  my $stringContactsFail = '';
  my $stringMailFail = '';
  my $stringCalendarFail = '';
  my $stringNameFail = '';
  my $stringMismatchFail = '';
  my $stringInterruptFail = '';
  my $stringMigstatusNotDefined = '';

  open my $PUIDFILE, "<", $puidFile
    or $log->logdie("SCRIPT|Unable to open file $puidFile: $!") && print("Unable to open file $puidFile: $!");

  while(my $puid = <$PUIDFILE>) {
    chomp($puid);
    $log->debug("PUID|$puid:Counting...");

    $countTotal++;

    # Lookup account info using PUID.
    (my $status,my $email,my $mailboxstatus,my $migstatus,my $consentstatus,my $maillogin) = com::owm::migBellCanada::lookupPUID($puid,$cfg,$log);

    if ( ! $migstatus ) {
      $log->error("$email|$puid:migstatus not defined");

      # As migstatus is not defined, count toward
      $countMigstatusNotDefined++;
      $stringMigstatusNotDefined .= "$email,$puid\n";
      next;
    }

    if ( $status eq $com::owm::migBellCanada::STATUS_SUCCESS ) {
      # The lookupPUID was successful.
      $log->debug("$email|$puid:status = $status, email = $email, mailboxstatus = $mailboxstatus, migstatus = $migstatus ");
    } else {
      $log->error("$email|$puid:$status:Failed to get account info using puid");
      $log->error("$email|$puid:Stop processing.");
      next;
    }

    if ( $mailboxstatus eq 'A' ) {
      $log->debug("$email|$puid:mailbox test 2 - migstatus=$migstatus, mailboxstatus=$mailboxstatus");
      $countSuccess++;
      $stringSuccess .= "$email,$puid\n";
    }

    # User successfully migrated all data but mailbox still in Proxy mode...
    if ( ($migstatus !~ /(ufvspcan)/) && ( $mailboxstatus eq 'P') ) {
      $log->debug("$email|$puid:mailbox test 1 - migstatus=$migstatus, mailboxstatus=$mailboxstatus");
      $countMailboxFail++;
      $stringMailboxFail .= "$email,$puid\n";
    }
    
    if ($migstatus =~ /u/) {
      $countUserListFail++;
      $stringUserListFail .= "$email,$puid\n";
    }

    if ($migstatus =~ /f/) {
      $countForwardingFail++;
      $stringForwardingFail .= "$email,$puid\n";
    }

    if ($migstatus =~ /v/) {
      $countVacationFail++;
      $stringVacationFail .= "$email,$puid\n";
    }

    if ($migstatus =~ /s/) {
      $countSignatureFail++;
      $stringSignatureFail .= "$email,$puid\n";
    }

    if ($migstatus =~ /p/) {
      $countAcountFail++;
      $stringAccountFail .= "$email,$puid\n";
    }

    if ($migstatus =~ /c/) {
      $countCalendarFail++;
      $stringCalendarFail .= "$email,$puid\n";
    }

    if ($migstatus =~ /a/) {
      $countContactsFail++;
      $stringContactsFail .= "$email,$puid\n";
    }

    if ($migstatus =~ /n/) {
      $countNameFail++;
      $stringNameFail .= "$email,$puid\n";
    }

    if ($migstatus =~ /x/) {
      $countMismatchFail++;
      $stringMismatchFail .= "$email,$puid\n";
    }

    if ($migstatus =~ /i/) {
      $countInterruptFail++;
      $stringInterruptFail .= "$email,$puid\n";
    }

  }

 writeFile('batch.successful',$stringSuccess,$batchID);
 writeFile('batch.userlist.failed',$stringUserListFail,$batchID);
 writeFile('batch.mailbox.failed',$stringMailboxFail,$batchID);
 writeFile('batch.forwarding.failed',$stringForwardingFail,$batchID);
 writeFile('batch.vacation.failed',$stringVacationFail,$batchID);
 writeFile('batch.signature.failed',$stringSignatureFail,$batchID);
 writeFile('batch.account.failed',$stringAccountFail,$batchID);
 writeFile('batch.contacts.failed',$stringContactsFail,$batchID);
 writeFile('batch.calendar.failed',$stringCalendarFail,$batchID);
 writeFile('batch.name.failed',$stringNameFail,$batchID);
 writeFile('batch.mismatch.failed',$stringMismatchFail,$batchID);
 writeFile('batch.interrupted',$stringInterruptFail,$batchID);
 writeFile('batch.migstatusNotDefined',$stringMigstatusNotDefined,$batchID);

  my $date = `date`;

  my $reportSumary = qq(
Batch Migration Report
Batch input file: $puidFile
Report date and time: $date

Total number of accounts: \t$countTotal
Successful migrated accounts: \t$countSuccess
Users' Lists data failed: \t$countUserListFail
Mail Forwarding data failed: \t$countForwardingFail
Vacation data failed: \t\t$countVacationFail
Signatures data failed: \t$countSignatureFail
Account data failed: \t\t$countAcountFail
Mailbox data failed: \t\t$countMailboxFail
Contacts data failed: \t\t$countContactsFail
Calendar data failed: \t\t$countCalendarFail
Name data failed: \t\t$countNameFail
Password retrieval failed: \t$countMismatchFail
Not processed: \t\t\t$countInterruptFail
Migstatus not defined: \t\t$countMigstatusNotDefined

);

  close($PUIDFILE);

  return($reportSumary);
}

######################################################
# ---------------------------------------------------
# Start Main
# ---------------------------------------------------
######################################################

$SDEBUG = $cfg->param('log_debug');
my $puidFile = '';
my $batchID = "";

GetOptions( '-d',\$SDEBUG,               # Override debug in config file.
            '-puidFile=s',\$puidFile,
            '-batchID=s',\$batchID
            ) || usage();


# Debug out info
if ($SDEBUG) {
  print("Running in debug mode. \n");
  $log->level($DEBUG); 
}

# Check input file exists
if ( -e $puidFile ) {
  $log->info("SCRIPT|Processing  input file: $puidFile");
} else {
  $log->error("SCRIPT| input file is missing: $puidFile");
  usage();
} 

# Start generating the report.
my $reportSummary = makeReport($puidFile,$batchID,$cfg,$log);
print($reportSummary);


# Exit script gracefully.
print "\n";    # This just makes things look clean

######################################################
# ---------------------------------------------------
# End Main
# ---------------------------------------------------
######################################################

######################################################
# ---------------------------------------------------
# BEGIN POD Documentation
# ---------------------------------------------------
######################################################

__END__

=pod

=head1 NAME

migrator.plx - Migration Utility

=head1 DESCRIPTION

=cut

exit 0;
