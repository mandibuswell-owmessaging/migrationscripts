#!/bin/bash
#
# Telstra, d363756, 22.6.2016
#
# History
#	14.3.2016, d363756: First draft of the script
#	26.3.2016, d363756: Update for production use
#	26.4.2016, d363756: Introduced script VERSION string for tracking and logging
#	16.5.2016, d363756: Limit job downloads only to those produced on the day of the run
#	24.5.2016, d363756: Log some stats. Email the log of the script run.
#	16.6.2016, d363756: Run processConsent for mailboxes/accounts in retreived/downloaded job files
#	22.6.2016, d363756: Ensure processConsent runs only for Windows Live mailboxes and only once per job file
#
# Purpose
#	Download all files that include the hostname of the server running the script (pattern matching)
#	to a nominated direcory. Filename format of files that need to be transferred:
#		<TIMESTAMP>-<MIGRATION SERVER>-<JOBID>.csv
#		TIMESTAMP: YYYYMMDDHHmmSS
#		MIGRATION SERVER: hostname of the server the job is assigned to (file transferred to)
#		JOBID: Identifier of the job (created when the job is created), 7-digit long with leading zeroes when required.
#
#		Example filename (for a job with ID 21 created on 14.8.2015 at 11:47:32 and assigned to migration server nsstlmig01p
#		20150814114732-nsstlmig01p-0000021.csv
#
# Use:
#  getMigrationJobFiles.sh [Hostname]
#
# Example:
#	getMigrationJobFiles.sh `hostname -s`
#
VERSION="v.20160622.1"
HOST="$(hostname -s)"

# For debugging
INTERACTIVE="NO"
#INTERACTIVE="YES"
EMAILRESULTS="YES"

# Functions
# err_out <ERROR CODE> <SCRIPT LINE> <ERROR MESSAGE>
err_out() {
	echo "$(date '+%F %T'),ERROR,Code=${1} Line=${2} Message=${3}" >> ${LOGFILE}
	#[[ $- == *i* ]] && echo "$(date '+%F %T'),ERROR,Code=${1} Line=${2} Message=${3}"
	[[ ${INTERACTIVE} == "YES" ]] && echo "$(date '+%F %T'),ERROR,Code=${1} Line=${2} Message=${3}"
}

# log_out <LOG TYPE> <MESSAGE>
log_out() {
	echo "$(date '+%F %T'),${1},${2}" >> ${LOGFILE}
	[[ ${INTERACTIVE} == "YES" ]] && echo "$(date '+%F %T'),${1},${2}"
}

# Set the variables
COMMANDLINE="$0"

# Openwave migration tools
TIMESTAMP="$(date +%Y%m%d%H%M%S)"
WORKDIR="/opt/owm/migration/migration_tool"
TMPFILE="${WORKDIR}/tmp/${TIMESTAMP}-$(basename "$0" .sh).$$"
LOGFILE="${WORKDIR}/log/$(basename "$0" .sh).log"
TODAY="$(date -d today '+%Y%m%d')"
YESTERDAY="$(date -d yesterday '+%Y%m%d')"

LOCALUSER="imail"
LOCALDIR="${WORKDIR}/batchfiles/watcher/scheduled"
REMOTEUSER="CE2_Generic"
REMOTEDIR="Outbound"
# Production blade-03 IP address is 10.108.64.239
SFTPSERVER="10.108.64.239"

# Send script results in an email?
MAILCMD="/bin/mail"
MAILFROM="milan.ristic@team.telstra.com"
MAILTO="milan.ristic@team.telstra.com"
#SUBJECT="${HOST}:${COMMANDLINE}"
SUBJECT="${COMMANDLINE}"
SMTPSERVER="mta-vip"

# Let's go
log_out "START" "${HOST}:${COMMANDLINE} (${VERSION})"
log_out "INFO" "INTERACTIVE=${INTERACTIVE}"
log_out "INFO" "WORKDIR=${WORKDIR}"
log_out "INFO" "LOGFILE=${LOGFILE}"
log_out "INFO" "SFTPSERVER=${SFTPSERVER}"
log_out "INFO" "REMOTEDIR=${REMOTEDIR}"

log_out "SFTP" "Looking for files to download ${REMOTEUSER}@${SFTPSERVER}:${REMOTEDIR}/${TODAY}*${HOST}*.csv to ${HOST}:${LOCALDIR}"

# Collect the list of files to download
sftp ${REMOTEUSER}@${SFTPSERVER} <<-EOF >  ${TMPFILE}
cd ${REMOTEDIR}
ls -1 ${TODAY}*${HOST}*.csv
quit
EOF
STATUS=$?
log_out "SFTP" "STATUS=${STATUS}"

TODOWNLOAD=$(awk "/^${TODAY}/ && /-${HOST}-/ && /\.csv/" ${TMPFILE})

# If there are no files to download, exit. If thre are, then download and process
#if [[ ${STATUS} -eq 0 ]]
if [[ ${TODOWNLOAD} == "" ]]
then
	log_out "SFTP" "TODOWNLOAD=${TODOWNLOAD}, no files to download"
	# Cleanup and finish
	log_out "CLEANUP" "Removing TMPFILE ${TMPFILE}"
	rm -f ${TMPFILE}
	log_out "FINISH" "${COMMANDLINE}"
	exit 0
else
	# Download, delete and process
	for f in ${TODOWNLOAD}
	do
		# Download
		sftp ${REMOTEUSER}@${SFTPSERVER} <<-EOF >>  ${TMPFILE}
		cd ${REMOTEDIR}
		get ${f} ${LOCALDIR}
		quit
		EOF
		DOWNLOADED="${LOCALDIR}/${f}"

		# Delete
		if [[ -f "${DOWNLOADED}" ]]
		then
			RECORDS="$(wc -l ${DOWNLOADED})"
			log_out "SFTP" "DOWNLOADED=${DOWNLOADED}, RECORDS=${RECORDS}"
			log_out "SFTP" "Deleting ${SFTPSERVER}:${REMOTEDIR}/${f}"
			sftp ${REMOTEUSER}@${SFTPSERVER} <<-EOF >>  ${TMPFILE}
			cd ${REMOTEDIR}
			# Delete here
			rm ${f}
			quit
			EOF
		else
			log_out "SFTP" "${DOWNLOADED} file not found! Leaving (not deleting) file at source ${SFTPSERVER}:${REMOTEDIR} (${DOWNLOADED})"
			log_out "INFO" "processConsent cannot run fo ${DOWNLOADED}"
		fi

		# Run process consent script for the downlaoided file, but only for WL records
		awk -F, '$1 ~ /@/ && toupper($2)=="WL"' ${DOWNLOADED} > ${TMPFILE}
		RECORDS="$(wc -l ${TMPFILE})"
		# Refresh consent tokens (they may have expired or even never retreived)
		cd ${WORKDIR}
		CMD="./processConsent.plx -r -puidFile ${TMPFILE}"
		log_out "PROCESSING" "RECORDS=${RECORDS}, CMD=${CMD}"
		${CMD}
	done
fi

# cd ${LOCALDIR}
# DOWNLOADED="$(ls ${TODAY}*${HOST}*.csv | awk '{strFiles=strFiles OFS $1;} END {print strFiles;}')"
# for f in ${DOWNLOADED}
# do
# 	log_out "SFTP" "DOWNLOADED=${f} RECORDCOUNT=$(awk 'BEGIN {i=0;} /@/ {i++;} END {print i;}' ${f})"
# done

# Cleanup
log_out "CLEANUP" "Removing TMPFILE ${TMPFILE}"
rm -f ${TMPFILE}
#echo "Check ${TMPFILE} (not removed for debugging purposes)"

# Done
log_out "FINISH" "${COMMANDLINE}"

if [[ ${EMAILRESULTS} == "YES" ]]
then
	grep ^$(date '+%F') ${LOGFILE} | ${MAILCMD} -v -s "${SUBJECT}" -S smtp="${SMTPSERVER}" -r "${MAILFROM}" "${MAILTO}"
fi
