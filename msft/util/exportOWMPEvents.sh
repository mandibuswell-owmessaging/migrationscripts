#!/bin/bash
#
# exportOWMPEvents.sh
#
# Telstra, d363756, 9.5.2016
#
# History:
#  29.11.2015, d363756: First draft
#  20.3.2016,  d363756: Read DB connection parameters from the ...migration_tool/conf/migrator.conf file
#  20.3.2016,  d363756: Tweaked event exporets to suit SDR import logic
#  2.4.2016,   d363756: Updates based on info avaialable in the event log DB afer internal trials
#  19.4.2016,  d363756: Move event_id's for preload events to the (new) prelaod event range
#  21.4.2016,  d363756: Leaving the files uncompressed in $OUTDIR directory, /opt/owm/migration/eventReports/
#  4.5.2016,   d363756: Adding events 112 (BatchFileStart) and 113 (ProcessConsent) to the export
#  9.5.2016,   d363756: Adding events 102 (MigrationRunStart) and 103 (MigrationRunEnd) to the export to capture preloads
#
# Purpose:
#  Export OWMP migration.events table into a file for Migration Support Solution (MSS)
#  Export file is processed in SDR, and data loaded to Migration Support Database (MSD)
#
# Prerequisite:
#  Valid user has to exit to execute SQL statement on the migration.events table
#    mysql> CREATE USER 'exportevents4mss'@'localhost' IDENTIFIED BY 'ndxjWD5X@jx_Kc';
#    mysql> GRANT SELECT ON migration.events TO 'exportevents4mss'@'localhost';
#
# Use:
#  exportOWMPEvents.sh [JobID]
#
# Files:
#  exportOWMPEvents.timestamp
#
# Output:
#  stdout: Console messages (errors and results)
#  OWMPEvents-<TIMESTAMP>.dat.gz: Export file
#    TIMESTAMP: YYYYDDMMHHmmSS
#    File iz compressed (gz)
#    Example filename: OWMPEvents-20150814114732.dat.gz
#
#  OWMPEvents-<TIMESTAMP>.dat file structure:
#	<date_time>|<milliseconds>|<migration_host>|<migration_scriptname>|<line_number>|<event_id>|<email_address>|<migration_job_id>|<event_details>
#   Fields:
#     1. <date_time>: Event date and time in yyyy-MM-dd hh:mm:ss format
#     2. <milliseconds>: Milliseconds from the date_time field if avaialble
#     3. <migration_host>: Hostname of the migration server that has produced the event
#     4. <migration_scriptname>: Title of the script that has produced the event
#     5. <line_number>: Line number within the file where the log statement was issued
#     6. <event_id>: Event Identifier. Links to Events table that offers details
#     7. <email_address>: Email address of the account that is being migrated
#     8. <migration_job_id>: Unique Identifier of a migration job
#     9. <event_details>: Details of the event. Large text field
#
#  Sample OWMPEvents-<TIMESTAMP>.dat record:
#    2015-10-14 14:46:53|0|nsdu2mig01t.dlx1.msg.in.telstra.com.au|migrateBatch|310|100|fn1_poc_10895@uat.bigpond.com|001|event_name=MigrationStart, migtype=WL, secondary_id=00034001A9516CB5, status=, message="Start migration"
#
# Example:
#	./exportOWMPEvents.sh 123
#
VERSION="v.20160509.1"

# For debugging
INTERACTIVE="NO"
#INTERACTIVE="YES"

# Functions
# err_out <ERROR CODE> <SCRIPT LINE> <ERROR MESSAGE>
err_out() {
	echo "$(date '+%F %T'),ERROR,Code=${1} Line=${2} Message=${3}" >> ${LOGFILE}
	#[[ $- == *i* ]] && echo "$(date '+%F %T'),ERROR,Code=${1} Line=${2} Message=${3}"
	[[ ${INTERACTIVE} == "YES" ]] && echo "$(date '+%F %T'),ERROR,Code=${1} Line=${2} Message=${3}"
}

# log_out <LOG TYPE> <MESSAGE>
log_out() {
	echo "$(date '+%F %T'),${1},${2}" >> ${LOGFILE}
	[[ ${INTERACTIVE} == "YES" ]] && echo "$(date '+%F %T'),${1},${2}"
}

COMMANDLINE="$0"

# Openwave migration tools
WORKDIR="/opt/owm/migration/migration_tool"
MIGCONF="${WORKDIR}/conf/migrator.conf"
LOGFILE="${WORKDIR}/log/$(basename "$0" .sh).log"
OUTDIR="/opt/owm/migration/eventReports"

# Let's go
log_out "START" "${COMMANDLINE} (${VERSION})"
log_out "INFO" "INTERACTIVE=${INTERACTIVE}"
log_out "INFO" "WORKDIR=${WORKDIR}"
log_out "INFO" "MIGCONF=${MIGCONF}"
log_out "INFO" "LOGFILE=${LOGFILE}"
log_out "INFO" "OUTDIR=${OUTDIR}"

TODAY="$(date -d today '+%Y%m%d')"
YESTERDAY="$(date -d yesterday '+%Y%m%d')"

# ..
MAILX=/bin/mail
MYSQLCLI=/usr/bin/mysql
SED=/bin/sed
GZIP=/bin/gzip

# Event file upload parameters
#UPLOAD="NO" # 19.4.2015, d363756: This is managed in the code, not a static parameter anymore
HOST="$(hostname -s)"
LOCALUSER="imail"
REMOTEUSER="CE2_Generic"
REMOTEDIRS="Inbound/OWMP_EVENTS"
# Production blade-03 IP address is 10.108.64.239
SFTPSERVER="10.108.64.239"

# When script used interactively, offer correction of the command line ...
if [[ "$#" -ne 1 ]]
then
	if [[ ${INTERACTIVE} == "YES" ]]
	then
		echo "Syntax: `basename $0` [<JobID>|date=<DATESTRING>]"
		echo "Examples:"
		echo "  1) ${COMMANDLINE} 12311"
		echo "  2) ${COMMANDLINE} date=20160320"
	fi
	err_out 33 ${LINENO} "Input parameter [<JobID>|date=<DATESTRING>] not provided in the command line"
	log_out "FINISH" "${COMMANDLINE}"
	exit 33
else
	ISDATE=$(echo $1 | awk -F"=" '{print toupper($1);}')
	if [[ ${ISDATE} == "DATE" ]]
	then
		EXPORTDATE=$(echo $1 | awk -F"=" '{if (length($2)==8) {print toupper($2);}}')
		EXPORTDATE="${EXPORTDATE:-$(date '+%Y%m%d')}"
		SQLFILTER="date(create_time)='${EXPORTDATE}'"
		log_out "INFO" "Exporting migration events for the date of ${EXPORTDATE}"
		log_out "INFO" "Base SQL Filter: ${SQLFILTER}"
		UPLOAD="NO"
	else
		JOBID=$1
		#SQLFILTER="migration_job_id=${JOBID} AND primary_id IS NOT NULL AND primary_id != '' AND (event_id IN (100,115,117,118,162,185,190,197,198,199) OR (event_id IN (101) AND UPPER(migtype) IN ('WL', 'MX8', 'MYI')))"
		SQLFILTER="migration_job_id=${JOBID}"

		log_out "INFO" "Exporting migraton events for job ${JOBID}"
		log_out "INFO" "Base SQL Filter: ${SQLFILTER}"
		UPLOAD="YES"
	fi
fi

# Import operating parameters from the migration configuration file, if possible
if [[ -f ${MIGCONF} ]]
then
	DB="$(awk -F"=" '/^mysql_db=/ {print $2;}' ${MIGCONF})"
	DBUSER="$(awk -F"=" '/^mysql_user=/ {print $2;}' ${MIGCONF})"
	DBPASSWORD="$(awk -F"=" '/^mysql_pass=/ {print substr($2,2,length($2)-2);}' ${MIGCONF})"
	DBSERVER="$(awk -F"=" '/^mysql_host=/ {print $2;}' ${MIGCONF})"
	log_out "INFO" "Read DB parameters from ${MIGCONF}: DB=${DB} DBUSER=${DBUSER} DBSERVER=${DBSERVER}"
else
	# If the configm file can't be found, use static config values
	DB="migration"
	DBUSER=exportevents4mss
	DBPASSWORD="ndxjWD5X@jx_Kc"
	DBSERVER=viclamigi01p.bpe.nexus.telstra.com.au
	log_out "WARNING" "${MIGCONF} not found. Using deafult DB settings: DB=${DB} DBUSER=${DBUSER} DBSERVER=${DBSERVER}"
fi

#
# mysql -uexportevents4mss -pndxjWD5X@jx_Kc --batch --skip-column-names < exportOWMPEvents_20151129.sql | sed "s/\t/\|/g" | head
#
MYSQLCMDPARAMS="--batch --skip-column-names"
#
# Output filename is OWMPEvents-<TIMESTAMP>.dat.gz, where TIMESTAMP format is "YYYYDDMMHHmmSS" and the file si compressed (gz)
# Posiblity that simultaneous runs of the script may result in the output file of the same name was considered, and that
#   should not be a problem, as long as the file is appended rather than overwritten. However, a simple test is done
#   to tray to avoid such a situation, and try to write to a file exlusively in any run of the script.
# Use different filenames for job-based event export and those for one whole day
if [[ ${UPLOAD} == "YES" ]]
then
	OUTFILE="${OUTDIR}/OWMPEvents-$(date +%Y%m%d%H%M%S).dat"
	if [[ ! -e "${OUTFILE}" ]] ; then
	  cat /dev/null > "$OUTFILE"
	else
	  # $RANDOM returns a different random integer at each invocation.
	  # Nominal range: 0 - 32767 (signed 16-bit integer)
	  # Create another filename after a random delay of up to 3 seconds
	  sleep $[($RANDOM % 3)].$[($RANDOM % 1000)+1]s
	  OUTFILE="${OUTDIR}/OWMPEvents-`date +%Y%m%d%H%M%S`.dat"
	  cat /dev/null > "$OUTFILE"
	fi
else
	OUTFILE="${OUTDIR}/migration-events-${EXPORTDATE}.dat"
	# Make sure export replaces the prevous file
	cat /dev/null > ${OUTFILE}
fi
log_out "OUTPUT" "Exporting events to ${OUTFILE}"

TMPFILE="/tmp/$(basename $0 .sh)_$(date +%Y%m%d).$$"
UXLOGIN="`whoami`"
UXNAME="`getent passwd ${UXLOGIN} | cut -d ':' -f 5`"
UXHOST="`hostname`"
DATE=`date '+%Y-%m-%d'`
#
#MAILFROM="${UXNAME} <${UXLOGIN}@${UXHOST}>"
MAILFROM="exportOWMPEvents <${UXLOGIN}@${UXHOST}>"
REPLYTO="milan.ristic@team.telstra.com"
#SUBJECT="Migration event log file exported"
SUBJECT="${COMMANDLINE}"
MAILTO="milan.ristic@team.telstra.com"
#BCC="dABCDEF@team.telstra.com"
BCC="milan.ristic@team.telstra.com"
#MAILHEADERS="$(echo -e "${SUBJECT}\nFrom: ${MAILFROM}\nReply-to: ${REPLYTO}\nContent-Type: text/html\n")"
SMTPSERVER="mta-vip"

# Make sure the temporary file is clean
echo "" > ${TMPFILE}
date >> ${TMPFILE}
echo "Exported migration event log where ${SQLFILTER}" >> ${TMPFILE}

#
# Extract events from the event log table
#
# 1) Export preload event data for a preload job (expected it will always be only one or the other)
echo "
use ${DB};
SELECT
	create_time as date_time,
	CAST(MICROSECOND('create_time')/1000 AS UNSIGNED) AS milliseconds,
	migration_host,
	migration_scriptname,
	line_number,
	CASE
		WHEN event_id=101 THEN 131
		WHEN event_id=199 THEN 171
		ELSE event_id
	END AS event_id,
	primary_id AS email_address,
	CASE
		WHEN migration_job_id < 1000000000 THEN ''
		ELSE migration_job_id
	END AS migration_job_id,
	CASE
		WHEN event_id=101 THEN CONCAT('PreloadSuccess=', migtype, ' ', 'status=', status, ' message=\"', message, '\"')
		WHEN event_id=199 THEN CONCAT('PreloadFail=', migtype, ' ', 'status=', status, ' message=\"', message, '\"')
		ELSE CONCAT(event_name, '=', migtype, ' ', 'status=', status, ' message=\"', message, '\"')
	END AS event_details
FROM
	migration.events
WHERE
	${SQLFILTER} AND
	primary_id IS NOT NULL AND
	primary_id != '' AND
	event_id IN (101,102,103,199) AND
	UPPER(migtype) IN ('WLP', 'MX8P', 'MYIP')
;
QUIT
" |  mysql --user=${DBUSER} --password=${DBPASSWORD} --host=${DBSERVER} ${MYSQLCMDPARAMS} | ${SED} "s/\t/\|/g" >> ${OUTFILE}
#
# 2) Export migration event data for a final migration job
echo "
use ${DB};
SELECT
	create_time as date_time,
	CAST(MICROSECOND('create_time')/1000 AS UNSIGNED) AS milliseconds,
	migration_host,
	migration_scriptname,
	line_number,
	event_id,
	primary_id AS email_address,
	migration_job_id,
	CONCAT(event_name, '=', migtype, ' ', 'status=', status, ' message=\"', message, '\"') AS event_details
FROM
	migration.events
WHERE
	${SQLFILTER} AND
	primary_id IS NOT NULL AND
	primary_id != '' AND
	event_id IN (100,101,112,113,115,117,118,162,185,190,197,198,199) AND
	UPPER(migtype) IN ('WL', 'MX8', 'MYI')
;
QUIT
" |  mysql --user=${DBUSER} --password=${DBPASSWORD} --host=${DBSERVER} ${MYSQLCMDPARAMS} | ${SED} "s/\t/\|/g" >> ${OUTFILE}

RECORDSCOUNT=`wc -l ${OUTFILE} | awk '{print $1;}'`
log_out "STATS" "Number of records in ${OUTFILE}: ${RECORDSCOUNT}"
echo "Number of records in ${OUTFILE}: ${RECORDSCOUNT}" >> ${TMPFILE}

log_out "MAIL" "Sending email report with subject \"${SUBJECT}\" to ${MAILTO}, bcc to: ${BCC}"
log_out "MAIL" "CMD=${MAILX} -v -s ${SUBJECT} -S smtp=${SMTPSERVER} -r ${MAILFROM} -b ${BCC} ${MAILTO}  < ${TMPFILE}"
${MAILX} -v -s "${SUBJECT}" -S smtp="${SMTPSERVER}" -r "${MAILFROM}" -b "${BCC}" "${MAILTO}"  < ${TMPFILE}

# Compress the file
gzip -qf ${OUTFILE}
STATUS="$?"
if [[ "${STATUS}" -eq 0 ]]
then
	log_out "INFO" "${OUTFILE} sucessfully compressed for sftp transfer"
	OUTFILE="${OUTFILE}.gz"
else
	err_out ${STATUS} ${LINENO} "error when compressing ${OUTFILE}"
fi

# Upload MX8Export file if and where required
#
if [[ ${UPLOAD} == "YES" ]]
then
	if [[ ${INTERACTIVE} == "YES" ]]
	then
		for d in ${REMOTEDIRS}
		do
			# put [-P] local-path [remote-path]
			#sudo -u ${LOCALUSER} sftp ${REMOTEUSER}@${SFTPSERVER}:${d} <<< $"put ${OUTFILE}"
			sftp ${REMOTEUSER}@${SFTPSERVER}:${d} <<< $"put ${OUTFILE}"
			STATUS="$?"
			if [[ "${STATUS}" -eq 0 ]]
			then
				log_out "SFTP" "No errors reported in uploading ${OUTFILE} to ${REMOTEUSER}@${SFTPSERVER}:${d}"
			else
				err_out ${STATUS} ${LINENO} "Error reported when uploading ${OUTFILE} to ${REMOTEUSER}@${SFTPSERVER}:${d}"
			fi
		done
	else
		for d in ${REMOTEDIRS}
		do
			sftp ${REMOTEUSER}@${SFTPSERVER}:${d} <<< $"put ${OUTFILE}"
			STATUS="$?"
			if [[ "${STATUS}" -eq 0 ]]
			then
				log_out "SFTP" "No errors reported in uploading ${OUTFILE} to ${REMOTEUSER}@${SFTPSERVER}:${d}"
			else
				err_out ${STATUS} ${LINENO} "Error reported when uploading ${OUTFILE} to ${REMOTEUSER}@${SFTPSERVER}:${d}"
			fi
		done
	fi
else
	log_out "SFTP" "Upload disabled: copy ${OUTFILE} manually if required"
fi

# Uncompress the file to allow elasticsearch indexing
gunzip -qf ${OUTFILE}*
STATUS="$?"
if [[ "${STATUS}" -eq 0 ]]
then
	log_out "INFO" "${OUTFILE} successfully uncompressed for elasticsearch indexing"
else
	err_out ${STATUS} ${LINENO} "error when uncompressing ${OUTFILE}"
fi

# Cleanup
log_out "CLEANUP" "Removing TMPFILE ${TMPFILE}"
rm -f ${TMPFILE}

# Done
log_out "FINISH" "${COMMANDLINE}"
