#!/usr/bin/perl

###########################################################################
#
#  Copyright 2016 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     MSFTPeopleAPI.pm
#  Description: migrate contacts from MSFT live to Mx9
#  Input:       See usage.
#  Author:      Huanwen Jiao <Huanwen.Jiao@synchronoss.com>
#  Date:        May 2016
#  Version:     1.0.0
#
#  Version History:
#
###########################################################################
package com::owm::MSFTPeopleAPI;

use warnings;
use strict;

use LWP::UserAgent;
use JSON;

use com::owm::PABClient;
use com::owm::migBellCanada;

sub sanitize($) {
    my $input = shift;
    $input =~ s/\n/\\n/g;
    $input =~ s/;/\\;/g;
    return($input);
}

sub migrateContacts($$$$$$$){
    my $puid = shift;
    my $email = shift;
    my $password = shift;
    my $userpabstorehost = shift;
    my $cfg = shift;
    my $log = shift;
    my $DEBUG = shift;
    my $LOGSTRING = $email.'|'.$puid;
    my $decimal_puid = $puid;

    #Record counts for reporting
    my %counters;
    #initialise counters to -1 so that in the case none are found we dont get an unitialised value
    $counters{'retrieved'}=-1;
    $counters{'pabErrors'}=-1;

    my $pkcs12_file = $cfg->param('microsoft_people_api_https_pkcs12_file');
    my $pkcs12_pass = $cfg->param('microsoft_people_api_https_pkcs12_password');

    if ($puid =~ /^FFF/) {
        # fake PUID
        $counters{'fakePuid'} = 1;
        return (%counters);
    } elsif ($puid =~ /^0003/) {
        my $decimal = hex($puid);
        $log->info($email.'|'.$puid.':Hex PUID detected.  Decimal PUID is '.$decimal);
        $decimal_puid = $decimal;
    }

    my $endpoint= $cfg->param('microsoft_people_api_endpoint'); #'https://rdr-people.directory.live.com/people/abcore';
    my $AppId = 'abb5a6c5-180e-485b-b3ec-dc8ff30fcf37';
    my $PageSize_cfg = $cfg->param('microsoft_people_api_pagesize'); #100;
    my $PageSize = '';
    if (defined($PageSize_cfg) && ($PageSize_cfg > 0)) {
        $PageSize = '&PageSize='.$PageSize_cfg;
    }
    my $hasMorePages = 0;
    my $isFullSync = 'true';
    my $syncKey;
    my $pageKey ;

    my @vGroup = ();
    my %vcategories;

    #my $url = "https://rdr-people.directory.live.com/people/abcore?AppId=abb5a6c5-180e-485b-b3ec-dc8ff30fcf37&market=en-US&CallerPuid=1055521822157453&PageSize=100";
    my $url = "${endpoint}?AppId=${AppId}&market=en-US&CallerPuid=${decimal_puid}${PageSize}";
    my $ua = LWP::UserAgent->new(
        max_redirect => 0, 
        ssl_opts => { 
            SSL_use_cert => 1,
            SSL_cert_file => $pkcs12_file,
            SSL_passwd_cb => sub { return($pkcs12_pass);}
            }
        );
#     $ua->add_handler("request_send",  sub { shift->dump; return });
#     $ua->add_handler("response_done", sub { shift->dump; return });

    my $retry_count = 0;
    my $req;
    my $resp;
    my $location = '';
    do {
        $req = HTTP::Request->new('GET', $url);
        $req->header("Accept" => "application/json" );
        $resp = $ua->request($req);

        if ($resp->code == 301) {
            $location = $resp->header('Location');
        } else {
            $log->debug("$LOGSTRING|ABCH url = $url");
            $log->error("$LOGSTRING|".$resp->status_line ."\n");
            $log->error("$LOGSTRING|ABCH did not response with Data Center location by redirect");
            $retry_count++;
            if ($retry_count > 5) {
                $counters{'fatalerror'} = 1;
                return (%counters);
            } else {
                sleep(1 + $retry_count);
            }
        }
    } while ($resp->code != 301);

    my $redirect_count = 0;
    $retry_count = 0;

    LOOP: do {
        if ($retry_count > 5) {
            $log->error($LOGSTRING.':Too many errors fetching data from MSFT people API - aborting');
            $counters{'fatalerror'} = 1;
            return (%counters);
        }
        $url = "$location?AppId=${AppId}&market=en-US&CallerPuid=${decimal_puid}${PageSize}";
        if ( $hasMorePages ) {
            $url .= "&syncKey=${syncKey}&pageKey=${pageKey}";
        }

        $log->debug("$LOGSTRING|ABCH url = $url");
        #print "\nurl=$url\n";

        $req = HTTP::Request->new('GET', $url);
        $req->header("Accept" => "application/json" );
        $resp = $ua->request($req);

        #if($resp->code != 200) { 
        #} else {
        #}
        #print ("\n" . $resp->as_string . "\n");
        if ($resp->code == 301) {
            $location = $resp->header('Location');
            $redirect_count ++;
            if ($redirect_count > 5) {
                $log->error($LOGSTRING.':Error when fetching PAB data from Microsoft API, possible redirection loop');
                $counters{'fatalerror'} = 1;
                return (%counters);
            }
            goto LOOP;
        }

        my $content = $resp->content;
        unless (lc(substr($content, 0, 6)) eq '<json>') {
            $log->error($LOGSTRING.':MSFT people API did not return "<json>" tag at start of data');
            $counters{'fatalerror'} = 1;
            return (%counters);
        }
        $content = substr( $content,6,length($content));
        #print ("\n\n\n$content\n\n\n");
        my $content_decoded;
        eval{
            $content_decoded = decode_json($content);
        };
        if($@){
            $log->error($LOGSTRING.':Error -'. $@);
            $retry_count++;
            sleep(1);
            goto LOOP;
        }
        
        if (defined($content_decoded->{'error'})) {
            my %error = %{$content_decoded->{'error'}};
            $log->error($LOGSTRING.':Error from MSFT People API: "'.$error{'code'}.'", "'.$error{'message'}.'"');
            $counters{'fatalerror'} = 1;
            return (%counters);
        }

        unless (defined($content_decoded->{'ab'})) {
            $log->error($LOGSTRING.':Error - no "ab" object in the returned JSON from Microsoft');
            $retry_count++;
            sleep(1);
            goto LOOP;
        }
        unless (defined($content_decoded->{'ab'}->{'persons'})) {
            $log->error($LOGSTRING.':Error - no "ab->persons" object in the returned JSON from Microsoft');
            $retry_count++;
            sleep(1);
            goto LOOP;
        }

        my $ab = $content_decoded->{'ab'};
        $pageKey = $ab->{'pageKey'} if ($ab->{'pageKey'}) ;
        $syncKey = $ab->{'syncKey'} if ($ab->{'syncKey'}) ;
        if ($ab->{'hasMorePages'}){
           $hasMorePages = $ab->{'hasMorePages'};
           unless (defined($ab->{'pageKey'})) {
                $log->warn($LOGSTRING.':Did not receive pageKey from MSFT persons API, but hasMorePages is set')
            }
            unless (defined($ab->{'syncKey'})) {
                $log->warn($LOGSTRING.':Did not receive syncKey from MSFT persons API, but hasMorePages is set')
            }
        }else{
            $hasMorePages = 0;
        }
        $log->debug("$LOGSTRING| hasMorePages = $hasMorePages");
        if ($ab->{'categories'}){
            my @categories = @{$ab->{'categories'}};
            foreach my $category (@categories) {
                unless (defined($category->{'categoryId'})) {
                    $log->warning($LOGSTRING.':Category from MSFT persons API is missing categoryId, skipping');
                    next;
                }
                unless (defined($category->{'name'})) {
                    $log->warning($LOGSTRING.':Category from MSFT persons API is missing name, skipping');
                    next;
                }
                my $categoryId = $category->{'categoryId'};
                my $name = $category->{'name'};
                $vcategories{$categoryId}{'name'} = $category->{'name'};
                $vcategories{$categoryId}{'members'} = '';
            }
        }
        
        my @persons = @{$ab->{'persons'}};
        foreach my $p (@persons){
            my $vcard = "BEGIN:VCARD\n";
            $vcard .= "VERSION:3.0\n";
            if ( $p->{'id' }) {
                $counters{'retrieved'}++;
            } else {
                next;
            }
            #last unless ($p->{'id'});
            $vcard .= 'UID:'.sanitize($p->{'id'})."\n";
            my $fn = "FN:";
            $fn .= sanitize($p->{'firstName'}) if ($p->{'firstName'});
            $fn .= ' '.sanitize($p->{'lastName'}) if ($p->{'lastName'});
            $vcard .= "$fn\n";
            # my $nn = "N:<FamilyName>;<GivenName>;<AdditionalNames>;<HonorificPrefixes>;<HonorificSuffixes>\n";
            my $nn = 'N:';
            $nn .= sanitize($p->{'lastName'}) if ($p->{'lastName'});
            $nn .= ';';
            $nn .= sanitize($p->{'firstName'}) if ($p->{'firstName'});
            $nn .= ';';
            $nn .= sanitize($p->{'middleName'}) if ($p->{'middleName'});
            $nn .= ';';
            # prefix - missing from MSFT API
            $nn .= ';';
            $nn .= sanitize($p->{'suffix'}) if ($p->{'suffix'});
            $nn .= "\n";

            $vcard .= $nn;
            $vcard .= 'ORG:'.sanitize($p->{'companyName'})."\n" if ($p->{'companyName'}) ;
            if ($p->{'birthday'}){
                my $d = $p->{'birthday'};
                $d =~ m/(\d+)/g  ;
                my $datetime = $1;
                my @date = gmtime($datetime/1000);
                my $date = sprintf('%4d-%02d-%02d', $date[5]+1900, $date[4]+1, $date[3]);
                $vcard .= 'BDAY:'.$date."\n" ;
            }
            $vcard .= 'NICKNAME:'.sanitize($p->{'nickname'})."\n" if ($p->{'nickname'}) ;
            $vcard .= 'TITLE:'.sanitize($p->{'title'})."\n" if ($p->{'title'}) ;
            $vcard .= 'URL:'.sanitize($p->{'website'})."\n" if ($p->{'website'});

            if ($p->{'emails'}) {
                my @emails = @{$p->{'emails'}};
                $vcard .= 'EMAIL;TYPE=internet;TYPE=home:'.sanitize($emails[0])."\n" if ($emails[0]);
                $vcard .= 'EMAIL;TYPE=internet;TYPE=work:'.sanitize($emails[1])."\n" if ($emails[1]);
                $vcard .= 'EMAIL;TYPE=internet;TYPE=other:'.sanitize($emails[2])."\n" if ($emails[2]);
                $vcard .= 'EMAIL;TYPE=internet;TYPE=home:'.sanitize($emails[3])."\n" if ($emails[3]);#3-Messenger
                $vcard .= 'EMAIL;TYPE=internet;TYPE=home:'.sanitize($emails[4])."\n" if ($emails[4]);
                $vcard .= 'EMAIL;TYPE=internet;TYPE=home:'.sanitize($emails[5])."\n" if ($emails[5]);
                $vcard .= 'EMAIL;TYPE=internet;TYPE=home:'.sanitize($emails[6])."\n" if ($emails[6]);
            }
            
            if ($p->{'phones'}) {
                my @phones = @{$p->{'phones'}};
                $vcard .= 'TEL;TYPE=cell:'.sanitize($phones[0])."\n" if ($phones[0]);
                $vcard .= 'TEL;TYPE=home:'.sanitize($phones[1])."\n" if ($phones[1]);
                $vcard .= 'TEL;TYPE=home:'.sanitize($phones[2])."\n" if ($phones[2]);
                $vcard .= 'TEL;TYPE=work:'.sanitize($phones[3])."\n" if ($phones[3]);
                $vcard .= 'TEL;TYPE=work:'.sanitize($phones[4])."\n" if ($phones[4]);
                $vcard .= 'TEL;TYPE=pager:'.sanitize($phones[5])."\n" if ($phones[5]);
                $vcard .= 'TEL;TYPE=fax;TYPE=home:'.sanitize($phones[6])."\n" if ($phones[6]);
                $vcard .= 'TEL;TYPE=fax;TYPE=work:'.sanitize($phones[7])."\n" if ($phones[7]);
                $vcard .= 'TEL;TYPE=cell:'.sanitize($phones[8])."\n" if ($phones[8]); #Mobile 
                $vcard .= 'TEL;TYPE=work:'.sanitize($phones[9])."\n" if ($phones[9]); #Company
                $vcard .= 'TEL;TYPE=other:'.sanitize($phones[10])."\n" if ($phones[10]); #Other
            }
            
            if ($p->{'categoryIds'}) {
                my @categoryIds = @{$p->{'categoryIds'}};
                foreach my $categoryId (@categoryIds){
                    $vcategories{$categoryId}{'members'} .= "$p->{'id' };";
                }
            }

            if ($p->{'addresses'}) {
                my @addresses = @{$p->{'addresses'}};
                foreach my $index (0..2) {
                    my $address;
                    if ($index == 0) {
                        $address = 'ADR;TYPE=home:;;';
                    } elsif ($index == 1) {
                        $address = 'ADR;TYPE=work:;;';
                    } elsif ($index == 2) {
                        $address = 'ADR;TYPE=other:;;';
                    }
                    if ($addresses[$index]) {
                        my %address = %{$addresses[$index]};
                        if ($address{'street'}) {
                            $address .= sanitize($address{'street'});
                        }
                        $address .= ';';
                        if ($address{'city'}) {
                            $address .= sanitize($address{'city'});
                        }
                        $address .= ';';
                        if ($address{'state'}) {
                            $address .= sanitize($address{'state'});
                        }
                        $address .= ';';
                        if ($address{'postalCode'}) {
                            $address .= sanitize($address{'postalCode'});
                        }
                        $address .= ';';
                        if ($address{'country'}) {
                            $address .= sanitize($address{'country'});
                        }
                    }
                    $vcard .= $address."\n";;
                }
            }

            my $notes = 'NOTE:';

            if ($p->{'notes'}) {
                my $note = $p->{'notes'};
                $note =~ s/\n/\\n/g;
                $notes .= sanitize($note).'\n\n';
            }

            if ($p->{'anniversary'}){
                my $d = $p->{'anniversary'};
                $d =~ m/(\d+)([+-]\d+)/g  ;
                my $datetime = $1;
                my $offset = $2;
                my @date = gmtime($datetime/1000);
                my $date = sprintf('%4d-%02d-%02d', $date[5]+1900, $date[4]+1, $date[3]);
                $notes .= 'Anniversary: '.$date.'\n';
            }

            if ($p->{'jobTitle'}) {
                $notes .= 'Job Title: '.sanitize($p->{'jobTitle'}).'\n';
            }

            if ($p->{'significantOther'}) {
                $notes .= 'Significant Other: '.sanitize($p->{'significantOther'}).'\n';
            }

            if ($p->{'suffix'}) {
                $notes .= 'Suffix: '.sanitize($p->{'suffix'}).'\n';
            }

            if ($p->{'yomiFirstName'}) {
                $notes .= 'Yomi first name: '.sanitize($p->{'yomiFirstName'}).'\n';
            }

            if ($p->{'yomiLastName'}) {
                $notes .= 'Yomi last name: '.sanitize($p->{'yomiLastName'}).'\n';
            }

            if ($p->{'yomiCompanyName'}) {
                $notes .= 'Yomi company name: '.sanitize($p->{'yomiCompanyName'}).'\n';
            }

            if ($notes ne 'NOTE:') {
                $vcard .= $notes."\n";
            }

            $vcard .= "END:VCARD";
            print ("\nvcard =\n$vcard\n") if ($DEBUG);
            # $log->debug("$LOGSTRING| \n$vcard\n");
            $counters{'processed'}++;
            my $contact_reference = "put contact=[ " . $p->{'id'} . " - " . $p->{'lastName'} . ", " . $p->{'firstName'} . " ]";
            %counters = uploadVCard($email,$puid,$password,$vcard,$userpabstorehost,$cfg,$log,$contact_reference,\%counters);
        }
    } while ( $hasMorePages );

    %counters = migrateGroups($email,$puid,$password,$userpabstorehost,$cfg,$log,$DEBUG,\%vcategories,\%counters);
    
    $counters{'retrieved'}++; #add one as we initialised it to -1 to start.
    $counters{'pabErrors'}++;
    return (%counters);
}

sub migrateGroups($$$$$$$$$) {
    my $email = shift;
    my $puid = shift;
    my $password = shift;
    my $userpabstorehost = shift;
    my $cfg = shift;
    my $log = shift;
    my $DEBUG = shift;
    my $vcategories_ = shift;
    my %vcategories = %$vcategories_;
    my $counters_ = shift;
    my %counters = %$counters_;

    foreach my $ctid (keys %vcategories) {
        $counters{'retrieved'}++;
        my $name = "$vcategories{$ctid}{'name'}\n";
        my $members = "$vcategories{$ctid}{'members'}\n";
        my $vcard = "BEGIN:VCARD\n";
        $vcard .= "VERSION:3.0\n";
        $vcard .= "UID:$ctid\n";
        $vcard .= "X-ADDRESSBOOKSERVER-KIND:group\n";
        $vcard .= "N:$vcategories{$ctid}{'name'};;;;\n";
        $vcard .= "FN:$vcategories{$ctid}{'name'}\n";

        if ( $vcategories{$ctid}{'members'} ) {
            my @uid = split (/;/,$vcategories{$ctid}{'members'});
            foreach my $i (0 .. $#uid) {
                $vcard .= "X-ADDRESSBOOKSERVER-MEMBER:urn:uuid:$uid[$i]\n";
            }
        }
        $vcard .= "END:VCARD\n";
        print 'group vcard = '.$vcard ."\n" if ($DEBUG);
        # $log->debug("$email|$puid| \n$vcard\n");
        $counters{'processed'}++;
        my $contact_reference = "put group=[ " . $ctid . " - " . $vcategories{$ctid}{'name'} . " ]";
        %counters = uploadVCard($email,$puid,$password,$vcard,$userpabstorehost,$cfg,$log,$contact_reference,\%counters);
    }
    return(%counters);

}


sub uploadVCard($$$$$$$$$) {
    my $email = shift;
    my $puid = shift;
    my $password = shift;
    my $vcard = shift;
    my $userpabstorehost = shift;
    my $cfg = shift;
    my $log = shift;
    my $contact_reference = shift;
    my $counters_ = shift;
    my %counters = %$counters_;

    my $pab_url =  $cfg->param('pab_base_url');
    my ($port, $error) = com::owm::migBellCanada::getPortNumber('http','pab',$userpabstorehost,$log,$cfg);
    $pab_url =~ s/<host>/$userpabstorehost/;
    $pab_url =~ s/<port>/$port/;
    $pab_url = $pab_url."/".$email."/"."Main";
    my $resp = com::owm::PABClient::put($pab_url,$email,$password,$vcard,$log);

    if ($resp->code == 201) {
        $log->info("$email|$puid:$contact_reference successful.");
    }elsif ($resp->code == 412) {
        $log->info("$email|$puid:$contact_reference already exists " . $resp->code);
        $counters{'duplicate'}++;
    } elsif ($resp->code == 424) {
        $log->error("$email|$puid:$contact_reference failed, check file path location on PAB " . $resp->code);
        $counters{'fileMissing'}++;
    }else {
        $log->info("$email|$puid:$contact_reference failed response code=" . $resp->code . " response content=".$resp->as_string );
        $counters{'error'}++;
    }

    return(%counters);
}

1;
