#!/usr/bin/perl
###########################################################################
#
#  Copyright 2014 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Huanwen Jiao <huanwen.jiao@owmessaging.com>
#  Date:        3Q/2015
#  Version:     1.0.0 - Aug 17, 2015
#
###########################################################################
package com::owm::Mx8toMx9;
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use com::owm::migBellCanada;
use strict;
use warnings;
use com::owm::PABClient;
use com::owm::mosApi;
use Config::Simple;
use Text::CSV;
use com::owm::migBellCanada;

sub getAutoReplyData($$$){
	my $datafile = shift;
	my $email = shift;
	my $log = shift;
	my %autoReplydata = ();
	my $autoReplyType = 0;
	my $autoReplyMsg = "";
	$autoReplydata{"99"}="init"; #initialize a dummy hash field to use to indicate that no data was retrieved.

	if (open my $fh, "<", $datafile) {
		while(my $line = <$fh>){
			 if(substr($line,0,1) eq "["){
				$autoReplydata{$autoReplyType} = $autoReplyMsg	if $autoReplyType > 0;
				(my $garbage,$autoReplyType) = ($line =~ /\[(.*):(.*)\]/); 
				$autoReplyMsg = "";
			 } else{		 
					$autoReplyMsg .= $line if ($autoReplyType > 0); 
			 }
		} 
		$autoReplydata{$autoReplyType} = $autoReplyMsg if(($autoReplyType > 0) && ($autoReplyMsg ne "")); 
		
	} else {
		$log->info("$email|No auto reply data was found");
		$autoReplydata{"99"}="none";
	}
	return \%autoReplydata;
}

sub processData ($$$$$$$$) {
	my $email = shift;
	my $password = shift;
	my $data = shift;
	my $mos = shift;
    my $usercalstorehost = shift;
    my $userpabstorehost = shift;
	my $cfg = shift;
	my $log = shift;

	my %autoReplydata = %$data;
	
	my @cckey = keys %autoReplydata;

	for my $cck (@cckey) {
			#print "$cck=$autoReplydata{$cck}\n";
	#		print "$cck\n";
		if ( $cck eq '1') { #Vocation Message
			print "type1\n";
			processVacationMsg ($email,$autoReplydata{'1'},$mos,$cfg,$log);
		} elsif ( $cck eq '2' ) { #Signature
			print "type2\n";
			processSignature ($email,$autoReplydata{'2'},$mos,$cfg,$log);
		} elsif( $cck eq '7' ) {
			print "type7\n";
			processContacts($email, $password, $autoReplydata{'7'},$usercalstorehost,$userpabstorehost, $cfg,$log);
		} 
		
	}

}

sub processVacationMsg ($$$$$) {
	my $email = shift;
	my $vacationMsg = shift;
	my $mos = shift;
	my $cfg = shift;
	my $log = shift;
	my $autoReplyMode;
	my @ldapUser;
	my $result;

	$log->debug("$email|Vacation message:".$vacationMsg);
	# Set Vacation message
	# AutoReplyMode options are none|vacation|reply|echo
	# For Mx8 user this should be set in LDAP already if it exists
	# To read from ldap each time set readLdapForMx8Settings = true this will force an update in case
	# Default the autoreplymode to none 
	my %autoReplyModes = ( "R" => "reply", "E" => "echo", "V" => "vacation", "N" => "none");
	if ($cfg->param('readLdapForMx8Settings') eq "true" ) {
		($result,@ldapUser) = ldapGetUser($email,$cfg,$log);
		my $thisMode = $ldapUser[0]->get_value('mailautoreplymode');
		$autoReplyMode = $autoReplyModes{"$thisMode"};
		if ( ! defined $autoReplyMode || $autoReplyMode eq "" ) {
			$autoReplyMode = "none";
		}
		$log->debug("$email|Getting autoreplymode from ldap, mode is  ".$autoReplyMode);
	} else {
		$autoReplyMode = "MX8";
	}
	$mos->user($email);
	my $rtn = $mos->SetVacation($autoReplyMode,$vacationMsg);
	if (!$mos->is_success) {
		chomp($rtn);
		$log->error("$email|Setting vacation failed : ");
		return($com::owm::migBellCanada::STATUS_GENERAL_ERROR,$email);
	}  else {
		$log->info("$email|Setting vacation complete");
		return($com::owm::migBellCanada::STATUS_SUCCESS,$email);
	}
}

sub processSignature ($$$$$) {
	my $email = shift;
	my $signature = shift;
	my $mos = shift;
	my $cfg = shift;
	my $log = shift;
	my $isDefault = 'yes';
	my @ldapUser;
	my $result;
  
	$log->debug("$email|Signature:$signature") if ($signature);
	if ($cfg->param('readLdapForMx8Settings') eq "true" ) {
		($result,@ldapUser) = ldapGetUser($email,$cfg,$log);
		my $thisSignature = $ldapUser[0]->get_value('mailwebmailusesignature');
		if ($thisSignature && $thisSignature == 0){
			$isDefault = 'no';
		}
		$log->debug("$email|Getting mailwebmailusesignature from ldap,  ".$isDefault);
	} 
	$mos->user($email);
    my $rtn = $mos->signatureCreate("mss",$signature,'signature',$isDefault); 
	   if (!$mos->is_success) {
      chomp($rtn);
      $log->error("$email|Setting signature failed : ".$rtn);
      $mos->PrintResponse;
	  return($com::owm::migBellCanada::STATUS_FAIL_AS_SET_SIGN,$email);
    }  else {
      $log->info("$email|Setting signature complete");
	  return($com::owm::migBellCanada::STATUS_SUCCESS,$email);
    }
}

sub processContacts($$$$$$$) {
	my $email = shift;
	my $password = shift;
	my $data = shift;
    my $usercalstorehost = shift;
    my $userpabstorehost = shift;
	my $cfg = shift;
	my $log = shift;
	
	#Record counts for reporting		
	my %counters;	
	#initialise counters to -1 so that in the case none are found we dont get an unitialised value
	$counters{'retrieved'}=-1;
	$counters{'pabErrors'}=-1;
	
	my $csvfile =  "./tmp/$email.csv";
	
	open(my $fh, ">", "$csvfile");
    print  $fh ($data);
	close  ($fh);

	my @rows = ();
	my $csv = Text::CSV->new ( { binary => 1 } )	# should set binary attribute.
					or die "Cannot use CSV: ".Text::CSV->error_diag ();

	open $fh, "<:encoding(utf8)", $csvfile or die "$csvfile: $!";
	
	while ( my $row = $csv->getline( $fh ) ) {
		my @fields = @$row;
		chomp $row;
		push @rows, $row;
	}
	$csv->eof or $csv->error_diag();
	close $fh;

	$csv->eol ("\r\n");

	#unlink($csvfile);	
	
	my $title = $rows[0];

    my @vGroup = ();
	my $vcardID = "";
	foreach my $i (1 .. $#rows) {
		my $line = $rows[$i];
		my %contact = ();
		foreach my $i (0 .. $#{$line}) {
			$contact{@{$title}[$i]} = @{$line}[$i];
			$contact{@{$title}[$i]} =~ s/\\/\\\\/g;
			$contact{@{$title}[$i]} =~ s/\r\n/rfvrnrfv/g;
			###$contact{@{$title}[$i]} =~ s/\r\n/\\r\\n/g;
			$contact{@{$title}[$i]} =~ s/\n/\\n/g;
			$contact{@{$title}[$i]} =~ s/rfvrnrfv/\\n/g;
		}

        if ( $contact{'entry.type'} && ($contact{'entry.type'} eq "C" )) {
        	$counters{'retrieved'}++;
            my $vcard = createVCARD(\%contact);
			$vcardID = $contact{'Id'}." - ".$contact{'LastName'}.", ".$contact{'FirstName'}." ".$contact{'MiddleName'};
            if ( $vcard ne "") {
    #			$log->debug("$email|vcard - \n$vcard\n");
                (my $status,%counters) = uploadContacts($email,$password,$vcard,$usercalstorehost,$userpabstorehost,$cfg,$log,\%counters,$vcardID);
                if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
                    #Migration will continue to attempt the other vcards
                }
            }
        } elsif ($contact{'entry.type'} && ($contact{'entry.type'} eq "L") ) {
        	$counters{'retrieved'}++;
            my $vcardg = createVGroup(\%contact);
            push (@vGroup,$vcardg);
        }
	}
    
    #
    foreach my $i (0 .. $#vGroup) {
        (my $status,%counters) = uploadContacts($email,$password, $vGroup[$i],$usercalstorehost,$userpabstorehost,$cfg,$log,\%counters,"vgroup-$i");
        if ( $status ne $com::owm::migBellCanada::STATUS_SUCCESS ) {
            #Migration will continue to attempt the other vcards
        }
    }

	$counters{'retrieved'}++; #add one as we initialised it to -1 to start.
	$counters{'pabErrors'}++;

	if ( ($counters{'retrieved'} > 0) && ($counters{'retrieved'} == $counters{'pabErrors'} )) {
		return($com::owm::migBellCanada::STATUS_FAIL_MSFT_CONTACTS, $email, %counters);
	} else {
		return($com::owm::migBellCanada::STATUS_SUCCESS, $email, %counters);
	}
}

sub uploadContacts($$$$$$$$$) {
	my $email = shift;
	my $password = shift;
	my $vcard = shift;
    my $usercalstorehost = shift;
    my $userpabstorehost = shift;
	my $cfg = shift;
	my $log = shift;
	my $counters_ = shift;
	my $vcardID = shift;
    my %counters = %$counters_;

	my $puid=""; #test
    my $pab_url =  $cfg->param('pab_base_url');
    my ($port, $error) = com::owm::migBellCanada::getPortNumber('http','pab',$userpabstorehost,$log,$cfg);
    $pab_url =~ s/<host>/$userpabstorehost/;
    $pab_url =~ s/<port>/$port/;
	$pab_url = $pab_url."/".$email."/"."Main";
	my $resp = com::owm::PABClient::put($pab_url,$email,$password,$vcard,$log);

	if ($resp->code == 201) {
		$log->info("$email| put contact successful.");
		#return($com::owm::migBellCanada::STATUS_SUCCESS);
    }elsif ($resp->code == 412) {
    	$log->info("$email|$vcardID: contact already exists " . $resp->code);
		$counters{'duplicate'}++;
	} elsif ($resp->code == 424) {
		$log->error("$email|$vcardID: put contact failed, check file path location on PAB/CAL " . $resp->code);
		$counters{'fileMissing'}++;
	}else {
		$log->info("$email|$vcardID put contact failed " . $resp->code);
		print ("\n".$resp->as_string ."\n") ;
		$counters{'error'}++;
		#return($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
	}
	#always return success, failure of individual contact import should not cause migration failure.
    return($com::owm::migBellCanada::STATUS_SUCCESS,%counters);
}

sub createVGroup($) {
    my $param = shift;
    my %contact = %$param;
    
    my $vcard = "BEGIN:VCARD\n";
    $vcard .= "VERSION:3.0\n";
    $vcard .= "UID:$contact{'Id'}\n";
    $vcard .= "X-ADDRESSBOOKSERVER-KIND:group\n";
    $vcard .= "N:$contact{'list.Name'};;;;\n";
    $vcard .= "FN:$contact{'list.Name'}\n";
    if ( $contact{'list.Members'} ) {
        my @uid = split (/;/,$contact{'list.Members'});
    	foreach my $i (0 .. $#uid) {
         $vcard .= "X-ADDRESSBOOKSERVER-MEMBER:urn:uuid:$uid[$i]\n";      
        }
    }
    $vcard .= "END:VCARD\n";
    
    #print "\n\n====\n$vcard\n====\n\n";  #test
    
    return $vcard;
}

sub createVCARD($) {
	my $param = shift;
	my %contact = %$param;
	my $vcard = "";
	{
	no warnings 'uninitialized';
	 
			$vcard = "BEGIN:VCARD\n";
			$vcard .= "VERSION:3.0\n";
            $vcard .= "UID:".$contact{'Id'}."\n";
			$vcard .= "N:".$contact{'LastName'}.";".$contact{'FirstName'}.";".$contact{'MiddleName'}.";;\n";
			$vcard .= "FN:".$contact{'LastName'}."\, ".$contact{'FirstName'}."\n";
			$vcard .= "NICKNAME:".$contact{'Nickname'}."\n";
			#print "\ndate:--$contact{'BirthdayYear'}--$contact{'BirthdayMonth'}--$contact{'BirthdayDay'}--\n";
			if ($contact{'BirthdayYear'} && $contact{'BirthdayMonth'} && $contact{'BirthdayDay'}) {
				if ( length ($contact{'BirthdayMonth'}) ==1 ) {
					$contact{'BirthdayMonth'} = '0'. $contact{'BirthdayMonth'};
				}
				if ( length ($contact{'BirthdayDay'}) ==1 ) {
					$contact{'BirthdayDay'} = '0'. $contact{'BirthdayDay'};
				}
				if ( length ($contact{'BirthdayYear'}) < 4 ) {
					;
				}
				$vcard .= "BDAY:".$contact{'BirthdayYear'}.$contact{'BirthdayMonth'}.$contact{'BirthdayDay'}."\n";#2015-08-05";
			}
			$vcard .= "ORG:".$contact{'Company'}.";".$contact{'Department'}."\n"; #company;department";
			$vcard .= "TITLE:".$contact{'Title'}."\n"; #title";
			$vcard .= "EMAIL;TYPE=internet;TYPE=home:".$contact{Email}."\n";
			$vcard .= "EMAIL;TYPE=internet;TYPE=other:".$contact{AltEmail}."\n";
			$vcard .= "TEL;TYPE=fax;TYPE=home:".$contact{'HomeFax'}."\n";
			$vcard .= "TEL;TYPE=fax;TYPE=work:".$contact{'WorkFax'}."\n";
			$vcard .= "TEL;TYPE=cell:".$contact{'MobilePhone'}."\n";
			#$vcard .= "TEL;TYPE=cell;TYPE=work;TYPE=voice:".011
			#$vcard .= "TEL;TYPE=cell;TYPE=voice:"."\n";
			$vcard .= "TEL;TYPE=home;TYPE=pager:".$contact{'Pager'}."\n";
			$vcard .= "TEL;TYPE=home:".$contact{'HomePhone'}."\n"; #020
			$vcard .= "TEL;TYPE=work:".$contact{'WorkPhone'}."\n";
			$vcard .= "TEL;TYPE=other:".$contact{'OtherPhone'}."\n";
			#$vcard .= "TEL;TYPE=voice:022
			$vcard .= "ADR;TYPE=HOME:;;".$contact{'HomeAddress1'}.";".$contact{'HomeCity'}.";".$contact{'HomeState'}.";".$contact{'HomeZip'}.";".$contact{'HomeCountry'}."\n";
			$vcard .= "ADR;TYPE=WORK:;;".$contact{'WorkAddress1'}.";".$contact{'WorkCity'}.";".$contact{'WorkState'}.";".$contact{'WorkZip'}.";".$contact{'WorkCountry'}."\n";
			my $note = "NOTE:".$contact{'Notes'}."\\n\\n";
			if ( $contact{'BuddyName'} ) {
				$note .= "Social: ". $contact{'ImProvider'}."  ". $contact{'BuddyName'}."\\n\\n";
			}
			if ( $contact{'PGPKey'} ) {
				$note .= "PGP KEY: ". $contact{'PGPKey'}."\\n\\n";
			}
			if ($contact{'AnniversaryYear'} && $contact{'AnniversaryMonth'} && $contact{'AnniversaryDay'}) {
				if ( length ($contact{'AnniversaryMonth'}) ==1 ) {
					$contact{'AnniversaryMonth'} = '0'. $contact{'AnniversaryMonth'};
				}
				if ( length ($contact{'AnniversaryDay'}) ==1 ) {
					$contact{'AnniversaryDay'} = '0'. $contact{'AnniversaryDay'};
				}
				if ( length ($contact{'AnniversaryYear'}) < 4 ) {
					;
				}
				#$vcard .= "ANNIVERSARY:".$contact{'AnniversaryYear'}.$contact{'AnniversaryMonth'}.$contact{'AnniversaryDay'}."\n";#2015-08-05";
				$note .= "ANNIVERSARY:".$contact{'AnniversaryYear'}.'-'.$contact{'AnniversaryMonth'}.'-'.$contact{'AnniversaryDay'}."\\n\\n";
			}
			if ( $contact{'Spouse'} ) {
				$note .= "Spouse: ".$contact{'Spouse'}."\\n";
			}
			if ( $contact{'Children'} ) {
				$note .= "Children: ".$contact{'Children'}."\\n";
			}
			#$vcard .= "RELATED;TYPE=spouse;VALUE=text:".$contact{'Spouse'}."\n";
			#$vcard .= "RELATED;TYPE=child;VALUE=text:".$contact{'Children'}."\n";
			$vcard .= $note."\n";
			#$vcard .= "X-GTALK:".0101
			#$vcard .= IMPP;X-Service-Type=GoogleTalk:xmpp:0101
			$vcard .= "URL:".$contact{'Website'}."\n";
			$vcard .= "URL;TYPE=home:".$contact{'HomeUrl'}."\n"; #http://home.com
			$vcard .= "URL;TYPE=work:".$contact{'WorkUrl'}."\n"; #http://home.com
			#$vcard .= "KEY:data:application/pgp-keys;base64,".$contact{'PGPKey'}."\n";
			$vcard .= "END:VCARD";


	}
    #print "\n\n====\n$vcard\n====\n\n";  #test
	return $vcard;
}

sub ldapGetUser($$$)
{
  my $email = shift;
  my $cfg = shift;
  my $log = shift;
  my $mx_dircache_host = $cfg->param('mx_dircache_host_src');
  my $mx_dircache_port = $cfg->param('mx_dircache_port_src');
  my $mx_dircache_pwd  = $cfg->param('mx_dircache_pwd_src');

  my $ldap = Net::LDAP->new ( $mx_dircache_host, port => $mx_dircache_port );
  if (! $ldap) {
    $log->error("$email:connect to $mx_dircache_host:$mx_dircache_port failed");
    return($com::owm::migBellCanada::STATUS_FAIL_EMAIL_LOOKUP);
  }

  my $mesg = $ldap->bind ( "cn=root", password => $mx_dircache_pwd );
  if ( $mesg->code ne '0' ) {
    $log->error("$email:bind to $mx_dircache_host:$mx_dircache_port failed. Check LDAP password.");
    return($com::owm::migBellCanada::STATUS_FAIL_EMAIL_LOOKUP);
  } 

  my $searchString = "mail=$email";
  $log->debug("$email:searchString = $searchString");

  my $result = $ldap->search ( base   => "", filter  => "$searchString")
    || $log->error("$email:search failed");

  if ($result->code) {
    $log->error("$email:Email lookup for Email failed. $result->error");
    return($com::owm::migBellCanada::STATUS_FAIL_EMAIL_LOOKUP);
  }

  my @entries = $result->entries;

  my $dn = $entries[0]->dn;
  (my $jumk,$email,my @jumk) = split(/[=,]/,$dn);

  my $mailboxstatus = $entries[0]->get_value("mailboxstatus");
  my $maillogin = $entries[0]->get_value("maillogin");
  my $mailautoreplymode =  $entries[0]->get_value("mailautoreplymode");
  my $mailboxID =  $entries[0]->get_value("mailboxid");
  my $migStatus =  $entries[0]->get_value("migstatus");
  
  $ldap->unbind;
  return($com::owm::migBellCanada::STATUS_SUCCESS,$entries[0]);
}


1;
