package com::owm::Timers;

##############################################################################################################
#
#  Copyright 2016 Openwave Messaging.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#############################################################################################################
#
#  Version Control
#
#  Version: 1.0.0
#  Changes:
#
##############################################################################################################

use warnings;
use strict;

use Time::HiRes qw( gettimeofday tv_interval );

sub new
{
    my ($that, $pabPedCount, $calPedCount) = @_;
    my $class = ref($that) || $that;
    my $self = {
        durations => { },
    };

    bless($self, $class);
}

#------------------------------------------------------------------------------
# time_*: manage durations
#
# time_start: mark the end of a time interval
# time_end:   mark the end of a time interval
# time_dump:  dump the saved duration info
#------------------------------------------------------------------------------

sub time_start($$$)
{
    my ($self, $log, $what) = @_;
    my ( @now ) = gettimeofday();
    my $cumulative_time = 0;
    my $samples = 0;

    my $sec = $now[0] + ($now[1] / 1000000);

#    printf STDERR ( "--START $what %.6f\n", $sec );

    if ( exists( $self->{'durations'}{$what} ) )
    {
        my $cref = $self->{'durations'}{$what};

        if ( $cref->[0] eq 'RUNNING' )
        {
            $log->error('TIMERS: Duration "'.$what.'" is already active');
        }

        $cumulative_time = $cref->[1];
        $samples = $cref->[2];
    }

    $self->{'durations'}{$what} = [ 'RUNNING', $cumulative_time, $samples, [ @now ] ];
}

sub time_end($$$)
{
    my ($self, $log, $what) = @_;

    if ( !exists( $self->{'durations'}{$what} ) )
    {
        $log->error('TIMERS: No start time defined for "'.$what.'"');
    }

    my $cref = $self->{'durations'}{$what};

    if ( $cref->[0] ne 'RUNNING' )
    {
        $log->error('TIMERS: Duration "'.$what.'" is not active');
    }

    my $interval = tv_interval( $cref->[3] );
    my $cumulative_time = $cref->[1] + $interval;
    my $samples = $cref->[2] + 1;

    $self->{'durations'}{$what} = [ 'FINISHED', $cumulative_time, $samples, $interval ];

#    print STDERR "--END   $what\n";
}

sub time_show($$$)
{
    my ($self, $log, $what) = @_;

    if ( !exists( $self->{'durations'}{$what} ) )
    {
        $log->error('TIMERS: No duration defined for "'.$what.'"');
    }

    my $cref = $self->{'durations'}{$what};
    my ( $state, $cumulative_time, $samples, $average ) = @$cref;

    my $line = 
        sprintf( "TIMERS: %30.30s: %9.3fs avg (%9.3fs / %6d)\n",
                 $what, $average, $cumulative_time, $samples );

    $log->debug('TIMERS: '.$line);
    # print STDERR $line;
}

sub time_dump($$)
{
    my $self = shift;
    my $log = shift;

    my @list = ();

    foreach my $what ( keys %{ $self->{'durations'} } )
    {
        # Work out the average...

        my $cref = $self->{'durations'}{$what};

        my ( $state, $cumulative_time, $samples, $interval ) = @$cref;

        my $average = $cumulative_time / $samples;

        # ...and save that back into the hash.

        $self->{'durations'}{$what} = [ 'AVERAGED', $cumulative_time, $samples, $average ];

        # Also stuff the average and the key into our sort list.

        push( @list, [ $average, $what ] );
    }

    # After all that, dump the list sorted by average.

    foreach my $bref ( sort byelapsed @list )
    {
        $self->time_show( $log, $bref->[1] );
    }
}

sub byelapsed
{
    my( $a_avg, $a_what ) = @$a;
    my( $b_avg, $b_what ) = @$b;

    $b_avg <=> $a_avg;
}

1;
