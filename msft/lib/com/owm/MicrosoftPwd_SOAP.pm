package com::owm::MicrosoftPwd_SOAP;

sub getPassword($$$) {
	my ( $endpoint, $email, $log) = @_;
	my $ua = LWP::UserAgent->new;
	my $req = HTTP::Request->new("POST", $endpoint);
	$req->header("Content-Type" => "text/xml; charset=utf-8" );
	$req->header("SOAPAction" => "");

	my $body = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
			<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" 
					xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
				<soapenv:Body><getPassword xmlns=\"http://DefaultNamespace\"><email>$email</email></getPassword></soapenv:Body></soapenv:Envelope>";
	$req->content($body);
	my $resp = $ua->request($req);

	if ($resp->code == 200) {
		my $resp_str = $resp->as_string;
		if ($resp_str =~  /<getPasswordReturn>(.*)<\/getPasswordReturn>/) {
			return ($1,'',0)
		} else {
			return('',$resp_str,2)
		}
	} else {
		$log->debug("$puid:". $resp->as_string);
		return('','',1);
	}

}


1;   # Indicate successful package loading.

