#!/usr/bin/perl
###########################################################################
#
#  Copyright 2015 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Huanwen Jiao <huanwen.jiao@owmessaging.com>
#  Date:        Sep/2015
#  Version:     1.0.0 - Sep 23 2015
#
###########################################################################
package com::owm::RetrievePassword;
#use SOAP::Lite +trace;
use SOAP::Lite;
use MIME::Base64;
use Crypt::Mode::ECB;
use Crypt::CBC;
use Crypt::Cipher::AES;
use strict;
use Date::Format;
use UUID::Generator::PurePerl;
use UUID::Object;
use DateTime;
use DateTime::Format::XSD;

use Crypt::DES_PP;
use Getopt::Long;
use Sys::Hostname;


sub fault_($$) {

    my ($userNameValue, $log) = @_;
    $log->error("$userNameValue| retrievePassword failed");
    return;
}


sub retrievePassword ($$$$$$$) {
    my ($jobID, $proxy, $userNameValue, $emailValue, $sourceNameValue, $log, $keyFile) = @_;
    #keyFile is optional, if provided the method assumes we are using the V3 password retrieval method
    my $server = hostname();
    my @h = split (/\./,$server);
    $server = $h[0];

    my $uri = 'http://telstra.com/sdfcore/bsc/BPSCredentialManagement/messages/v1';
    my $soap = SOAP::Lite ->readable(1)  ->uri($uri)  ->proxy($proxy); #, ssl_opts => [ SSL_verify_mode => 0 ]
    
    my $ug = UUID::Generator::PurePerl->new();
    #my $uuid = $ug->generate_v4()->as_string();
    my $uuid =  "migration_jobid_$jobID";
    #my $timeStamp = time2str("%Z,%Y-%m-%dT%T\n", time());
    my $timeStamp = DateTime::Format::XSD->format_datetime( DateTime->now);
    
    my $requestApplicationLabel =    SOAP::Header->name('requestApplicationLabel' => 'openwave_migration_01') -> prefix("v1");
    my $requestSessionID =    SOAP::Header->name('requestSessionID' => $uuid)-> prefix("v1");
    my $requestTransactionID = SOAP::Header->name('requestTransactionID' => $emailValue)-> prefix("v1");
    my $requestTimestamp =    SOAP::Header->name('requestTimestamp' => $timeStamp)-> prefix("v1");
    my $requestorId =    SOAP::Header->name('requestorId' => $server)-> prefix("v1");
    my $SDFConsumerRequestHeader = SOAP::Header->name('SDFConsumerRequestHeader')-> value (\SOAP::Header->value($requestApplicationLabel,$requestSessionID,$requestTransactionID,$requestTimestamp,$requestorId));
    $SDFConsumerRequestHeader ->  prefix("v1");
    $soap->headerattr({'xmlns:v1' => 'http://telstra.com/sdfcore/SDFConsumerRequestHeader/v1_0'});
    

    
    my $userName = SOAP::Data->name('UserName' => $userNameValue);
    my $emailAddress = SOAP::Data->name('EmailAddress' => $emailValue);
    my $sourceName = SOAP::Data->name('SourceName' => $sourceNameValue);
    my $som;
    {
    local $SIG{__DIE__} = 'DEFAULT';
    $soap->on_fault(sub { fault_($userNameValue,$log) });
    $som = $soap->RetrievePasswordRequest($userName, $emailAddress, $sourceName, $SDFConsumerRequestHeader);
    }
    if (! defined $som) {
     $log->error("$emailValue | Retrieve Password Request returned undefined");
     return ('',1);
   }
    
    if ($som->fault) {
        $log->error("$emailValue|".$requestTransactionID.",".$requestSessionID.",".$requestApplicationLabel.",".$requestTimestamp.",".$userNameValue.$som->faultcode .",". $som->faultstring .", code=".$som->valueof('//Fault/detail/InvalidParameterFault/code'));
        return ('',1);
     } else {
        my $requestApplicationLabel = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestApplicationLabel');
        my $requestTransactionID = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestTransactionID');
        my $requestSessionID = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestSessionID');
        my $requestTimestamp = $som->valueof('/Envelope/Header/SDFConsumerResponseHeader/requestTimestamp');
        my $passowrd = $som->valueof('//RetrievePasswordResponse/Password');
        my $returnCode = $som->valueof('//RetrievePasswordResponse/ReturnStatus/ReturnCode');
        my $returnMessage =  $som->valueof('//RetrievePasswordResponse/ReturnStatus/ReturnMessage');
        
        $log->info("$emailValue|".$requestTransactionID.",".$requestSessionID.",".$requestApplicationLabel.",".$requestTimestamp.",".$userNameValue.", ".$returnMessage);
        if ( $returnCode ne "0"){
            return ('',1);
        } else {
            if ($uuid ne $requestSessionID) {
                $log->error("$emailValue|requestSessionID are not equal, requestSessionID = $uuid, responsedSessionID = $requestSessionID,");
                #comment out till can handle flat file or api
                #return ('',1);
            }
            # decrypt

            #password will be encrypted if keyFile is passed to this method
            open(FH, "<", $keyFile ) or die "cannot open < $keyFile: $!";
            binmode(FH); 
            read(FH, my $key, 32, 0);
            close(FH);
            
            $passowrd = decode_base64($passowrd);
            my $iv = substr($passowrd,0,24);
            $iv =  decode_base64($iv );
            my $encryptedPassword = substr($passowrd,24);
            $encryptedPassword =  decode_base64($encryptedPassword );
            my $cbc = Crypt::CBC->new(-literal_key => 1, -cipher=>'Cipher::AES', -key=>$key, -iv=>$iv ,  -header=> 'none');
            my $ciphertext = $cbc->decrypt($encryptedPassword);
            checkLength($emailValue, $ciphertext,$log);
            return ($ciphertext,0);

        }
     }
}

sub retrievePasswordFromFile($$$) {
    my ($file,$account, $log, ) = @_;
    
    open(FH, "<", $file ) or die "cannot open < $file: $!";
    
    while ( my $data_line = <FH> ) {
        chomp($data_line);
        #print "\n$data_line\n";
        my($email, $pwd) = split( /,/, $data_line );
        if ( $account eq $email ) {
            $log->info("$account| |got password!");
            close(FH);
            checkLength($account, $pwd,$log);
            return ($pwd,0);
        }
    }
    close(FH);
    $log->error("$account|password does not exist!");
    return ('',1);

}

sub encryptDESPassword($) {
    my($password) = @_;
    my $cipher = Crypt::DES_PP->new("\x34\x87\x20\x02\x27\x12\xcc\xff");
    my $pos=0; my $encrypted="";
    while ($pos < length($password)) {
        $encrypted .= unpack("H16",$cipher->encrypt(pack("a8", substr($password, $pos)))  );
        $pos += 8;
     }
    return uc $encrypted;
}

sub decryptDESPassword($) {
    my $pwd=shift;
    my $cipher = Crypt::DES_PP->new("\x34\x87\x20\x02\x27\x12\xcc\xff");
    my $dec=""; my $p;
    my @parts = $pwd =~ /([0-9a-f]{16})/gi;
    foreach $p (@parts) { $dec.=$cipher->decrypt(pack("H16",uc($p)));    }
    if ($dec =~ /^(.+?)\x00/) {    $dec=$1;     }
    return $dec;
}

sub checkLength($$$) {
    my ($email,$string, $log) = @_;
    my $len = length($string);
    if ( $len < 4 ) {
        $log->warn("$email|password length is $len");
    }
}

1;



