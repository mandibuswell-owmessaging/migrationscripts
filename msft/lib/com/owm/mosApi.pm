package com::owm::mosApi;
##############################################################################################################
#
#  Copyright 2014 Openwave Messaging.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:                 mosApi.pm
#  Description:             Implement parts of the Openwave Messaging mOS API
#  Author:                  Andrew Perkins - Openwave Messaging
#  Date:                    2013
#  Customer:              This is a general perl module that can be used in multiple mOS installs.
#  Version:               2.0.0 - Sept 2014
#
#  Additional info...
#  mOS Version supported:   There has not been any firmal testing with mOS version, but has been used with mOS 2.1.30-2 and
#                           earlier versions of mOS.
#                           
#############################################################################################################
#
#  Version Control
# 
#  Version: 2.0.0
#  Changes:
#    - Started documenting versions of script.
#    - Add 'userCreate' and 'userDelete' subroutines to provision new subs
#
#  Version: 2.1.0
#  Changes:
#    - Add mOS API 'task' calls.
#
##############################################################################################################

use strict;
use JSON;
use URI;
#use HTTP::Request;
use HTTP::Request::Common;
use HTTP::Response;
use LWP::UserAgent;
use UUID::Generator::PurePerl;
use UUID::Object;

##############################################################################################################
# Global variables
##############################################################################################################
$| = 1; # Makes STDOUT flush immediately
my $DEBUG = 0; # 0=off, 1=on.

##############################################################################################################
# new
# The object constructor
# Aurgment: host, port (optional)
##############################################################################################################

sub new
{
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self  = {};
  if (@_ == 1) {
    $self->{HOST}      = shift;
    $self->{PORT}      = 8081;
    $self->{BASE_URI}  = "http://".$self->{HOST}.":".$self->{PORT}."/mxos/";
  } elsif (@_ == 2) {
    $self->{HOST}      = shift;
    $self->{PORT}      = shift;
    $self->{BASE_URI}  = "http://".$self->{HOST}.":".$self->{PORT}."/mxos/";
  } else {
    die "ERROR :: mosApi->new called with invalid number of arguments\n";
  }

  $self->{UA}           = LWP::UserAgent->new;
  $self->{UA}->cookie_jar({});  # Set in memory cookie jar.

  $self->{SESSION}      = undef;
  $self->{OXSESSION}    = undef;
  $self->{OXCOOKIE}     = undef;
  $self->{RANDOM}       = undef;
  $self->{ERROR}        = undef;
  $self->{RESPONSE}     = HTTP::Response->new;
  $self->{CALL_URI}     = undef;
  $self->{USER}         = undef;
  $self->{CONTENT_TYPE} = undef;

  bless($self,$class);
  return $self;
}


##############################################################################################################
# host
# Set or return host value
# Aurgment: host (optional)
##############################################################################################################
sub host
{
  my $self = shift;
  if (@_) { $self->{HOST} = shift }
  return $self->{HOST};
}

##############################################################################################################
# port
# Set or return port value
# Aurgment: port (optional)
##############################################################################################################
sub port
{
  my $self = shift;
  if (@_) { $self->{PORT} = shift }
  return $self->{PORT};
}

##############################################################################################################
# session
# Return session value
# Aurgment: <NONE>
##############################################################################################################
sub session
{
  my $self = shift;
  return $self->{SESSION};
}

##############################################################################################################
# random
# Return random value
# Aurgment: <NONE>
##############################################################################################################
sub random
{
  my $self = shift;
  return $self->{RANDOM};
}

##############################################################################################################
# oxsession
# Return oxsession value
# Aurgment: <NONE>
##############################################################################################################
sub oxsession
{
  my $self = shift;
  return $self->{OXSESSION};
}

##############################################################################################################
# oxcookie
# Return oxcookie value
# Aurgment: <NONE>
##############################################################################################################
sub oxcookie
{
  my $self = shift;
  return $self->{OXCOOKIE};
}

##############################################################################################################
# user
# Return user value
# Aurgment: <NONE>
##############################################################################################################
sub user
{
  my $self = shift;
  if (@_) { $self->{USER} = shift }
  return $self->{USER};
}

##############################################################################################################
# error
# Return error value
# Aurgment: <NONE>
##############################################################################################################
sub error
{
  my $self = shift;
  return $self->{ERROR};
}

##############################################################################################################
# content_type
# Return content_type value
# Aurgment: <NONE>
##############################################################################################################
sub content_type
{
  my $self = shift;
  return $self->{CONTENT_TYPE};
}

##############################################################################################################
# base_uri
# Return base_uri value
# Aurgment: <NONE>
##############################################################################################################
sub base_uri
{
  my $self = shift;
  return $self->{BASE_URI};
}

##############################################################################################################
# is_success
# Return 0 (zero) if last request was unsuccessfully processed, otherwise non-zero.
# Aurgment: <NONE>
##############################################################################################################

sub is_success
{
  my $self = shift;
  if ($self->{ERROR} eq "") {
    return 1;
  } else {
    return 0;
  }
}

##############################################################################################################
# print_error
# Aurgment: <NONE>
##############################################################################################################
sub print_error
{
  my $self = shift;
  print STDERR $self->{ERROR};
}

##############################################################################################################
# debugOn
# Aurgment: <NONE>
##############################################################################################################
sub debugOn
{
  my $self = shift;
  $DEBUG=1;
}

##############################################################################################################
# debugOff
# Aurgment: <NONE>
##############################################################################################################
sub debugOff
{
  my $self = shift;
  $DEBUG=0;
}

##############################################################################################################
# Login
# Login to RM via OX HTTP API.
# Aurgment: username, password
##############################################################################################################
sub Login
{
  my $self = shift;
  my $user = shift;
  my $password = shift;
  if (@_ != 0) { die "ERROR :: mosApi->Login called with invalid number of arguments\n"; }

  # Set user for object.
  $self->{USER} = $user;

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/$user/login");

  # Create a POST for Login
  # By default the request is build with application/x-www-form-urlencoded content type. 
  my $req = POST( $self->{CALL_URI}, [ 'password' => $password ] );

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }


  if ($DEBUG) {
    print "-------------------------\n".$req->as_string."\n-------------------------\n";
  }
  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    my $decoded_json;
    eval {
      local $SIG{__DIE__} = 'DEFAULT';
      $decoded_json = decode_json($content);
    };
    $self->{OXSESSION} = $decoded_json->{"sessionId"};
    $self->{OXCOOKIE} = $decoded_json->{"cookieString"};
    $self->{ERROR}   = $decoded_json->{"error"};    # If login is successful, this will be set to "".
  } else {
    $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }

  if (is_success) {
    $self->{LOGGEDIN} = 1;   # If login was successful, set LOGGEDIN to non-zero.
  }
}

##############################################################################################################
# Logout
# Login to RM via OX HTTP API.
# Aurgment: username, password
##############################################################################################################
sub Logout($)
{
  my $self = shift;
  if (@_ != 0) { die "ERROR :: mosApi->Logout called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/logout");
  $self->{CALL_URI}->query_form( 'sessionId' => $self->{OXSESSION},
                                 'cookieString' => $self->{OXCOOKIE} );

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->get($self->{CALL_URI});

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    $self->{OXSESSION} = undef;
    $self->{OXCOOKIE} = undef;
    $self->{ERROR}   = "";
  } else {
    $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
  if (is_success) {
    $self->{LOGGEDIN} = 0;
  }
}

##############################################################################################################
# GetCredentials
##############################################################################################################
sub GetCredentials($)
{
  my $self = shift;
  if (@_ != 0) { die "ERROR :: mosApi->GetCredentials called with invalid number of arguments\n"; }
  my $passwordStoreType = "";
  my $password = "";

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$self->{USER}."/credentials");

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->get($self->{CALL_URI});

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    my $decoded_json;
    eval {
      local $SIG{__DIE__} = 'DEFAULT';
      $decoded_json = decode_json($content);
    };
    $passwordStoreType  = $decoded_json->{"passwordStoreType"};
    $password = $decoded_json->{"password"};
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR}   = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }

  return($passwordStoreType,$password);
}

##############################################################################################################
# setCredentials
##############################################################################################################
sub setCredentials($$$$)
{
  my $self = shift;
  my $email = shift;
  my $passwordStoreType = shift;
  my $password = shift;

  print "\n\nWARNING - setCredentials is cause LDAP issue when run at load. ONLY USE AT LOW LOAD.\n";

  #####
  # ALTERNATE, which works at load...
  #
  # my $getHashCmd = qq~imdbcontrol sp $user $domain '$password' -convert ssha1~;
  # $log->debug("$puid:set password cmd:: $getHashCmd");
  #
  # my $cmdRes = `$getHashCmd`;
  # if ( $cmdRes ne '' ) {
  #   $log->warn("$puid:imdbcontrol failed to set password:: $cmdRes");
  #   return("imdbcontrol failed to set password");
  # } else {
  #   $log->debug("$puid:imdbcontrol set password")if ($DEBUG);
  #   return("imdbcontrol set password");
  # }
  #####

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$email."/credentials");

  # Create a POST for updating contact
  my $req = POST( $self->{CALL_URI},
                  Content => [ 'password' => $password,
                               'passwordStoreType' => $passwordStoreType,
                               'preEncryptedPasswordAllowed' => 'false' ] );
  if ($DEBUG){
    print "-------------------------\n".$req->as_string."\n-------------------------\n";
  }
  
  # Do HTTP request.
  my $ua = LWP::UserAgent->new;
  my $response;
  {
      local $SIG{__DIE__} = 'DEFAULT';
      $response = $ua->request($req);
  }
  # $log->debug($req->as_string)if ($DEBUG);
  if ($DEBUG) {
    print "-------------------------\n".$response->as_string."\n-------------------------\n";
  }
  # Evaluate the response.
  if ($response->code == 200) {
    return(0);
  } else {
    print($response->as_string)if ($DEBUG);
    return(1);
  }
}

##############################################################################################################
# GetContactsAll
# Return: Array if contact IDs in default folder (folderId 22) if successful.
##############################################################################################################
sub GetContactsAll
{
  my $self = shift;
  if (@_ != 0) { die "ERROR :: mosApi->GetContactsAll called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts/base/list");
  $self->{CALL_URI}->query_form( 'sessionId' => $self->{OXSESSION},
                                 'cookieString' => $self->{OXCOOKIE} );

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->get($self->{CALL_URI});

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    my @decoded_json;
    eval {
      local $SIG{__DIE__} = 'DEFAULT';
      @decoded_json = @{decode_json($content)};
    };
    my @listContactId;
    foreach (@decoded_json) {
      if ($_->{"folderId"} == 22) {
        push(@listContactId,$_->{"contactId"});
      }
    }
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
    return (@listContactId);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# GetContact
##############################################################################################################
sub GetContact
{
  my $self = shift;
  my $contactId = shift;
  if (@_ != 0) { die "ERROR :: mosApi->GetContact called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts/$contactId/name");
  $self->{CALL_URI}->query_form( 'sessionId' => $self->{OXSESSION},
                                 'cookieString' => $self->{OXCOOKIE} );

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->get($self->{CALL_URI});

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    my $decoded_json;
    eval {
      local $SIG{__DIE__} = 'DEFAULT';
      $decoded_json = decode_json($content);
    };
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
    return ($decoded_json);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# updateContactBase
#
# Parameters - All parameter MUST be pressent (even if empty strings).
#   notes
#   pager
#   yomiFirstName
#   yomiLastName
#   yomiCompany
#   email3
#   firstName
##############################################################################################################
sub updateContactBase($$$$$$$$$$)
{
  my $self = shift;
  my $contactId = shift;
  my %postBody = ();
  $postBody{notes} = shift;
  $postBody{pager} = shift;
  $postBody{yomiFirstName} = shift;
  $postBody{yomiLastName} = shift;
  $postBody{yomiCompany} = shift;
  $postBody{email3} = shift;
  $postBody{firstName} = shift;
  $postBody{otherPhone} = shift;
  if (@_ != 0) { die "ERROR :: mosApi->updateContactBase called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts/$contactId/base");

  # Build the actual POST body (any parameter which are empty are not included).
  my %postBodyActual = ();
  foreach my $key (keys %postBody) {
    if ( $postBody{$key} ne '' ) {
      $postBodyActual{$key} = $postBody{$key};
    }
  }

  # If this is a OX session, add OX sessionId and cookie to POST body.
  if ( $self->{OXSESSION} ne '' ) {
    $postBodyActual{sessionId} = $self->{OXSESSION};
    $postBodyActual{cookieString} = $self->{OXCOOKIE};
  }

  # Create POST 
  # By default the request is build with application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, \%postBodyActual );

  if ($DEBUG){
    print "-------------------------\n".$req->as_string."\n-------------------------\n";
  }
  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# createContact
#
# Parameters - 
#   Parameter 1 : A refence to a hash, where the hash contains fieldname / value pairs of contact to be created. 
##############################################################################################################
sub createContact($$)
{
  my $self = shift;
  my $postBodyRef = shift;
  my %postBody = %$postBodyRef;
  if (@_ != 0) { die "ERROR :: mosApi->createContact called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts");

  # Build the actual POST body (any parameter which are empty are not included).
  my %postBodyActual = ();
  foreach my $key (keys %postBody) {
    if ( $postBody{$key} ne '' ) {
      $postBodyActual{$key} = $postBody{$key};
    }
  }

  # If this is a OX session, add OX sessionId and cookie to POST body.
  if ( $self->{OXSESSION} ne '' ) {
    $postBodyActual{sessionId} = $self->{OXSESSION};
    $postBodyActual{cookieString} = $self->{OXCOOKIE};
  }

  # Create PUT (Can't make PUT directly, so first make POST, then turn it into PUT).
  # By default the request is build with application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, \%postBodyActual );
  $req->method('PUT');

  if ($DEBUG) {
    print "-------------------------\n".$req->as_string."\n-------------------------\n";
  }
  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# createMultiContact
#
# Parameters -
#   Parameter 1 : Path to the file containing form-data (one line per contact), like:
#     firstName=abc1&lastName=def1&displayName=abc1
#     firstName=abc2&lastName=def2&displayName=abc2
#     firstName=abc3&lastName=def3&displayName=abc3
#   Return -
#     If successful, an array of contact IDs.
##############################################################################################################
sub createMultiContact($$)
{
  my $self = shift;
  my $filePath = shift;
  if (@_ != 0) { die "ERROR :: mosApi->createMultiContact called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts");
  
  my $req = POST $self->{CALL_URI},
         Content_Type => 'form-data',
         Content      => [ file   => [$filePath],
                           'sessionId' => $self->{OXSESSION},
                           'cookieString' => $self->{OXCOOKIE} ];
  if ($DEBUG){
    print "-------------------------\n".$req->as_string."\n-------------------------\n";
  }
   
  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    my @decoded_json;
    eval {
      local $SIG{__DIE__} = 'DEFAULT';
      @decoded_json = @{decode_json($content)};
    };
    $self->{ERROR} = "";    # If operation is successful, this will be set to "".
    return (@decoded_json);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}
##############################################################################################################
# deleteContact
#
# Parameters -
##############################################################################################################
sub deleteContact($$)
{
  my $self = shift;
  my $contactId = shift;
  if (@_ != 0) { die "ERROR :: mosApi->deleteContact called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts/$contactId");
  $self->{CALL_URI}->query_form( 'sessionId' => $self->{OXSESSION},
                                 'cookieString' => $self->{OXCOOKIE} );

  # Create a PUT for updating contact
  # For PUT, need to add application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI} );

  $req->method('DELETE');
  $req->content_type('application/x-www-form-urlencoded');

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}


##############################################################################################################
# updateContactName
##############################################################################################################
sub updateContactName($$$$$$$$$)
{
  my $self = shift;
  my $contactId = shift;
  my %postBody = ();
  $postBody{firstName} = shift;
  $postBody{middleName} = shift;
  $postBody{lastName} = shift;
  $postBody{displayName} = shift;
  $postBody{nickName} = shift;
  $postBody{prefix} = shift;
  $postBody{suffix} = shift;
  if (@_ != 0) { die "ERROR :: mosApi->updateContactName called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts/$contactId/name");

  # Build the actual POST body (any parameter which are empty are not included).
  my %postBodyActual = ();
  foreach my $key (keys %postBody) {
    if ( $postBody{$key} ne '' ) {
      $postBodyActual{$key} = $postBody{$key};
    }
  } 
  
  # If this is a OX session, add OX sessionId and cookie to POST body.
  if ( $self->{OXSESSION} ne '' ) { 
    $postBodyActual{sessionId} = $self->{OXSESSION};
    $postBodyActual{cookieString} = $self->{OXCOOKIE};
  }

  # Create POST
  # By default the request is build with application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, \%postBodyActual );
  if ($DEBUG){
    print "-------------------------\n".$req->as_string."\n-------------------------\n";
  }
  
  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# updateContactPersonalInfo
##############################################################################################################
sub updateContactPersonalInfo($$$$$$)
{
  my $self = shift;
  my $contactId = shift;
  my %postBody = ();
  $postBody{maritalStatus} = shift;
  $postBody{spouseName} = shift;
  $postBody{children} = shift;
  $postBody{homeWebPage} = shift;
  if (@_ != 0) { die "ERROR :: mosApi->updateContactPersonalInfo called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts/$contactId/personalInfo");

  # Build the hash for the actual POST body (any parameter which are empty are not included).
  my %postBodyActual = ();
  foreach my $key (keys %postBody) {
    if ( $postBody{$key} ne '' ) {
      $postBodyActual{$key} = $postBody{$key};
    }
  }

  # If this is a OX session, add OX sessionId and cookie to POST body.
  if ( $self->{OXSESSION} ne '' ) {
    $postBodyActual{sessionId} = $self->{OXSESSION};
    $postBodyActual{cookieString} = $self->{OXCOOKIE};
  }

  # Create POST
  # By default the request is build with application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, \%postBodyActual );

  if ($DEBUG) { 
    print "-------------------------\n".$req->as_string."\n-------------------------\n";
  }
  
  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# updateContactPersonalInfoAddr
##############################################################################################################
sub updateContactPersonalInfoAddr($$$$$$$$)
{
  my $self = shift;
  my $contactId = shift;
  my %postBody = ();
  $postBody{street} = shift;
  $postBody{city} = shift;
  $postBody{stateOrProvince} = shift;
  $postBody{postalCode} = shift;
  $postBody{country} = shift;
  $postBody{personalInfoAdress} = shift;
  if (@_ != 0) { die "ERROR :: mosApi->updateContactPersonalInfoAddr called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts/$contactId/personalInfo/address");

  # Build the actual POST body (any parameter which are empty are not included).
  my %postBodyActual = ();
  foreach my $key (keys %postBody) {
    if ( $postBody{$key} ne '' ) {
      $postBodyActual{$key} = $postBody{$key};
    }
  }
 
  # If this is a OX session, add OX sessionId and cookie to POST body.
  if ( $self->{OXSESSION} ne '' ) {
    $postBodyActual{sessionId} = $self->{OXSESSION};
    $postBodyActual{cookieString} = $self->{OXCOOKIE};
  }

  # Create POST
  # By default the request is build with application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, \%postBodyActual );

  if ($DEBUG) { 
    print "-------------------------\n".$req->as_string."\n-------------------------\n";
  }
  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# updateContactPersonalInfoComm
##############################################################################################################
sub updateContactPersonalInfoComm($$$$$$$$$)
{
  my $self = shift;
  my $contactId = shift;
  my %postBody = ();
  $postBody{phone1} = shift;
  $postBody{phone2} = shift;
  $postBody{voip} = shift;
  $postBody{mobile} = shift;
  $postBody{fax} = shift;
  $postBody{email} = shift;
  $postBody{imAddress} = shift;
  if (@_ != 0) { die "ERROR :: mosApi->updateContactPersonalInfoComm called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts/$contactId/personalInfo/communication");

  # Build the actual POST body (any parameter which are empty are not included).
  my %postBodyActual = ();
  foreach my $key (keys %postBody) {
    if ( $postBody{$key} ne '' ) {
      $postBodyActual{$key} = $postBody{$key};
    }
  }
 
  # If this is a OX session, add OX sessionId and cookie to POST body.
  if ( $self->{OXSESSION} ne '' ) {
    $postBodyActual{sessionId} = $self->{OXSESSION};
    $postBodyActual{cookieString} = $self->{OXCOOKIE};
  }

  # Create POST
  # By default the request is build with application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, \%postBodyActual );

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# updateContactWorkInfo
##############################################################################################################
sub updateContactWorkInfo($$$$$$$$$$$)
{
  my $self = shift;
  my $contactId = shift;
  my %postBody = ();
  $postBody{profession} = shift;
  $postBody{companyName} = shift;
  $postBody{department} = shift;
  $postBody{employeeId} = shift;
  $postBody{manager} = shift;
  $postBody{title} = shift;
  $postBody{url} = shift;
  $postBody{assistant} = shift;
  $postBody{assistantPhone} = shift;
  if (@_ != 0) { die "ERROR :: mosApi->updateContactWorkInfo called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts/$contactId/workInfo");

  # Build the actual POST body (any parameter which are empty are not included).
  my %postBodyActual = ();
  foreach my $key (keys %postBody) {
    if ( $postBody{$key} ne '' ) {
      $postBodyActual{$key} = $postBody{$key};
    }
  }
 
  # If this is a OX session, add OX sessionId and cookie to POST body.
  if ( $self->{OXSESSION} ne '' ) {
    $postBodyActual{sessionId} = $self->{OXSESSION};
    $postBodyActual{cookieString} = $self->{OXCOOKIE};
  }

  # Create POST
  # By default the request is build with application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, \%postBodyActual );

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# updateContactWorkInfoAddr
##############################################################################################################
sub updateContactWorkInfoAddr($$$$$$$$)
{
  my $self = shift;
  my $contactId = shift;
  my %postBody = ();
  $postBody{street} = shift;
  $postBody{city} = shift;
  $postBody{stateOrProvince} = shift;
  $postBody{postalCode} = shift;
  $postBody{country} = shift;
  $postBody{workInfoAddress} = shift;
  if (@_ != 0) { die "ERROR :: mosApi->updateContactWorkInfoAddr called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts/$contactId/workInfo/address");

  # Build the actual POST body (any parameter which are empty are not included).
  my %postBodyActual = ();
  foreach my $key (keys %postBody) {
    if ( $postBody{$key} ne '' ) {
      $postBodyActual{$key} = $postBody{$key};
    }
  }
 
  # If this is a OX session, add OX sessionId and cookie to POST body.
  if ( $self->{OXSESSION} ne '' ) {
    $postBodyActual{sessionId} = $self->{OXSESSION};
    $postBodyActual{cookieString} = $self->{OXCOOKIE};
  }

  # Create POST
  # By default the request is build with application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, \%postBodyActual );

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# updateContactWorkInfoComm
##############################################################################################################
sub updateContactWorkInfoComm($$$$$$$$$)
{
  my $self = shift;
  my $contactId = shift;
  my %postBody = ();
  $postBody{phone1} = shift;
  $postBody{phone2} = shift;
  $postBody{voip} = shift;
  $postBody{mobile} = shift;
  $postBody{fax} = shift;
  $postBody{email} = shift;
  $postBody{imAddress} = shift;
  if (@_ != 0) { die "ERROR :: mosApi->updateContactWorkInfoComm called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts/$contactId/workInfo/communication");

  # Build the actual POST body (any parameter which are empty are not included).
  my %postBodyActual = ();
  foreach my $key (keys %postBody) {
    if ( $postBody{$key} ne '' ) {
      $postBodyActual{$key} = $postBody{$key};
    }
  }
 
  # If this is a OX session, add OX sessionId and cookie to POST body.
  if ( $self->{OXSESSION} ne '' ) {
    $postBodyActual{sessionId} = $self->{OXSESSION};
    $postBodyActual{cookieString} = $self->{OXCOOKIE};
  }

  # Create POST
  # By default the request is build with application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, \%postBodyActual );

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# signatureList
##############################################################################################################
sub signatureList($$)
{
  my $self = shift;
  my $target = shift;    # "appsuite" or "mss"

  if (@_ != 0)
  {
    die 'ERROR :: mosApi->signatureList called with invalid number of arguments'."\n";
  }

  if ($target != 'mss')
  {
    die 'ERROR :: mosApi->signatureList called with invalid "target"'."\n";
  }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  my $req;
  my $uri = $base_uri.'mailbox/v2/'.$self->{USER}.'/mailSend';
  $self->{CALL_URI} = URI->new($uri); # For listing sig in MSS
  $req = POST($self->{CALL_URI});
  $req->method('GET');

  my $divider = '-'x25 . "\n";

  print $divider.$req->as_string."\n".$divider if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  print $divider.$self->{RESPONSE}->as_string."\n".$divider if ($DEBUG);

  if ($self->{RESPONSE}->code == 200)
  {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  }
  else
  {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return(undef);
  }

  my @ids;
  if ($self->{RESPONSE}->content !~ /signatureId/)
  {
    return(\@ids);
  }

  my $content = $self->{RESPONSE}->content;
  while ($content =~ /"signatureId":"([^"]+)"/g)
  {
    push(@ids, $1);
  }
  return(\@ids);
}

##############################################################################################################
# signatureDelete
##############################################################################################################
sub signatureDelete($$$)
{
  my $self = shift;
  my $target = shift;    # "appsuite" or "mss"
  my $sigId = shift;

  if (@_ != 0)
  {
    die 'ERROR :: mosApi->signatureDelete called with invalid number of arguments'."\n";
  }

  if ($target != "mss")
  {
    die 'ERROR :: mosApi->signatureDelete called with invalid "target"'."\n";
  }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  my $req;
  my $uri = $base_uri.'mailbox/v2/'.$self->{USER}.'/mailSend/signatures/'.$sigId;
  $self->{CALL_URI} = URI->new($uri); # For deleting sig in MSS
  $req = POST($self->{CALL_URI});
  $req->method('DELETE');

  my $divider = '-'x25 . "\n";

  print $divider.$req->as_string."\n".$divider if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  print $divider.$self->{RESPONSE}->as_string."\n".$divider if ($DEBUG);

  if ($self->{RESPONSE}->code == 200)
  {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  }
  else
  {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return(undef);
  }
}

##############################################################################################################
# signatureCreate
##############################################################################################################
sub signatureCreate($$$$$)
{
  my $self = shift;
  my $target = shift;    # "appsuite" or "mss"
  my $signature = shift;
  my $label = shift;
  my $isDefault = shift;
  if (@_ != 0) { die "ERROR :: mosApi->signatureCreate called with invalid number of arguments\n"; }
  if (($target != "mos") && ($target != "mss")) { die "ERROR :: mosApi->signatureCreate called with invalid 'target'\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  my $req;

  print "\ntarget=$target\n"  if ($DEBUG);
  print "\nsignature=$signature\n"  if ($DEBUG);

  if ($target eq "appsuite" || $target eq "mss" ) {

    # target = appsuite...
    $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$self->{USER}."/mailSend/signatures"); # For setting sig in AppSuite

    # Create a PUT for updating contact
    # For PUT, need to add application/x-www-form-urlencoded content type.
    $req = POST( $self->{CALL_URI}, 
                 Content => [ 'signatureName' => $label,
                              'signature' => $signature,
                              'isDefault' => $isDefault ] );

    $req->method('PUT');
    $req->content_type('application/x-www-form-urlencoded');

  } else {

    # target = mss...
    $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$self->{USER}."/mailSend");            # For setting sig in MSS.

    # Create a POST for updating contact
    $req = POST( $self->{CALL_URI},   
                 Content => [ 'signature' => $signature ] );

  }

  print "-------------------------\n".$req->as_string."\n-------------------------\n" if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  print "-------------------------\n".$self->{RESPONSE}->as_string."\n-------------------------\n" if ($DEBUG);

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
    return('');
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ($self->{ERROR});
  }
}

#############################################################################
# MailSendPreference
#############################################################################
sub MailSendPreference($$)
{
    my ($self, $settingStr) = @_;

    $self->{ERROR} = ""; # Set this to "" so is_success has this calls status.   
    my $base_uri = $self->{BASE_URI};
    $self->{CALL_URI} = URI->new($base_uri.'mailbox/v2/'.$self->{USER}.
        '/mailSend');
    my $req = POST $self->{CALL_URI}, Content => $settingStr;
    $req->content_type('application/x-www-form-urlencoded');

    print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

    # Do HTTP request.
    {
        local $SIG{__DIE__} = 'DEFAULT';
        $self->{RESPONSE} = $self->{UA}->request($req);
    }

    # Evaluate the response.
    my $decoded_json;
    if ($self->{RESPONSE}->code == 200)
    {
        return(undef); 
    }
    elsif ($self->{RESPONSE}->code == 400)
    {
        my $content = $self->{RESPONSE}->content;
        $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
        return($content);
    }
    else
    {
        $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
        return($self->{ERROR});
    }
}

##############################################################################################################
# GetContactImage
##############################################################################################################
sub GetContactImage
{
  my $self = shift;
  my $contactId = shift;
  my $imageType = shift;
  if (@_ != 0) { die "ERROR :: mosApi->GetContactImage called with invalid number of arguments\n"; }

  print "GetContactImage : imageType = $imageType\n" if ($DEBUG);

  if ( $imageType eq "" ) {
    # Default is to get the URL of the image.
    $imageType = "image"; 
  } elsif ( $imageType eq "image" ) {
  } elsif ( $imageType eq "actualImage" ) {
  } else {
    die "ERROR :: mosApi->GetContactImage called with invalid imageType. imageType = $imageType\n";
  }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts/$contactId/$imageType");
  $self->{CALL_URI}->query_form( 'sessionId' => $self->{OXSESSION},
                                 'cookieString' => $self->{OXCOOKIE} );

  # Do HTTP request.
  $self->{RESPONSE} = $self->{UA}->get($self->{CALL_URI});

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
    # $decoded_json = decode_json($content);
    # return ($decoded_json);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# SetContactImage
##############################################################################################################
sub SetContactImage
{
  my $self = shift;
  my $contactId = shift;
  my $imageString = shift;
  if (@_ != 0) { die "ERROR :: mosApi->SetContactImage called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/contacts/$contactId/image");

  # Create a POST for updating contact
  # By default the request is build with application/x-www-form-urlencoded content type.
  my  $req = POST $self->{CALL_URI},
         Content      => [ 'actualImage'  => $imageString,
                           'cookieString' => $self->{OXCOOKIE},
                           'sessionId' => $self->{OXSESSION}
                         ];

  print "-------------------------\n".$req->as_string."\n-------------------------\n" if ($DEBUG);

#  my $req = POST( $self->{CALL_URI}, [ 'sessionId' => $self->{OXSESSION},
#                                       'cookieString' => $self->{OXCOOKIE},
#                                       'actualImage' => $imageString ] );

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    # $decoded_json = decode_json($content);
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
    return ($decoded_json);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# createGroup
#
# Parameters -
#   NONE
# Return -
#   If successful, the new group ID.
##############################################################################################################
sub createGroup($$)
{
  my $self = shift;
  my $groupName = shift;
  if (@_ != 0) { die "ERROR :: mosApi->createGroup called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/groups");

  # Build the actual POST body (any parameter which are empty are not included).
  my %postBodyActual = ('groupName' => $groupName);

  # If this is a OX session, add OX sessionId and cookie to POST body.
  if ( $self->{OXSESSION} ne '' ) {
    $postBodyActual{sessionId} = $self->{OXSESSION};
    $postBodyActual{cookieString} = $self->{OXCOOKIE};
  }

  # Create PUT (Can't make PUT directly, so first make POST, then turn it into PUT).
  # By default the request is build with application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, \%postBodyActual );
  $req->method('PUT');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
    return($self->{RESPONSE}->content);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# deleteGroup
#
# Parameters -
#   groupId (optional)
#
# If groupId parameter provided, only that group is deleted.
# If no groupId parameter provided, ALL groups are deleted.
##############################################################################################################
sub deleteGroup
{
  my $self = shift;
  my $groupId;

  # Check if subroutine was called with optional image arg.
  if (@_ == 0) {
    $groupId = undef;
  } elsif (@_ == 1) {
    $groupId = shift;
  } else {
    die "ERROR :: OxHttpApi->deleteGroup called with invalid number of arguments\n";
  }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  if ( $groupId ) {
    # Delete only group $groupId.
    $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/groups/$groupId");
  } else {
    # Delete ALL groups.
    $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/groups");
  }
  $self->{CALL_URI}->query_form( 'sessionId' => $self->{OXSESSION},
                                 'cookieString' => $self->{OXCOOKIE} );

  # Create a PUT for updating contact
  # For PUT, need to add application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI} );

  $req->method('DELETE');
  $req->content_type('application/x-www-form-urlencoded');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# getGroup
#
# Parameters -
#   groupId (optional)
#
# If groupId parameter provided, only that group is got.
# If no groupId parameter provided, ALL groups are got.
##############################################################################################################
sub getGroup
{
  my $self = shift;
  my $groupId;

  # Check if subroutine was called with optional image arg.
  if (@_ == 0) {
    $groupId = undef;
  } elsif (@_ == 1) {
    $groupId = shift;
  } else {
    die "ERROR :: OxHttpApi->getGroup called with invalid number of arguments\n";
  }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  if ( $groupId ) {
    # Get only group $groupId.
    $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/groups/$groupId/base");
  } else {
    # Get ALL groups.
    $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/groups/base/list");
  }
  $self->{CALL_URI}->query_form( 'sessionId' => $self->{OXSESSION},
                                 'cookieString' => $self->{OXCOOKIE} );

  my $req = GET( $self->{CALL_URI} );

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    return($self->{RESPONSE}->content);
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# createGroupMember
#
# Parameters -
#   groupId
# Return -
#   If successful, the new group ID.
##############################################################################################################
sub createGroupMember($$)
{
  my $self = shift;
  my $groupId = shift;
  my $contactId = shift;
  if (@_ != 0) { die "ERROR :: mosApi->createGroupMember called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/groups/$groupId/members/$contactId");

  # If this is a OX session, add OX sessionId and cookie to POST body.
  my %postBodyActual;
  if ( $self->{OXSESSION} ne '' ) {
    $postBodyActual{sessionId} = $self->{OXSESSION};
    $postBodyActual{cookieString} = $self->{OXCOOKIE};
  }

  # Create PUT (Can't make PUT directly, so first make POST, then turn it into PUT).
  # By default the request is build with application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, \%postBodyActual );
  $req->method('PUT');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
    return($self->{RESPONSE}->content);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# createMultiGroupMember
#
# Parameters -
#   Parameter 1 : groupId of group where members are added.
#   Parameter 2 : Path to the file containing form-data (one line per group member), like:
#     memberName=abcd1&memberEmail=foo1@bar.com&memberFolderId=22&mailField=firstName1
#     memberName=abcd2&memberEmail=foo2@bar.com&memberFolderId=22&mailField=firstName
#   Return -
#     If successful, an array of contact IDs.
##############################################################################################################
sub createMultiGroupMember($$)
{
  my $self = shift;
  my $groupId = shift;
  my $filePath = shift;
  if (@_ != 0) { die "ERROR :: mosApi->createMultiGroupMember called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."addressBook/v2/".$self->{USER}."/groups/$groupId/members");

  my $req = POST $self->{CALL_URI},
         Content_Type => 'form-data',
         Content      => [ file   => [$filePath],
                           'sessionId' => $self->{OXSESSION},
                           'cookieString' => $self->{OXCOOKIE} ];

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    $self->{ERROR} = "";    # If operation is successful, this will be set to "".
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# userCreate
##############################################################################################################
sub userCreate
{
  my $self = shift;
  my $user = shift;
  my $pwd = shift;
  my $pwdType = shift;
  my $cos = shift;
  my $msgStoreHost = shift;
  my $MbExistsRtnError = shift;
  my $status = shift;
  my $billingId = shift;
  my $mailboxId = shift;
  my $popProxyHost = shift;
  my $imapProxyHost = shift;
  my $smtpProxyHost = shift;
  my $autoReplyHost = shift;
  my $userName = shift; # AKA maillogin

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$user);

  # Create a PUT for updating contact
  # For PUT, need to add application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, Content => [ 'userName' => $userName,
                                                  'password' => $pwd,
                                                  'passwordStoreType' => $pwdType,
                                                  'cosId' => $cos,
                                                  'messageStoreHost' => $msgStoreHost,
                                                  'LdapMailboxExistsReturnError' => $MbExistsRtnError,
                                                  'status' => $status,
                                                  'billingId' => $billingId,
                                                  'mailboxId' => $mailboxId,
                                                  'popProxyHost' => $popProxyHost,
                                                  'imapProxyHost' => $imapProxyHost,
                                                  'smtpProxyHost' => $smtpProxyHost,
                                                  'autoReplyHost' => $autoReplyHost,
                                                   ] );

  $req->method('PUT');
  $req->content_type('application/x-www-form-urlencoded');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
#    $decoded_json = decode_json($content);
#    return ($decoded_json);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# userUpdateMaillogin
##############################################################################################################
sub userUpdateMaillogin
{
  my $self = shift;
  my $user = shift;
  my $userName = shift; # AKA maillogin

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$user."/base");

  # Create a PUT for updating contact
  # For PUT, need to add application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, Content => [ 'userName' => $userName, ] );

#  $req->method('PUT');
  $req->content_type('application/x-www-form-urlencoded');

#$DEBUG = 1;
  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  print "-------------------------\n".$self->{RESPONSE}->as_string."\n-------------------------\n"if ($DEBUG);

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
#    $decoded_json = decode_json($content);
#    return ($decoded_json);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# userDelete
##############################################################################################################
sub userDelete
{
  my $self = shift;
  my $user = shift;
  if (@_ != 0) { die "ERROR :: mosApi->userDelete called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$user);

  # Create a DELETE for updating contact
  # For DELETE, need to add application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI} );

  $req->method('DELETE');
  $req->content_type('application/x-www-form-urlencoded');

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

##############################################################################################################
# CreateSenderBlockAllow
##############################################################################################################
sub CreateSenderBlockAllow
{
  my $self = shift;
  my $filterStr = shift;
  my $blockOrAllow = shift;
  if (@_ != 0) { die "ERROR :: mosApi->CreateSenderBlockAllow called with invalid number of arguments\n"; }
  if ( ($blockOrAllow ne 'blocked') && ($blockOrAllow ne 'allowed') ) {
    die "ERROR :: mosApi->CreateSenderBlockAllow called with invalid parameter. Need 'block' or 'allow'. blockOrAllow = $blockOrAllow\n";
  }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  if ($blockOrAllow eq 'blocked') {
    $self->{CALL_URI} =
      URI->new($base_uri."mailbox/v2/".$self->{USER}."/mailReceipt/senderBlocking/blockedSendersList");
  } else {
    $self->{CALL_URI} =
      URI->new($base_uri."mailbox/v2/".$self->{USER}."/mailReceipt/senderBlocking/allowedSendersList");
  }


  # Create a PUT for updating contact
  # For PUT, need to add application/x-www-form-urlencoded content type.
  my $req = POST $self->{CALL_URI}, Content => $filterStr;

  $req->method('PUT');
  $req->content_type('application/x-www-form-urlencoded');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
    # $decoded_json = decode_json($content);
    # return ($decoded_json);
  } elsif ($self->{RESPONSE}->code == 400) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    $self->{CONTENT_TYPE} = $self->{RESPONSE}->header('Content-Type');
    return ($content);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    $self->{CONTENT_TYPE} = $self->{RESPONSE}->header('Content-Type');
    return ($self->{ERROR});
  }
}

##############################################################################################################
# SetForwardingSettings
##############################################################################################################
sub SetForwardingSettings
{
  my $self = shift;
  my $setStr = shift;
  if (@_ != 0) { die "ERROR :: mosApi->SetForwarding called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$self->{USER}."/mailReceipt");

  # Create a POST for updating contact
  my $req = POST $self->{CALL_URI}, Content => $setStr;

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
#    $decoded_json = decode_json($content);
#    return ($decoded_json);
  } elsif ($self->{RESPONSE}->code == 400) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    # $decoded_json = decode_json($content);
    return ($content);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ($self->{ERROR});
  }
}

##############################################################################################################
# GetForwardingAddresses
##############################################################################################################
sub GetForwardingAddresses
{
  my $self = shift;
  if (@_ != 0) { die "ERROR :: mosApi->GetForwardingAddresses called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$self->{USER}."/mailReceipt/forwardingAddress");


  # Create a PUT for updating contact
  # For PUT, need to add application/x-www-form-urlencoded content type.
  my $req = GET($self->{CALL_URI});

  $req->content_type('application/x-www-form-urlencoded');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    my $decoded_json;
    $self->{CONTENT_TYPE} = $self->{RESPONSE}->header('Content-Type');
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
    if ($self->{CONTENT_TYPE} eq 'application/json') {
      eval {
        $decoded_json = decode_json($content);
      };
      return($decoded_json, 0);
    } else {
      $self->{ERROR} = 'Did not get JSON back from mOS API.  HTTP return code: '.
        $self->{RESPONSE}->code." ".$self->{RESPONSE}->message;
      return($content, 1);
    }
#    $decoded_json = decode_json($content);
#    return ($decoded_json);
  } elsif ($self->{RESPONSE}->code == 400) {
    my $content = $self->{RESPONSE}->content;
    $self->{CONTENT_TYPE} = $self->{RESPONSE}->header('Content-Type');
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    # $decoded_json = decode_json($content); #must use eval to catch any decoding error. as $content is returned, unsure why JSON decoding is required
    return ($content, 1);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ($self->{ERROR}, 1);
  }
}

##############################################################################################################
# DeleteForwardingAddresses
##############################################################################################################
sub DeleteForwardingAddress
{
  my $self = shift;
  my $address = shift;
  if (@_ != 0) { die "ERROR :: mosApi->DeleteForwardingAddress called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$self->{USER}."/mailReceipt/forwardingAddress/".$address);

  my $req = POST($self->{CALL_URI});
  $req->method('DELETE');
  $req->content_type('application/x-www-form-urlencoded');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
    return(undef);
  } elsif ($self->{RESPONSE}->code == 400) {
    my $content = $self->{RESPONSE}->content;
    $self->{CONTENT_TYPE} = $self->{RESPONSE}->header('Content-Type');
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ($content);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ($self->{ERROR});
  }
}


##############################################################################################################
# SetForwardingAddresses
##############################################################################################################
sub SetForwardingAddresses
{
  my $self = shift;
  my $addressStr = shift;
  if (@_ != 0) { die "ERROR :: mosApi->SetForwardingAddresses called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$self->{USER}."/mailReceipt/forwardingAddress");


  # Create a PUT for updating contact
  # For PUT, need to add application/x-www-form-urlencoded content type.
  my $req = POST $self->{CALL_URI}, Content => $addressStr;

  $req->method('PUT');
  $req->content_type('application/x-www-form-urlencoded');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
#    $decoded_json = decode_json($content);
#    return ($decoded_json);
  } elsif ($self->{RESPONSE}->code == 400) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    # $decoded_json = decode_json($content); #must use eval to catch any decoding error. as $content is returned, unsure why JSON decoding is required
    return ($content);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ($self->{ERROR});
  }
}

##############################################################################################################
# SetVacation
##############################################################################################################
sub SetVacation
{
  my $self = shift;
  my $autoReplyMode = shift;
  my $autoReplyMessage = shift;
  my @putContent;
  if (@_ != 0) { die "ERROR :: mosApi->SetVacation called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$self->{USER}."/mailReceipt");

  # Create a POST for updating contact
  # If MX8 user do not set autoReplyMode
  if ( $autoReplyMode ne "MX8" ) {
    push(@putContent, 'autoReplyMode', $autoReplyMode);
  }
  push(@putContent, 'autoReplyMessage', $autoReplyMessage);
  
  my $req = POST( $self->{CALL_URI}, Content => [@putContent]);

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }
  print "-------------------------\n".$self->{RESPONSE}->as_string."\n-------------------------\n"if ($DEBUG);
  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
#    $decoded_json = decode_json($content);
#    return ($decoded_json);
  } elsif ($self->{RESPONSE}->code == 400) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    # $decoded_json = decode_json($content);
    return ($content);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ($self->{ERROR});
  }
}


##############################################################################################################
# GeneralPreference
##############################################################################################################
sub GeneralPreference
{
  my $self = shift;
  my $settingStr = shift; # eg. "locale=en" or "timezone=America/Montreal" or "locale=en&timezone=America/Montreal".
  if (@_ != 0) { die "ERROR :: mosApi->GeneralPreference called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$self->{USER}."/generalPreferences");

  # Create a POST for updating contact
  my $req = POST $self->{CALL_URI}, Content => $settingStr;
  $req->content_type('application/x-www-form-urlencoded');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
#    $decoded_json = decode_json($content);
#    return ($decoded_json);
  } elsif ($self->{RESPONSE}->code == 400) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    #$decoded_json = decode_json($content);
    return ($content);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ($self->{ERROR});
  }
}

##############################################################################################################
# updateAccountBase
#
# Parameters - All parameter MUST be pressent (even if empty strings).
#   firstName
#   lastName
##############################################################################################################
sub updateAccountBase($$$$)
{
  my $self = shift;
  $self->{USER} = shift;
  my %postBody = ();
  $postBody{firstName} = shift;
  $postBody{lastName} = shift;
  if (@_ != 0) { die "ERROR :: mosApi->updateAccountBase called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$self->{USER}."/base");

  # Build the actual POST body (any parameter which are empty are not included).
  my %postBodyActual = ();
  foreach my $key (keys %postBody) {
    if ( $postBody{$key} ne '' ) {
      $postBodyActual{$key} = $postBody{$key};
    }
  }

  # Create POST
  # By default the request is build with application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, \%postBodyActual );

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
  }
}

#############################################################################################################
# deleteAsContext
#
# Parameters
#   email
##############################################################################################################
sub deleteAsContextAndMB($$)
{
  my $self = shift;
  my $user = shift;
  if (@_ != 0) { die "ERROR :: mosApi->deleteAsContextAndMB called with invalid number of arguments\n"; }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$user);
  $self->{CALL_URI}->query_form( 'skipLDAP' => 'true', 
                                 'skipPAB' => 'true',
                                 'skipCAL' => 'true' );
 
  # Create a DELETE for updating contact
  # For DELETE, need to add application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI} );

  $req->method('DELETE');
  $req->content_type('application/x-www-form-urlencoded');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);
 
  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  print "-------------------------\n".$self->{RESPONSE}->as_string."\n-------------------------\n"if ($DEBUG);

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->content."\n";
  }
}

##############################################################################################################
# PrintResponse
# Print the response to a HTTP Request
# Aurgment: <NONE>
##############################################################################################################
sub PrintResponse
{
  my $self = shift;
  print STDOUT "\nREQUEST ::\n$self->{CALL_URI}\n\nRESPONSE ::\n".$self->{RESPONSE}->as_string."\n";
}

##############################################################################################################
# fileResponse
# Put contects of the last HTTP Request in a file
# Aurgment: 
#   filename to put contects in.
##############################################################################################################
sub fileResponse
{
  my $self = shift;
  my $file = shift;

  open( FILE, ">$file" );
  print FILE $self->{RESPONSE}->content;
  close FILE;
}

################################################################################################################
# Additional Functions for MSFT to UXSuite
#
################################################################################################################
##############################################################################################################
# userProvisionRealm
##############################################################################################################
sub userProvisionRealm
{
  my $self = shift;
  my $user = shift;
  my $pwd = shift;
  my $cos = shift;
  my $realm = shift;
  my $msgStoreHost = shift;
  my $autoReplyHost = shift;
  my $calStoreHost = shift;
  my $pabStoreHost = shift;

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  my $req = ""; #initialise
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$user);

  # Create a PUT for recreating
  # For PUT, need to add application/x-www-form-urlencoded content type.
  my @putContent = (
        'password', $pwd,
        'cosId', $cos,
        'messageStoreHost', $msgStoreHost,
        'realm', $realm,
        'autoReplyHost',$autoReplyHost,
        'skipLDAP',"true",
        'skipOX',"true",
        'skipMSS',"true"
  );

  if ( $self->getValue('calendarStorageLocation') eq "" ) {
    push(@putContent, 'calendarStoreHost', $calStoreHost);
    push(@putContent, 'skipCAL', "false");
  } else {
    push(@putContent, 'skipCAL', "true");
  }
  if ( $self->getValue('addressBookStorageLocation') eq "" ) {
    push(@putContent, 'addressBookStoreHost', $pabStoreHost);
    push(@putContent, 'skipPAB', "false");
  } else {
    push(@putContent, 'skipPAB', "true");
  }

  $req = PUT( $self->{CALL_URI}, Content => [@putContent]);
  $req->method('PUT');
  $req->content_type('application/x-www-form-urlencoded');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
#    $decoded_json = decode_json($content);
#    return ($decoded_json);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# userProvisionMSS
# recreate the MSS for a user
##############################################################################################################
sub userProvisionMSS
{
  my $self = shift;
  my $user = shift;
  my $pwd = shift;
  my $cos = shift;


  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  my $req = ""; #initialise
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$user);

  # Create a PUT for recreating
  # For PUT, need to add application/x-www-form-urlencoded content type.

  $req = PUT( $self->{CALL_URI}, Content => [ 'password' => $pwd,
                                              'cosId' => $cos,
                                              'skipLDAP' => "true",
                                              'skipOX' => "true",
                                              'skipMSS' => "false",
                                              'skipCAL' => "true",
                                              'skipPAB' => "true"
                                              ] );

  $req->method('PUT');
  $req->content_type('application/x-www-form-urlencoded');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
#    $decoded_json = decode_json($content);
#    return ($decoded_json);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# deleteMailbox
# delete the MSS for a user
##############################################################################################################
sub deleteMailbox
{
  my $self = shift;
  my $user = shift;
  my $recreate = shift;

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  my $req = ""; #initialise
  my $content = "deleteOnlyOnMss=true&suppressMers=true";
  if ($recreate eq "true") {
    $content .= "&recreateOnMss=true";
  } else {
    $content .= "&recreateOnMss=false";
  }
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$user."?".$content);

  # Create a PUT for recreating
  # For PUT, need to add application/x-www-form-urlencoded content type.
  $req = POST( $self->{CALL_URI});

  $req->method('DELETE');
  $req->content_type('application/x-www-form-urlencoded');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
#    $decoded_json = decode_json($content);
#    return ($decoded_json);
    return ("");
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}


################################################################################################################
# userUpdateMailbox
#
# Sets COS and MailboxStatus
##############################################################################################################
sub userUpdateMailbox
{
  my $self = shift;
  my $mode = shift;
  my $user = shift;
  my $mailboxStatus = shift;
  my $cosId = shift;
  my $pwd = shift;
  my $cfg = shift;
  my $log = shift;
  my $wait = $cfg->get_param('allowForReplication');
  my $doWait = 1;

  if ($mode =~ /-nowait$/i)
  {
    $doWait = 0;
    $mode =~ s/-nowait$//;
  }

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$user."/base");

  # Create a PUT for updating contact
  # For PUT, need to add application/x-www-form-urlencoded content type.
  my $req = POST( $self->{CALL_URI}, Content => [ 'status' => $mailboxStatus, 
                                                  'cosId' => $cosId,  ] );

#  $req->method('PUT');
  $req->content_type('application/x-www-form-urlencoded');

#$DEBUG = 1;
  if ($DEBUG) { 
    print "-------------------------\n".$req->as_string."\n-------------------------\n";
  }

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }
    if ($DEBUG) { 
    print "-------------------------\n".$self->{RESPONSE}->as_string."\n-------------------------\n";
  }
  
  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
    if ($doWait)
    {
      $log->debug("$user| Waiting for replication. $wait seconds");
      # Sleep, to allow time for propogation of mailbox status change.
      sleep($wait);
    }
#    $decoded_json = decode_json($content);
#    return ($decoded_json);
#    return ($com::owm::migBellCanada::STATUS_SUCCESS);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}

##############################################################################################################
# getUserBase
#
# Makes a read user call to mOS.
##############################################################################################################
sub getUserBase
{
  my $self = shift;
  my $user = shift;


  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$user);

  # Create a PUT for updating contact
  # For PUT, need to add application/x-www-form-urlencoded content type.
  my $req = GET( $self->{CALL_URI} );

#  $req->method('PUT');
  $req->content_type('application/x-www-form-urlencoded');

#$DEBUG = 1;
  if ($DEBUG) { 
  print "-------------------------\n".$req->as_string."\n-------------------------\n";
  }

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }
  if ($DEBUG) { 
    print "-------------------------\n".$self->{RESPONSE}->as_string."\n-------------------------\n";
  }

  # Evaluate the response.
  my $decoded_json;
  if ($self->{RESPONSE}->code == 200) {
    my $content = $self->{RESPONSE}->content;
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
    my $decoded_json;
    eval {
      local $SIG{__DIE__} = 'DEFAULT';
      $decoded_json = decode_json($content);
    };
    my $webmailFeatures = $decoded_json->{webMailFeatures};
    my $calendarFeatures = $webmailFeatures->{calendarFeatures};
    $self->{calendarStorageLocation} = $calendarFeatures->{calendarStorageLocation};
    my $addressBookFeatures = $webmailFeatures->{addressBookFeatures};
    $self->{addressBookStorageLocation} = $addressBookFeatures->{addressBookStorageLocation};
#    return ($decoded_json);
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return ("");
  }
}
##############################################################################################################
# getValue
#
# return the value of the $key from last HTTP request.
##############################################################################################################
sub getValue {
  my $self = shift;
  my $key = shift;
  if ($self->{$key} ne "") {
    return $self->{$key};
  } else {
    return "";
  }
}
##############################################################################################################
# renameFolder
#
# renames one folder to another
##############################################################################################################
sub renameFolder
{
  my $self = shift;
  my $user = shift;
  my $folder = shift;
  my $newfolder = shift;

  # Set up the request uri.
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$user."/folders/".$folder);
 
  my $req = POST( $self->{CALL_URI}, Content => [ 'toFolderName' => $newfolder, ] );

  $req->method('POST');
  $req->content_type('application/x-www-form-urlencoded');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);
 
  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  print "-------------------------\n".$self->{RESPONSE}->as_string."\n-------------------------\n"if ($DEBUG);

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->message."\n";
    return($self->{RESPONSE}->content);
  }
}

sub setExternalAccounts
{
  my ($self, $user, @external_account_array_ref) = @_;
  
  foreach (@external_account_array_ref) {
    my @values = split(/\|/, $_);

    # change true/false to yes/no
    foreach (@values) {
      if ($_ eq "true") {
        $_ = "yes";
      } elsif ($_ eq "false") {
        $_ = "no";
      }
    }

    # Legacy UI has no IMAP SSL capabilities, so this is set depending on the port
    my $accountType = "";
    if ($values[6] eq "993") {
      $accountType = "imapssl";
    } elsif ($values[6] eq "995") {
      $accountType = "pop3ssl";
    } elsif ($values[6] eq "143") {
      $accountType = "imap";
    } else {
      $accountType = $values[12];
      # POP3 SSL needs to be adjusted for mos
      if ($accountType eq "pop3s") {
        $accountType = "pop3ssl";
      }
    }
    if ($accountType eq '') {
      $accountType = 'pop3';
    }

    my $leaveMailOnServer = 'no';
    my $autoFetchMails = 'no';
    if ((lc($values[9]) eq 'false') || (lc($values[9]) eq 'no')) {
      $leaveMailOnServer = 'yes';
    }
    if ((lc($values[11]) eq 'true') || (lc($values[11]) eq 'yes')) {
      $autoFetchMails = 'yes';
    }

    my @putContent = (
        "accountName", $values[13] || $values[2],
        "accountPassword", $values[3],
        "accountType", $accountType,
        "accountUserName", $values[2],
        "autoFetchMails", $autoFetchMails,
        "displayImage", $values[8],
        "emailAddress", $values[5],
        "fetchedMailsFolderName", $values[10],
        "fromAddressInReply", $values[5],
        "leaveEmailsOnServer", $leaveMailOnServer,
        "mailSendSMTPAddress", $values[1], # default to the same as the incoming mail server since WebEdge doesn't have a separate field for outgoing
        "mailSendSMTPPort", 25, # Hardcoded because a value is needed here, but none is available on the legacy system
        "serverAddress", $values[1],
        "serverPort", $values[6],
        "smtpSSLEnabled", "no",
        "timeoutSecs", $values[7],
        "useSmtpLogin", "yes",
    );

    my $ug = UUID::Generator::PurePerl->new();
    my $base_uri = $self->{BASE_URI};
    $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$user."/externalAccounts/mailAccounts/".$ug->generate_v1());
    my $req = POST( $self->{CALL_URI}, Content => [@putContent] );
    $req->method('PUT');
    $req->content_type('application/x-www-form-urlencoded');

    print "----------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

    # Do HTTP request.
    {
      local $SIG{__DIE__} = 'DEFAULT';
      $self->{RESPONSE} = $self->{UA}->request($req);
    }

    print "-------------------------\n".$self->{RESPONSE}->as_string."\n-------------------------\n"if ($DEBUG);

    # Evaluate the response.
    if ($self->{RESPONSE}->code == 200) {
      # There is no body if successful.
      $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
    } else {
      $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->content."\n";
    }
  }
}

sub deleteExternalAccount
{
  my ($self, $user, $account_id) = @_;

  my @putContent = ();
  
  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$user."/externalAccounts/mailAccounts/$account_id");
  my $req = POST( $self->{CALL_URI}, Content => [@putContent] );
  $req->method('DELETE');
  $req->content_type('application/x-www-form-urlencoded');

  print "----------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  print "-------------------------\n".$self->{RESPONSE}->as_string."\n-------------------------\n"if ($DEBUG);

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->content."\n";
  }
}

sub readExternalAccounts
{
  my ($self, $user) = @_;
  
  my @putContent = ();

  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$user."/externalAccounts/mailAccounts");
  my $req = POST( $self->{CALL_URI}, Content => [@putContent] );

  $req->method('GET');
  $req->content_type('application/x-www-form-urlencoded');

  print "----------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);

  # Do HTTP request.
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }

  print "-------------------------\n".$self->{RESPONSE}->as_string."\n-------------------------\n"if ($DEBUG);

  # Evaluate the response.
  if ($self->{RESPONSE}->code == 200) {
    # There is no body if successful.
    $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
    $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->content."\n";
  }
  
  return ($self->{RESPONSE}->content);
}

sub getMailFolders
{
  my $self = shift;
  my $email = shift;

  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri."mailbox/v2/".$email."/folders/list");

  my $req = POST( $self->{CALL_URI}, Content => [ ] );

  $req->method('GET');
  $req->content_type('application/x-www-form-urlencoded');

  print "-------------------------\n".$req->as_string."\n-------------------------\n"if ($DEBUG);
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }
  print "-------------------------\n".$self->{RESPONSE}->as_string."\n-------------------------\n"if ($DEBUG);

  if ($self->{RESPONSE}->code == 200) {
      # There is no body if successful.
      $self->{ERROR}   = ""; # Set this to "" so is_success has this calls status.
  } else {
      $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->content."\n";
  }
  return $self->{RESPONSE}->content;
}

sub checkAuth
{
  my $self = shift;
  my $email = shift;
  my $pwd = shift;

  my $base_uri = $self->{BASE_URI};
  $self->{CALL_URI} = URI->new($base_uri.'mailbox/v2/'.$email.'/authenticate');
  my $uri = URI->new($self->{CALL_URI});

  my $req = POST($uri, Content => [ 'password' => $pwd, ]);
  $req->content_type('application/x-www-form-urlencoded');
  # don't print request as it would contain the password
  {
    local $SIG{__DIE__} = 'DEFAULT';
    $self->{RESPONSE} = $self->{UA}->request($req);
  }
  if ($self->{RESPONSE}->code == 200) {
      $self->{ERROR} = ""; # Set this to "" so is_success has this calls status.
  } else {
      $self->{ERROR} = $self->{RESPONSE}->code." ".$self->{RESPONSE}->content."\n";
  }
  return $self->{RESPONSE}->content;
}

##############################################################################################################
# Exit perl module
##############################################################################################################
1;  # so the require or use succeeds

