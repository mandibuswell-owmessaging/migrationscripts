##############################################################################################################
#
#  Copyright 2016 Openwave Messaging.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:                 mgrConnection.pm
#  Description:             Package to speak to PAB/CAL management services
#  Author:                  Gary Palmer - Openwave Messaging
#  Date:                    May, 2016
#  Customer:                Telstra
#  Version:                 1.0.0 - 11 May 2016
#
#############################################################################################################
#
#  Version Control
#
#  Version: 1.0.0
#  Changes:
#
##############################################################################################################

package com::owm::mgrConnection;

use strict;
use warnings;

use IO::Socket;

sub new
{
    my $that = shift;
    my $class = ref($that) || $that;
    my $self = { };
    bless $self, $class;

    my %args = @_;
    unless (defined($args{'host'}))
    {
        $self->{'okError'} = 'ERROR Host not specified';
        return($self);
    }
    unless (defined($args{'port'}))
    {
        $self->{'okError'} = 'ERROR Port not specified';
        return($self);
    }
    unless (defined($args{'pass'}))
    {
        $self->{'okError'} = 'ERROR Password not specified';
        return($self);
    }

    $self->{'connected'} = 0;
    $self->{'socket'} = IO::Socket::INET->new(
        PeerAddr => $args{'host'},
        PeerPort => $args{'port'},
        Proto    => 'tcp',
        Timeout  => 30);
    $self->{'okError'} = undef;

    unless (defined($self->{'socket'}))
    {
        $self->{'okError'} = 'ERROR Cannot create TCP connection to "'.$args{'host'}.':'.$args{'port'}.'": '.$^E;
        return($self);
    }

    $self->{'connected'} = 1;

    my $banner = $self->_getline();
    unless(defined($banner))
    {
        $self->{'okError'} = 'ERROR Cannot read banner from "'.$args{'host'}.':'.$args{'port'}.'"';
        eval
        {
            local $SIG{__DIE__} = 'DEFAULT';
            undef($self->{'socket'});
        };
        $self->{'connected'} = 0;
        return($self);
    }

    unless ($banner =~ /<[A-Za-z0-9]+@[a-z0-9-.]+>/)
    {
        $self->{'okError'} = 'ERROR Unexpected banner from "'.$args{'host'}.':'.$args{'port'}.'": '.$banner;
        eval
        {
            local $SIG{__DIE__} = 'DEFAULT';
            undef($self->{'socket'});
        };
        $self->{'connected'} = 0;
        return($self);
    }

    my $answer = $self->command('login', $args{'pass'}, 'write');

    return($self)
}

sub command
{
    my $self = shift;
    my @cmd = @_;
    my $cmd = join(' ', @cmd);
    $cmd .= "\r\n";

    unless (defined($self->{'connected'}) && 
            ($self->{'connected'} = 1))
    {
        $self->okError('ERROR Not connected');
        return(undef);
    }

    if ($self->{'socket'}->print($cmd) < 1)
    {
        $self->okError('ERROR Cannot write to socket: '.$^E);
        return(undef);
    }

    return($self->get_response());
}

sub command_ok
{
    my $self = shift;
    my $response = $self->command(@_);

    if ($self->okError() !~ /^OK/)
    {
        die 'Error: "'.$response.'" from command "'.join(' ', @_).'"';
    }
    return($response)
}

sub _getline
{
    my $self = shift;

    # If we aren't connected, return undef
    return undef if ($self->{'connected'} != 1);

    # If our socket has disappeared, return undef
    my $fd = $self->{'socket'};
    return undef if (!defined $fd);
         
    # This should now work.
    my $ret = <$fd>;

    return $ret;
}

sub get_response
{
    my $self = shift;
    my @response = ();
    my $line;

    unless (defined($self->{'connected'}) && 
            ($self->{'connected'} = 1))
    {
        $self->okError('ERROR Not connected');
        return(undef);
    }

    $self->okError(undef);
    while(1)
    {
        $line = $self->_getline();
        unless(defined($line))
        {
            $self->okError('ERROR Connection was dropped');
            return(@response);
        }

        if ($line eq '')
        {
            $self->okError('ERROR EOF on connection');
            return(@response);
        }

        if ($line =~ /^\* /)
        {
            $line =~ s/^\* //g;
            $line =~ s/[\r\n]+$//;
            push(@response, $line);
        } 
        elsif ($line =~ /^OK/ || $line =~ /^ERROR/)
        {
            $line =~ s/[\r\n]+$//;
            $self->okError($line);
            return(@response);
        }
    }
}

sub okError
{
    my ($self, $val) = @_;

    if (defined($val))
    {
        $self->{'okError'} = $val;
    }

    return($self->{'okError'});
}

sub DESTROY
{
    my $self = shift;

    return if ($self->{'connected'} != 1);
    # trap any failures so it doesn't cause other
    # problems
    eval
    {
        local $SIG{__DIE__} = 'DEFAULT';
        $self->command('logout');
    };

    if (defined $self->{'socket'})
    {
        # trap any failures so it doesn't cause other
        # problems
        eval
        {
            local $SIG{__DIE__} = 'DEFAULT';
            $self->{'socket'}->close();
        };
    }
    return;
}

1;
