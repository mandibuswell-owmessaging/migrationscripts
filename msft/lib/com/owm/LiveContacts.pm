#!/usr/bin/perl
###########################################################################
#
#  Copyright 2015 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Huanwen Jiao <huanwen.jiao@owmessaging.com>
#  Date:        4Q/2015
#  Version:     1.0.0 - Dec 17, 2015
#
###########################################################################
package com::owm::LiveContacts;
use FindBin;                    # Locate this script
use lib "$FindBin::Bin/lib";    # Tell script where to find libraries
use com::owm::migBellCanada;
use strict;
use warnings;
use com::owm::PABClient;
use com::owm::mosApi;
use Config::Simple;
use Text::CSV;
use DBI;

sub uploadVCard($$$$$$$) {
    my $email = shift;
    my $password = shift;
    my $vcard = shift;
    my $userpabstorehost = shift;
    my $cfg = shift;
    my $log = shift;
    my $counters_ = shift;
    my %counters = %$counters_;

    my $puid=""; #test
    my $pab_url =  $cfg->param('pab_base_url');
    $pab_url =~ s/<host>/$userpabstorehost/;
    $pab_url = $pab_url."/".$email."/"."Main";
    my $resp = com::owm::PABClient::put($pab_url,$email,$password,$vcard,$log);

    if ($resp->code == 201) {
        $log->info("$email| put contact successful.");
        #return($com::owm::migBellCanada::STATUS_SUCCESS);
    }elsif ($resp->code == 412) {
        $log->info("$email|$puid: contact already exists " . $resp->code);
        $counters{'duplicate'}++;
    } elsif ($resp->code == 424) {
        $log->error("$email|$puid: put contact failed, check file path location on PAB/CAL " . $resp->code);
        $counters{'fileMissing'}++;
    }else {
        $log->info("$email| put contact failed " . $resp->code);
        $counters{'error'}++;
        #return($com::owm::migBellCanada::STATUS_GENERAL_ERROR);
    }
    #always return success, failure of individual contact import should not cause migration failure.
    return($com::owm::migBellCanada::STATUS_SUCCESS,%counters);
}


sub createUploadVCARD($$$$$) {
    my $email = shift;
    my $password = shift;
    my $userpabstorehost = shift;
    my $cfg = shift;
    my $log = shift;
    my %counters = {};

    $email = 'cmpetest09@bigpond.com'; #test
    
    #Create db connection
    my $mysql_user = $cfg->get_param("mysql_user");
    my $mysql_pass = $cfg->get_param("mysql_pass");
    my $mysql_db = $cfg->get_param("mysql_db_contacts");
    my $mysql_host = $cfg->get_param("mysql_host");

    my $table = "contacts";
    my $dbi_str = "DBI:mysql:$mysql_db;host=$mysql_host";
    my $dbh = DBI->connect($dbi_str,$mysql_user,$mysql_pass) or return 1;

    my $sql = "select * from $table where OwnerEmailAddress='$email'";
    my $sth = $dbh->prepare($sql);
    $sth->execute or return 1;
    my $numRows = $sth->rows;
    my $numFields = $sth->{'NUM_OF_FIELDS'};
    
    print "\n$numRows, $numFields\n ";
    
    while (my $ref = $sth->fetchrow_hashref()) {
#       print "Found a row: owner = $ref->{'OwnerEmailAddress'}, Title = $ref->{'Title'}, EmailAddress = $ref->{'EmailAddress'},\n";
        
        my $vcard = "";
        {
        no warnings 'uninitialized';
        $vcard = "BEGIN:VCARD\n";
        $vcard .= "VERSION:3.0\n";
#$vcard .= "UID:".$ref->{'Id'}."\n"; #
        
        #??$ref{'OwnerEmailAddress'};
        #####?! $vcard .= "TITLE:$ref->{'Title'}\n"; #???
        #$ref->{'First Name'}
        #$ref->{'Middle Name'}
        #$ref->{'Last Name'}
        #??$ref->{'Suffix'}
        #?$ref->{'Given Name Yomi'}
        #?$ref->{'Family Name Yomi'}
        $vcard .= "N:$ref->{'LastName'};$ref->{'FirstName'};$ref->{'MiddleName'};;$ref->{'Suffix'}\n";
        $vcard .= "FN:$ref->{'LastName'}\, $ref->{'FirstName'}\n";
        $vcard .= "NICKNAME:$ref->{'Nickname'}\n";
        
        #$ref->{'HomeStreet'}
        #$ref->{'HomeCity'}
        #$ref->{'HomeState'}
        #$ref->{'HomePostal Code'}
        #$ref->{'HomeCountry'
        $vcard .= "ADR;TYPE=HOME:;;$ref->{'HomeStreet'};$ref->{'HomeCity'};$ref->{'HomeState'};$ref->{'HomePostalCode'};$ref->{'HomeCountry'}\n";
        #$ref->{'Company'}
        #$ref->{'Department'}
        $vcard .= "ORG:$ref->{'Company'};$ref->{'Department'}\n"; 
        #$ref->{'Job Title'}
        $vcard .= 'TITLE:'.$ref->{'JobTitle'}."\n"; #title";
        #??$ref->{'Office Location'}
        #$ref{'Business Street'}
        #$ref->{'Business City'}
        #$ref->{'Business State'}
        #$ref->{'Business Postal Code'}
        #$ref->{'Business Country'}
        $vcard .= "ADR;TYPE=WORK:;;$ref->{'BusinessStreet'};$ref->{'BusinessCity'};$ref->{'BusinessState'};$ref->{'BusinessPostalCode'};$ref->{'BusinessCountry'}\n";
        #$ref->{'Other Street'}
        #$ref->{'Other City'}
        #$ref->{'Other State'}
        #$ref->{'Other Postal Code'}
        #$ref->{'Other Country'}
        $vcard .= "ADR;TYPE=Other:;;$ref->{'OtherStreet'};$ref->{'OtherCity'};$ref->{'OtherState'};$ref->{'OtherPostalCode'};$ref->{'OtherCountry'}\n";
        #$ref->{'Assistant\'s Phone'}
        $vcard .= "TEL;TYPE=work:$ref->{'AssistantsPhone'}\n";
        #$ref->{'Business Fax'}
        $vcard .= "TEL;TYPE=fax,work:$ref->{'BusinessFax'}\n";
        #$ref->{'Business Phone'}
        $vcard .= "TEL;TYPE=work:$ref->{'BusinessPhone'}\n";
        #$ref->{'Business Phone 2'} 
        $vcard .= "TEL;TYPE=work:$ref->{'BusinessPhone2'}\n";
        #$ref->{'Callback'}
        $vcard .= "TEL;TYPE=other:$ref->{'Callback'}\n";
        #$ref->{'Car Phone'}
        $vcard .= "TEL;TYPE=cell:$ref->{'CarPhone'}\n";
        #$ref->{'Company Main Phone'}
        $vcard .= "TEL;TYPE=work,pref:$ref->{'CompanyMainPhone'}\n";
        #$ref->{'Home Fax'}
        $vcard .= "TEL;TYPE=fax,home:$ref->{'HomeFax'}\n";
        #$ref->{'Home Phone'}
        $vcard .= "TEL;TYPE=home:$ref->{'HomePhone'}\n";
        #$ref->{'Home Phone 2'}
        $vcard .= "TEL;TYPE=home:$ref->{'HomePhone2'}\n";
        #?$ref->{'ISDN'}
        #$ref->{'Mobile Phone'}
        $vcard .= "TEL;TYPE=home;TYPE=cell;TYPE=voice:".$ref->{'MobilePhone'}."\n";
        #$ref->{'Other Fax'}
        $vcard .= "TEL;TYPE=fax,other:$ref->{'OtherFax'}\n";
        #$ref->{'Other Phone'}
        $vcard .= "TEL;TYPE=other:$ref->{'OtherPhone'}\n";
        #$ref->{'Pager'}
        $vcard .= "TEL;TYPE=pager;TYPE=home:$ref->{'Pager'}\n";
        #$ref->{'Primary Phone'}
        $vcard .= "TEL;TYPE=home:$ref->{'PrimaryPhone'}\n";
        #$ref->{'Radio Phone'}
        $vcard .= "TEL;TYPE=ohter:$ref->{'RadioPhone'}\n";
        #?$ref->{'TTY/TDD Phone'}
        #?$ref->{'Telex'}
        #?$ref->{'Anniversary'}
        #$ref->{'Birthday'}
        if ($ref->{'BirthdayYear'} && $ref->{'BirthdayMonth'} && $ref->{'BirthdayDay'} ) {
        $vcard .= "BDAY:$ref->{'BirthdayYear'}-$ref->{'BirthdayMonth'}-$ref->{'BirthdayDay'}\n";#2015-08-05";
        }
        #$ref->{'E-mail Address'}
        #$ref->{'E-mail Type'}
        if ( $ref->{'EmailType'} ){
            $vcard .= "EMAIL;TYPE=internet;TYPE=$ref->{'EmailType'}:$ref->{'EmailAddress'}\n";
        }
        #$ref->{'E-mail 2 Address'}
        #$ref->{'E-mail 2 Type'}
        if ( $ref->{'Email2Type'} ){
            $vcard .= "EMAIL;TYPE=internet;TYPE=$ref->{'Email2Type'}:$ref->{'Email2Address'}\n";
        }
        #$ref->{'E-mail 3 Address'}
        #$ref->{'E-mail 3 Type'}
        if ( $ref->{'Email3Type'} ){
            $vcard .= "EMAIL;TYPE=internet;TYPE=$ref->{'Email3Type'}:$ref->{'Email3Address'}\n";
        }
        #$ref->{'Notes'}
        my $note = "NOTE:$ref->{'Notes'}\\n\\n";
        #?$ref->{'Spouse'}
        #$ref->{'Web Page'}
        $vcard .= "URL;TYPE=work:$ref->{'WebPage'}\n"; #http://home.com

    $vcard .= "END:VCARD";
        }
        
        print "$vcard\n\n";
        $log->debug ("$email|:\n$vcard\n");
        #put contact
        uploadVCard($email,$password,$vcard,$userpabstorehost,$cfg,$log, \%counters);
    }

    #close db connection
    $sth->finish();
    $dbh->disconnect();
    

    
}


1;