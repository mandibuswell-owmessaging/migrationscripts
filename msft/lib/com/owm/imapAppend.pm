package com::owm::imapAppend;

##############################################################################################################
#
#  Copyright 2014 Openwave Messaging.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:                 imapAppend.pm
#  Description:             Package to insert email into a mailbox via IMAP APPEND.
#  Author:                  Andrew Perkins - Openwave Messaging
#  Date:                    10 Feb 2015
#  Version:                 1.0.0
#
#  Additional info...
#  none
#                           
#############################################################################################################
#
#  Version Control
# 
#  Version: 1.0.0
#  Changes:
#    - Initial version.
#    - Add sub imapAppendMessage()
#
##############################################################################################################

use strict;
use warnings;
use utf8;
use Mail::IMAPClient;
use POSIX qw(strftime);

####################################################################
# imapAppendMessage
#
# Routine to use IMAP APPEND to insert a last ('goodbye') message into 
# the inbox of the source platform.
####################################################################
sub imapAppendMessage($$$$$$)
{
  my $sourceImapServer = shift;
  my $imapUsername = shift;
  my $userPassword = shift;
  my $templateFile = shift; # File in RFC822 format. 
  my $userEmail = shift;
  my $mode = shift;
  my $msgString = "To: $imapUsername\n";
  my $ssl = 1;
  if (($mode eq 'MX8' ) || ($mode eq 'MYI' ) || ($mode eq 'WLW' )) {
    $ssl = 0;
  }

  my $imap = Mail::IMAPClient->new(
     Server   => $sourceImapServer,
     User     => $imapUsername,
     Password => $userPassword,
     Ssl      => $ssl,
     Uid      => 1,
     Timeout  => 60,
  ) or return ("Could not connect: $@");

  # Read the template file into a string.
  {
    local $/ = undef;
    open FILE, "<:utf8", $templateFile
      or return ("Could not open file: $!");
    #open FILE, $templateFile 
    #  or return ("Could not open file: $!");
    binmode FILE;
    $msgString .= <FILE>;
    close FILE;
  }

  # The templateFile should contain "USER'S_EMAIL_ADDRESS" strings
  # where the user's email address should appear. Make this change to the string.
  $msgString =~ s/USER'S_EMAIL_ADDRESS/$userEmail/g;

  # The templateFile should contain "CURRENT TIME" string
  # Set 'Date' header to current time.
  my $now = time();
  # We need to munge the timezone indicator to add a colon between the hour and minute part
  my $tz = strftime("%z", localtime($now));
  $tz =~ s/(\d{2})(\d{2})/$1:$2/;
  my $rfc822time = strftime("Date: %a, %d %b %Y %H:%M:%S %z", localtime($now));
  $msgString =~ s/Date: CURRENT TIME/$rfc822time/;

  # The subject string in the FR message should have an accent
  # but I have not found a work to do in template file.
  # So doing a regex replacement instead...
  $msgString =~ s/Subject: Modifiez vos parametres maintenant/Subject: Modifiez vos param\xE9tres maintenant/;

#  $imap->append_file('inbox', $templateFile )
  $imap->append_string('inbox', $msgString )
    or return("Could not append file: ".$imap->LastError);

  $imap->logout;

  return('');
}

##############################################################################################################
# End of package
##############################################################################################################
1;  # so the require or use succeeds
