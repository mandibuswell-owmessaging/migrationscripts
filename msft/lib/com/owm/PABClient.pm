#!/usr/bin/perl
###########################################################################
#
#  Copyright 2014 Openwave Systems Inc.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:     ???
#  Description: ???
#  Input:       ???
#  Author:      Huanwen Jiao <huanwen.jiao@owmessaging.com>
#  Date:        3Q/2015
#  Version:     1.0.0 - Jul 17, 2015
#
###########################################################################
package com::owm::PABClient;

use strict;
use LWP;
use XML::LibXML;

sub new {
	my $class  = shift;
	my %parm = @_;
	my $this = {};
	bless $this, $class;
}




sub doRequest($$$$$$$){

	my ($url, $user, $password, $method, $contentType, $body, $log) = @_;

	my $ua = LWP::UserAgent->new;

	my $req = HTTP::Request->new($method, $url);
	$req->authorization_basic($user, $password);
	$req->header("Content-Type" => "$contentType" );
	use Encode;
	$body = encode('UTF-8', $body);
	$req->content($body);

	# $log->debug ("$user: request:\n----------" . $req->as_string . "\n---------\n") ; 
    my $resp;
    {
        local $SIG{__DIE__} = 'DEFAULT';
        $resp = $ua->request($req);
    }
	# $log->debug ("response:\n----------" . $resp->as_string . "\n---------\n") ;

	return $resp;
}




sub mkcol ($$$$$) {
#sub mkcalendar {

	my ($url, $pabDisplayName, $user, $password,$log) = @_;
	my $method = "MKCOL";

	my $contentType = "text/xml; charset=utf-8";
	my $body = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>
   <D:mkcol xmlns:D=\"DAV:\" xmlns:C=\"urn:ietf:params:xml:ns:carddav\">
     <D:set>
       <D:prop>
         <D:resourcetype>
           <D:collection/>
           <C:addressbook/>
         </D:resourcetype>
         <D:displayname>$pabDisplayName</D:displayname>
         <C:addressbook-description xml:lang=\"en\">My address book.</C:addressbook-description>
       </D:prop>
     </D:set>
   </D:mkcol>";
	
	
	my $resp = doRequest($url, $user, $password, $method, $contentType, $body, $log );
	
	if ($resp->code == 201) {
		return $url;
	}else{
		return undef;
	}


}

sub report{

	my ($url, $user, $password, $log) = @_;
	my $method = "";
	my $contentType = "text/xml; charset=utf-8";
	my $body = "";
	
	doRequest($url, $user, $password, $method, $contentType, $body, $log);

}

sub put ($$$$$){

#Content-Type: text/calendar
	my ($url, $user, $password, $body,$log) = @_;
	my $method = "PUT";
	my $contentType = "text/vcard; charset=utf-8";

	my $resp = doRequest($url, $user, $password, $method, $contentType, $body, $log);
	

	return $resp;

}

sub put_with_file ($$$$$){

#Content-Type: text/calendar
	my ($url, $user, $password, $icalfile,$log) = @_;
	my $method = "PUT";
	my $contentType = "text/vcard";
	
	my $body="";
	open(FILE,$icalfile)||die"can not open the file: $!";
	my $eachline = "";
	while (defined ($eachline =<FILE>)) {
		$body = $body.$eachline
	}
	
	my $resp = doRequest($url, $user, $password, $method, $contentType, $body,$log);

	return $resp;

}


1;
