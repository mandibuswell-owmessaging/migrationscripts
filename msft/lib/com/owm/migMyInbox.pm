package com::owm::migMyInbox;
##############################################################################################################
#
#  Copyright 2015-2016 Openwave Messaging.  All rights reserved.
#
#  This software is provided without warranty or support, and
#  is only intended to be used in accordance with Openwave
#  support and operations guidelines.
#
#  Program:                 migMyInbox.pm
#  Description:             Perl module to migrate Telstra Myinbox data into MX
#  Author:                  Gary Palmer - Openwave Messaging
#  Date:                    Nov, 2015
#  Customer:                Telstra
#  Version:                 1.0.0 - Nov 2015
#
#############################################################################################################
#
#  Version Control
#
#  Version: 1.0.0
#  Changes:
#
##############################################################################################################

use strict;
use warnings;

# Standard/CPAN perl modules
use Data::ICal;
use File::Temp qw/ :POSIX /;
use Fcntl qw(:DEFAULT :flock);
use IO::Select;
use IPC::Open3;
use Mail::IMAPClient;
use MIME::Base64 qw( encode_base64 );
use POSIX qw(:sys_wait_h errno_h);
use SOAP::Lite +trace => [ transport => \&soap_log_message ];
use Symbol;
use URI;
use XML::Simple;

# OWM perl modules
use com::owm::CALClient;
use com::owm::migTelstraEvents;
use com::owm::OlsonToVTimezone;
use com::owm::migBellCanada;

my $soap_call_name = '';
my $soap_debug_print = 0;

sub soap_log_message
{
    my ($in) = @_;
    my $name = $soap_call_name;
    if (length($name) < 1)
    {
        $name = 'SOAP';
    }

    if (ref($in) eq 'HTTP::Request')
    {
        print $name.' XML request: '.$in->content."\n\n";
    }
    elsif (ref($in) eq 'HTTP::Response')
    {
        print $name.' XML response: '.$in->content."\n\n";
    }
}

##############################################################################
# 
# Create and lock a lockfile to prevent running the main body of the
# migration more than once
# 

sub lockMigration($$$)
{
    my $lockfile = shift;
    my $log = shift;
    my $logprefix = shift;

    my $migrationLockFile;
    if (!open($migrationLockFile, ">>", $lockfile))
    {
        $log->error($logprefix.'Cannot open WSDL lock file "'.$lockfile.'": '.$!);
        return(undef);
    }
 
    eval
    {
        local $SIG{ALRM} = sub {die "ALRM timeout\n"; };
        alarm 30;
        while (!flock($migrationLockFile, LOCK_EX))
        {
            next if ($! == EINTR);
            $log->error($logprefix.'Error locking "'.$lockfile.'": '.$!);
            return(undef);
        }
        alarm 0;
    };

    if ($@ && $@ eq "ALRM timeout\n")
    {
        $log->error($logprefix.'Lock timeout on "'.$lockfile.'": '.$!);
        return(undef);
    }
    return($migrationLockFile);
}

##############################################################################
#
# Release the lockfile
#

sub unlockMigration($$$)
{
    my $migrationLockFile = shift;
    my $log = shift;
    my $logprefix = shift;

    if (!flock($migrationLockFile, LOCK_UN))
    {
        $log->error($logprefix.'Error unlocking WSDL lock file: '.$!);
        return(0);
    }
    return(1);
}

#######################################################################
#
# Create new object
#
#######################################################################
sub new($$$$)
{
    my $that = shift;
    my $class = ref($that) || $that;
    my ($email, $cfg, $log, $DEBUG) = @_;
    my $self = {};

    # do NOT add $log to $self as it is a dynamic object and should
    # not be stored. $cfg is read-only so it is safe to cache
    $self->{'DEBUG'} = $DEBUG;
    $soap_debug_print = $DEBUG;
    $self->{'config'} = $cfg;
    $self->{'userpref'}{'userpref_loaded'} = 0;
    # Default time zone in case one isn't read from MyInbox
    $self->{'userpref'}{'timeZone'} = 'Australia/Sydney';
    $self->{'logprefix'} = $email.'|MYI:';
    my $logprefix = $self->{'logprefix'};

    # Cache of SOAP WSDL config keys and descriptions
    my %wsdlMap = (
        'adminWsdl' => {
            cfg_param => 'myinbox_admin_wsdl_path',
            name => 'AdministrationService WSDL',
            path => undef,
            cachefile => 'tmp/AdministrationService.wsdl',
            lockfile => 'tmp/.AdministrationService.wsdl.lock',
            },
        'configWsdl' => {
            cfg_param => 'myinbox_config_wsdl_path',
            name => 'ConfigurationService WSDL',
            path => undef,
            cachefile => 'tmp/ConfigurationService.wsdl',
            lockfile => 'tmp/.ConfigurationService.wsdl.lock',
            },
        'contactWsdl' => {
            cfg_param => 'myinbox_contact_wsdl_path',
            name => 'ContactManagementService WSDL',
            path => undef,
            },
        'eventWsdl' => {
            cfg_param => 'myinbox_event_wsdl_path',
            name => 'EventManagementService WSDL',
            path => undef,
            },
        'globalWsdl' => {
            cfg_param => 'myinbox_global_wsdl_path',
            name => 'GlobalManagementService WSDL',
            path => undef,
            },
        'inboxWsdl' => {
            cfg_param => 'myinbox_inbox_wsdl_path',
            name => 'InboxService WSDL',
            path => undef,
            cachefile => 'tmp/InboxService.wsdl',
            lockfile => 'tmp/.InboxService.wsdl.lock',
            },
        'userWsdl' => {
            cfg_param => 'myinbox_user_wsdl_path',
            name => 'UserManagementService WSDL',
            path => undef,
            cachefile => 'tmp/UserManagementService.wsdl',
            lockfile => 'tmp/.UserManagementService.wsdl.lock',
            },
        );

    my $cacheDays = 14;
    if (defined($cfg->param('myinbox_wsdl_cache_days')))
    {
        my $param = $cfg->param('myinbox_wsdl_cache_days');
        if ($param =~ /^[0-9]+$/)
        {
            if ((int($param) > 0) && (int($param) < 99))
            {
                $cacheDays = int($param);
            }
        }
    }

    $self->{'cacheDays'} = $cacheDays;

    # Populate the %wsdlMap hash with the config key values and check that
    # the values are valid
    OUTERLOOP: foreach my $wsdl (sort(keys(%wsdlMap)))
    {
        $wsdlMap{$wsdl}{'path'} = $cfg->param($wsdlMap{$wsdl}{'cfg_param'});
        if (defined($wsdlMap{$wsdl}{'path'}) && ($wsdlMap{$wsdl}{'path'} ne ''))
        {
            if (($wsdlMap{$wsdl}{'path'} !~ /^http:\/\//) &&
                (($wsdlMap{$wsdl}{'path'} !~ /^https:\/\//)))
            {
                unless(-f $wsdlMap{$wsdl}{'path'})
                {
                    $log->error($logprefix.$wsdlMap{$wsdl}{'name'}.
                        ' at path "'.$wsdlMap{$wsdl}{'path'}.'" is not a file.  '.
                        'Some data will not be able to be migrated.');
                    undef($wsdlMap{$wsdl}{'path'});
                    next OUTERLOOP;
                }
                else
                {
                    unless(-r $wsdlMap{$wsdl}{'path'})
                    {
                        $log->error($logprefix.$wsdlMap{$wsdl}{'name'}.
                            ' at path "'.$wsdlMap{$wsdl}{'path'}.'" is not '.
                            'readable.  Some data will not be able to be '.
                            'migrated.');
                        undef($wsdlMap{$wsdl}{'path'});
                        next OUTERLOOP;
                    }
                }
            }
        }
        else
        {
            $log->warn($logprefix.$wsdlMap{$wsdl}{'name'}.' file '.
                'location is not specified in configuration file using the '.
                'key "'.$wsdlMap{$wsdl}{'cfg_param'}.'".  Some data will '.
                'not be able to be migrated.');
            next OUTERLOOP;
        }

        # adminWsdl, configWsdl and userWsdl are used in this module so create
        # SOAP::Lite objects for them so we don't have to instantiate
        # a new one each time we need to make a call
        if (($wsdl eq 'adminWsdl') || ($wsdl eq 'configWsdl') ||
            ($wsdl eq 'inboxWsdl') || ($wsdl eq 'userWsdl'))
        {
            my $fileOK = 0;
            my $retryCount = 0;
            my $lock;
            do
            {
                $retryCount ++;
                $lock = lockMigration($wsdlMap{$wsdl}{'lockfile'}, $log, $logprefix); 
            } while((!defined($lock)) && $retryCount <= 3);

            if (-f $wsdlMap{$wsdl}{'cachefile'} && -r $wsdlMap{$wsdl}{'cachefile'})
            {
                my $modtime = (stat($wsdlMap{$wsdl}{'cachefile'}))[9];
                if (time - $modtime < $cacheDays * 24 * 3600)
                {
                    $log->debug($logprefix.'Cache file "'.$wsdlMap{$wsdl}{'cachefile'}.
                                '"less than '.$cacheDays.' days old, reusing');
                    $fileOK = 1;
                }
                else
                {
                    $log->debug($logprefix.'Cache file "'.$wsdlMap{$wsdl}{'cachefile'}.
                                '"more than '.$cacheDays.' days old, deleting');
                    unlink($wsdlMap{$wsdl}{'cachefile'});
                }
            }

            unless($fileOK)
            {
                $retryCount = 0;
                my $gotWSDL = 0;
                do
                {
                    sleep($retryCount);
                    $retryCount++;
                    my $ua = LWP::UserAgent->new();
                    my $resp = $ua->get($wsdlMap{$wsdl}{'path'});
                    if ($resp->is_success)
                    {
                        my $bytes = $resp->content;
                        if (length($bytes) > 0)
                        {
                            if (open(OUT, '>', $wsdlMap{$wsdl}{'cachefile'}))
                            {
                                print OUT $bytes;
                                close(OUT);
                                $gotWSDL = 1;
                                $log->debug($logprefix.'Cache file "'.$wsdlMap{$wsdl}{'cachefile'}.
                                            '" downloaded from "'.$wsdlMap{$wsdl}{'path'}.'"');
                            }
                        }
                    }
                    else
                    {
                        $log->warn($logprefix.'Error downloading WSDL from "'.
                            $wsdlMap{$wsdl}{'path'}.'": '.$resp->code.' '.
                            $resp->message);
                    }
                } while((!$gotWSDL) && ($retryCount <= 5));
            }
            unlockMigration($lock, $log, $logprefix);

            # silence a warning from SOAP::Lite about redefining
            # subroutines if the same process calls this routine
            # for different users
            local $SIG{'__WARN__'} = sub {
                if ($_[0] =~ /Subroutine.*redefined/)
                {
                    return;
                }
                print $_[0];
                return;
            };
            my $endpoint = $wsdlMap{$wsdl}{'path'};
            # Tell SOAP::Lite what endpoint is - in Telstra, just strip
            # off the '?wsdl' at the end
            $endpoint =~ s/\?wsdl//;

            my $success = 0;
            $retryCount = 0;

            do
            {
                sleep($retryCount);
                $retryCount++;
                eval {
                    local $SIG{__DIE__} = 'DEFAULT';
                    my $soap = SOAP::Lite->endpoint($endpoint);
                    if (defined($soap)) {
                        # Now tell SOAP::Lite what the service description (WSDL) is
                        my $compiledWsdl = $soap->service('file:'.$wsdlMap{$wsdl}{'cachefile'});
                        if (defined($compiledWsdl)) {
                            $self->{$wsdl}{'compiled'} = $compiledWsdl;
                        }
                    }
                };
                unless (defined($self->{$wsdl}{'compiled'})) {
                    $log->warn($logprefix.$wsdlMap{$wsdl}{'name'}.' could not '.
                        'be loaded: '.$@);
                }
                else
                {
                    $success = 1;
                }
            } while (($success == 0) && ($retryCount <= 3))
        }
       
        $self->{$wsdl}{'path'} = $wsdlMap{$wsdl}{'path'};
    }

    bless($self, $class);
    return($self);
}

#############################################################################
#
# First step in retrieving the profile data for a user - convert a username
# into a user ID, which is needed by most of the other calls to the MyInbox
# SOAP API
#
#############################################################################

sub getUserByUsername($$$$)
{
    my ($self, $email, $cfg, $log) = @_;
    my $logprefix = $self->{'logprefix'};

    # check for required SOAP connection configuration
    unless (defined($self->{'adminWsdl'}{'path'}) &&
            defined($self->{'adminWsdl'}{'compiled'}))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'getUserByUsername() without AdministrationService WSDL '.
                'loaded.  Cannot continue');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    my ($answer, $faultData, $failed);
    my $retryCount = 0;
    $soap_call_name = $email.': getUserByUsername';
    do
    {
        $failed = 0;
        $retryCount ++;
        sleep($retryCount);
        my $soap = $self->{'adminWsdl'}{'compiled'};
        # need to redirect STDERR because the XML::Compile modules can
        # write to STDERR if the server responds with a HTTP error code.
        # I'd prefer to capture it, but discarding it is significantly easier
        # than trying to write it to the logfile

        # This is in a block without an control statement (if, while, until,
        # etc) so that we can use 'local' to make sure that the file
        # descriptors are automatically reverted at the end of the block
        if ($self->{'DEBUG'})
        {
            unless(open(local *STDERR, '>&STDOUT'))
            {
                if (defined($log))
                {
                    $log->error($logprefix.'Cannot redirect STDERR to '.
                        'STDOUT: '.$!);
                }
                else
                {
                    die "cannot redirect STDERR to STDOUT: $!";
                }
            }
        }
        else
        {
            unless(open(local *STDERR, '>', '/dev/null'))
            {
                if (defined($log))
                {
                    $log->error($logprefix.'Cannot redirect STDERR to '.
                        '/dev/null: '.$!);
                }
                else
                {
                    die "cannot redirect STDERR to /dev/null: $!";
                }
            }
        }

        {
            local $SIG{__DIE__} = 'DEFAULT';
            # Query SOAP service
            $answer =
                $soap->on_fault(sub
                {
                    my $soap = shift;
                    my $fault = shift;
                    $faultData = $fault;
                    $failed = 1;
                    return(undef); # else the last variable is returned
                })->getUserByUsername($email);
        };
    } while ($failed && ($retryCount < 3));

    # the server responded with an error, likely a HTTP error (e.g. 500)
    if ((!defined($answer)) && (defined($faultData)))
    {
        my $error;
        if (defined($log))
        {
            if (ref(\$faultData) eq 'SCALAR')
            {
                eval
                {
                    local $SIG{__DIE__} = 'DEFAULT';
                    $log->error($logprefix.'Error from SOAP interface while '.
                        'fetching numeric ID: '.$faultData);
                    $error = $faultData;
                };
            }
            else
            {
                eval
                {
                    local $SIG{__DIE__} = 'DEFAULT';
                    $log->error($logprefix.'Error message from SOAP server while '.
                        'fetching numeric ID: '.$faultData->faultstring);
                    $error = $faultData->faultstring;
                };
            }
        }

        # TELSTRA-843 - retry user @telstra.com if they are a @bigpond.com
        # user to work around the lack of aliasing in MyInbox
        if ($error =~ /com.telstra.inbox.exception.UserNotFoundException/)
        {
            if ($email =~ /\@bigpond.com$/)
            {
                (my $telstra = $email) =~ s/\@bigpond.com$/\@telstra.com/;
                return($self->getUserByUsername($telstra, $cfg, $log));
            }
        }

        $soap_call_name = $email;
        $soap_call_name =~ s/\@telstra.com$/\@bigpond.com/;
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    if ($failed)
    {
        $log->error($logprefix.'Failed getUserByUsername call');
        $soap_call_name = $email;
        $soap_call_name =~ s/\@telstra.com$/\@bigpond.com/;
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    # We only need the numeric ID value, stored in the 'id' field in the
    # answer
    if (defined($answer->{'id'}))
    {
        my $userid = $answer->{'id'};
        if (defined($log))
        {
            $log->info($logprefix.'Numeric user ID for MyInbox user is '.$userid);
        }
        $self->{'user'}{'userid'} = $userid;
        $self->{'user'}{'myinbox_email'} = $email;
        $soap_call_name = $email;
        $soap_call_name =~ s/\@telstra.com$/\@bigpond.com/;
        return($com::owm::migTelstraEvents::STATUS_SUCCESS);
    }

    if (defined($log))
    {
        $log->error($logprefix.'Numerical user ID not found in SOAP '.
            'response from getUserByUsername call');
    }

    $soap_call_name = $email;
    $soap_call_name =~ s/\@telstra.com$/\@bigpond.com/;
    return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
}

#############################################################################
#
# Migrate the profile information (timezone, Empty trash on logout, Include
#   message in reply, Save sent messages, Use rich text editor)
#
#############################################################################

sub migrateUserConfiguration($$$$$)
{
    my ($self, $email, $cfg, $log, $mos) = @_;
    my $logprefix = $self->{'logprefix'};

    # check for required SOAP connection configuration
    unless (defined($self->{'configWsdl'}{'path'}) &&
            defined($self->{'configWsdl'}{'compiled'}))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateUserConfiguration() without ConfigurationService WSDL '.
                'loaded.  Cannot continue');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    unless (defined($self->{'user'}) && defined($self->{'user'}{'userid'}))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateUserConfiguration() without valid numeric user ID. '.
                'Cannot continue');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    my ($answer, $faultData, $failed);
    my $soap = $self->{'configWsdl'}{'compiled'};
    my $retryCount = 0;
    $soap_call_name = $email.': getUserConfiguration';
    $log->info('About to query SOAP getUserConfiguration call');
    do
    {
        $failed = 0;
        $retryCount ++;
        sleep($retryCount);
        local $SIG{__DIE__} = 'DEFAULT';
        # Query SOAP service
        $answer =
            $self->{'configWsdl'}{'compiled'}->on_fault(sub
            {
                my $soap = shift;
                my $fault = shift;
                $faultData = $fault;
                $failed = 1;
                return(undef); # else the last variable is returned
            })->getUserConfiguration($self->{'user'}{'userid'});
    } while ($failed && ($retryCount < 3));

    # the server responded with an error, likely a HTTP error (e.g. 500)
    if ((!defined($answer)) && (defined($faultData)))
    {
        if (defined($log))
        {
            if (ref(\$faultData) eq 'SCALAR')
            {
                eval
                {
                    local $SIG{__DIE__} = 'DEFAULT';
                    $log->error($logprefix.'Error from SOAP interface while '.
                        'fetching user configuration: '.$faultData);
                };
            }
            else
            {
                eval
                {
                    local $SIG{__DIE__} = 'DEFAULT';
                    $log->error($logprefix.'Error message from SOAP server while '.
                        'fetching user configuration: '.$faultData->faultstring);
                };
            }
        }

        $soap_call_name = $email;
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    if ($failed)
    {
        $log->error($logprefix.'Failed getUserConfiguration call');
        $soap_call_name = $email;
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    my $timeZone = $self->{'userpref'}{'timeZone'};

    # We expect a timezone value
    unless (defined($answer->{'timeZone'}) &&
            (length($answer->{'timeZone'}) > 0))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Users configured timezone not found in '.
                'SOAP response from getUserConfiguration call');
        }
    }
    else
    {
        $timeZone = $answer->{'timeZone'};
    }

    # Check that the returned timezone is in the Olson database
    my $vTimezone =
        com::owm::OlsonToVTimezone::getVtimezone($timeZone, $cfg, $log);
    unless(defined($vTimezone))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Timezone returned by SOAP '.
                'getUserConfiguration call is not recognized by Olson TZ '.
                'database: '.$answer->{'timeZone'});
        }

        $soap_call_name = $email;
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }
    else
    {
        $self->{'userpref'}{'timeZone'} = $timeZone;
    }

    # Record that we have loaded the preference values into the object
    # for later use
    $self->{'userpref'}{'userpref_loaded'} = 1;
    # save settings so we don't have to keep calling getUserConfiguration
    # each time we want to migrate a different setting (e.g. fowarding)
    foreach my $setting (keys(%{$answer}))
    {
        if (defined($answer->{$setting}) && 
            (length($answer->{$setting}) > 0))
        {
            $self->{'userpref'}{$setting} = $answer->{$setting};
        }
    }

    # Resolve any TZ aliases.  TELSTRA-793
    my $oldTZ = $self->{'userpref'}{'timeZone'};
    my $newTZ = com::owm::OlsonToVTimezone::resolveOlsonAlias($oldTZ);
    if ($oldTZ ne $newTZ)
    {
        if (defined($log))
        {
            $log->info($logprefix.'Timezone returned from SOAP '.
                'getUserConfiguration call ("'.$oldTZ.'") points to "'.
                $newTZ.'" which will be used on MX9');
        }

        $self->{'userpref'}{'timeZone'} = $newTZ;
    }
    undef($oldTZ);
    undef($newTZ);

    # Save the timezone to MX9 via mOS
    $mos->user($email);
    my $retval = $mos->GeneralPreference('timezone='.
                                         $self->{'userpref'}{'timeZone'});
    unless ($mos->is_success)
    {
        chomp($retval);
        $log->error($logprefix.'Failed to set timezone: tz = "'
            .$self->{'userpref'}{'timeZone'}.'": '.$retval);
        if ($self->{'DEBUG'})
        {
            $mos->PrintResponse();
        }
        if (defined($cfg->param("default_tz")) &&
            ($self->{'userpref'}{'timeZone'} ne $cfg->param("default_tz")))
        {
            my $defaultTz = $cfg->param("default_tz");
            $log->error($logprefix.'Defaulting to defaultTimezone = '.
                $defaultTz);
            $mos->user($email);
            $retval = $mos->GeneralPreference('timezone='.$defaultTz);
            unless ($mos->is_success)
            {
                chomp($retval);
                $log->error($logprefix.'Failed to set default timezone: '.
                     'tz = "'.$defaultTz.'": '.$retval);
                if ($self->{'DEBUG'})
                {
                    $mos->PrintResponse();
                }
                $soap_call_name = $email;
                return($com::owm::migTelstraEvents::STATUS_FAIL_MOS_SETTING);
            }
        }
    }
    else
    {
        $log->info($logprefix.'Set timezone to '.$self->{'userpref'}{'timeZone'});
    }

    if (defined($answer->{'emptyTrashOnLogout'}))
    {
        my $trash = $answer->{'emptyTrashOnLogout'};
        my $value = 'no';
        if ($trash =~ /true/i)
        {
            $value = 'yes';
        }

        # Save to MX9 via mOS
        $mos->user($email);
        $retval = $mos->GeneralPreference('emptyTrashOnLogout='.$value);
        unless ($mos->is_success)
        {
            chomp($retval);
            $log->error($logprefix.'Failed to set empty trash on logout '.
                'to "'.$value.'": '.$retval);
            if ($self->{'DEBUG'})
            {
                $mos->PrintResponse();
            }
            $soap_call_name = $email;
            return($com::owm::migTelstraEvents::STATUS_FAIL_MOS_SETTING);
        }
        else
        {
            $log->info($logprefix.'Set empty trash on logout to "'.$value.'"');
        }
    }

    if (defined($answer->{'includeMessageInReply'}))
    {
        my $include = $answer->{'includeMessageInReply'};
        # MyInbox only has true/false values
        my $value = 'includeOriginalMailInReply=no';
        if (lc($include) eq 'true')
        {
            # Duplicate the settings that Ux Suite uses I believe
            $value = 'includeOriginalMailInReply=yes&'.
                'originalMailSeperatorCharacter=">"';
        }

        # Save to MX9 via mOS
        $mos->user($email);
        $retval = $mos->MailSendPreference($value);
        unless ($mos->is_success)
        {
            chomp($retval);
            $log->error($logprefix.'Failed to set include message in reply '.
                'to "'.$value.'": '.$retval);
            if ($self->{'DEBUG'})
            {
                $mos->PrintResponse();
            }
            $soap_call_name = $email;
            return($com::owm::migTelstraEvents::STATUS_FAIL_MOS_SETTING);
        }
        else
        {
            $log->info($logprefix.'Set include message in reply to "'
                .$value.'"');
        }
    }

    if (defined($answer->{'saveSentMessages'}))
    {
        my $save = $answer->{'saveSentMessages'};
        my $value = 'autoSaveSentMessages=no';
        if (lc($save) eq 'true')
        {
            $value = 'autoSaveSentMessages=yes';
        }

        # Save to MX9 via mOS
        $mos->user($email);
        $retval = $mos->MailSendPreference($value);
        unless ($mos->is_success)
        {
            chomp($retval);
            $log->error($logprefix.'Failed to set save sent messages '.
                'to "'.$value.'": '.$retval);
            if ($self->{'DEBUG'})
            {
                $mos->PrintResponse();
            }
            $soap_call_name = $email;
            return($com::owm::migTelstraEvents::STATUS_FAIL_MOS_SETTING);
        }
        else
        {
            $log->info($logprefix.'Set save sent messages to "'.$value.'"');
        }
    }

    if (defined($answer->{'useRichTextEditor'}))
    {
        my $rich = $answer->{'useRichTextEditor'};
        my $value = 'useRichTextEditor=text';
        if (lc($rich) eq 'true')
        {
            $value = 'useRichTextEditor=html';
        }

        # Save to MX9 via mOS
        $mos->user($email);
        $retval = $mos->MailSendPreference($value);
        unless ($mos->is_success)
        {
            chomp($retval);
            $log->error($logprefix.'Failed to set use rich text editor '.
                'to "'.$value.'": '.$retval);
            if ($self->{'DEBUG'})
            {
                $mos->PrintResponse();
            }
            $soap_call_name = $email;
            return($com::owm::migTelstraEvents::STATUS_FAIL_MOS_SETTING);
        }
        else
        {
            $log->info($logprefix.'Set use rich text editor to "'.
                $value.'"');
        }
    }

    $soap_call_name = $email;
    return($com::owm::migTelstraEvents::STATUS_SUCCESS);
}

#############################################################################
#
# Read the mail forwarding settings from the cached user preferences and
# push them to mOS
#
#############################################################################
sub migrateMailForward($$$$$)
{
    my ($self, $email, $cfg, $log, $mos) = @_;
    my $logprefix = $self->{'logprefix'};
    my $overallSuccess = 1;
    my $retval;

    # Check that the values were saved by migrateUserConfiguration earlier
    unless ($self->{'userpref'}{'userpref_loaded'})
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateMailForward() without previous successful call to '.
                'migrateUserConfiguration().  Cannot continue');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    my @fwd;
    my %curFwd;

    my ($mosContent, $error) = $mos->GetForwardingAddresses();
    if ($error || (!$mos->is_success()))
    {
        $log->error($logprefix.'Unable to retrieve current forwarding info'.
            $mos->error());
        if ($self->{'DEBUG'})
        {
            $mos->PrintResponse();
        }
        return($com::owm::migTelstraEvents::STATUS_FAIL_FORWARDING);
    }

    if (ref($mosContent) eq 'ARRAY') {
        foreach my $addr (@{$mosContent}) {
            $curFwd{lc($addr)} = 1;
            # $log->debug($logprefix.'Found "'.lc($addr).'" on current '.
            #     'forwarding list');
        }
    }
    else
    {
        $log->error($logprefix.'Did not get list of email addresses back '.
            'after asking mOS for current list of forwarding addresses.  '.
            'Data is of type: '.ref($mosContent));
        return($com::owm::migTelstraEvents::STATUS_FAIL_FORWARDING);
    }

    # MyInbox has two fields that each hold a forwarding address.  Extract
    # them and save them to the @fwd array
    my $sourceCount = 0;
    my $aliasEmail = $email;
    $aliasEmail =~ s/\@bigpond.com$/\@telstra.com/i;
    foreach my $key ('first', 'second')
    {
        my $keyname = $key.'ForwardingAddress';
        if (defined($self->{'userpref'}{$keyname}) &&
            (length($self->{'userpref'}{$keyname}) > 0))
        {
            $sourceCount++;
            my $fwdAddr = $self->{'userpref'}{$keyname};
            my $lcFwdAddr = lc($fwdAddr);
            if (defined($curFwd{$lcFwdAddr}))
            {
                delete($curFwd{$lcFwdAddr});
                $log->debug($logprefix.'Forwarding address "'.$lcFwdAddr.'" '.
                    'from MyInbox SOAP API already set on MX9');
            }
            elsif ($lcFwdAddr eq lc($email))
            {
                $log->info($logprefix.'Forwarding address "'.$lcFwdAddr.'" '.
                    'from MyInbox SOAP API matches users email address ('.
                    $email.') - will not be added to MX9');
            }
            elsif ($lcFwdAddr eq lc($aliasEmail))
            {
                $log->info($logprefix.'Forwarding address "'.$lcFwdAddr.'" '.
                    'from MyInbox SOAP API matches users email address ('.
                    $aliasEmail.') - will not be added to MX9');
            }
            else
            {
                push(@fwd, $fwdAddr);
                $log->debug($logprefix.'Forwarding address "'.$fwdAddr.'" '.
                    'from MyInbox SOAP API to be added to MX9')
            }
        }
        else
        {
            $log->debug($logprefix.'Attribute "'.$keyname.'" not set in '.
                'MyInbox SOAP API result')
        }
    }

    foreach my $address (sort(keys(%curFwd)))
    {
        my $content = $mos->DeleteForwardingAddress($address);
        unless($mos->is_success())
        {
            $log->error($logprefix.'Unable to delete "'.$address.'" from '.
                'existing MX9 forwarding address list: '.$mos->error());
            if ($self->{'DEBUG'})
            {
                $mos->PrintResponse();
            }
            return($com::owm::migTelstraEvents::STATUS_FAIL_FORWARDING);
        }
        else
        {
            $log->info($logprefix.'Deleted "'.$address.'" from existing MX9 '.
                'forwarding address list');
        }
    }

    # If there are no forwards, then make sure that forwarding is turned off
    # on MX9.  Necessary for preload where a user could have a forward set
    # on a previous run and then delete it.
    if ($sourceCount == 0)
    {
        $log->info($logprefix.'No forwarding rules set in MyInbox, MX9 '.
            'forwarding will be disabled');
        $mos->user($email);
        $retval = $mos->SetForwardingSettings('forwardingEnabled=no&copyOnForward=yes');
        if (!$mos->is_success)
        {
            chomp($retval);
            $log->error($logprefix.'Disabling forwarding failed: '.$retval);
            if ($self->{'DEBUG'})
            {
                $mos->PrintResponse();
            }
            return($com::owm::migTelstraEvents::STATUS_FAIL_FORWARDING);
        }
        else
        {
            $log->info($logprefix.'Disabling forwarding complete');
            return($com::owm::migTelstraEvents::STATUS_SUCCESS);
        }
    }

    if (($sourceCount > 0) && (scalar(@fwd) == 0))
    {
        $log->info($logprefix.'Forwarding already correctly set');
        return($com::owm::migTelstraEvents::STATUS_SUCCESS);
    }

    # Create the mOS parameters needed for multiple addresses
    my $fwd = 'forwardingAddress='.join('&forwardingAddress=', @fwd);
    $log->info($logprefix.'Forwarding to be added/set "'.$fwd.'"');

    # Step 1) Set forwardingEnabled=yes
    $mos->user($email);
    $retval = $mos->SetForwardingSettings('forwardingEnabled=yes');
    if (!$mos->is_success)
    {
        chomp($retval);
        $log->error($logprefix.'Forwarding data on 1 of 3 failed: '.$retval);
        $overallSuccess = 0;
        if ($self->{'DEBUG'})
        {
            $mos->PrintResponse();
        }
    }
    else
    {
        $log->info($logprefix.'Forwarding data 1 of 3 complete');
    }

    # Step 2) Set copyOnForward=yes/no
    $mos->user($email);
    my $keepCopy = 0;
    if ($keepCopy == 1)
    {
        $retval = $mos->SetForwardingSettings('copyOnForward=yes');
    }
    else
    {
        $retval = $mos->SetForwardingSettings('copyOnForward=no');
    }
    if (!$mos->is_success)
    {
        chomp($retval);
        $log->error($logprefix.'Forwarding data on 2 of 3 failed: '.$retval);
        $overallSuccess = 0;
        if ($self->{'DEBUG'})
        {
            $mos->PrintResponse();
        }
    }
    else
    {
        $log->info($logprefix.'Forwarding data 2 of 3 complete');
    }

    $mos->user($email);
    $retval = $mos->SetForwardingAddresses($fwd);
    if (!$mos->is_success)
    {
        chomp($retval);
        $log->warn($logprefix.'Forwarding data on 3 of 3 failed: '.$retval);
        $overallSuccess = 0;
        if ($self->{'DEBUG'})
        {
            $mos->PrintResponse();
        }
    }
    else
    {
        $log->info($logprefix.'Forwarding data 3 of 3 complete');
    }

    if ($overallSuccess)
    {
        return($com::owm::migTelstraEvents::STATUS_SUCCESS);
    }
    else
    {
        return($com::owm::migTelstraEvents::STATUS_FAIL_FORWARDING);
    }
}

#############################################################################
#
# Read the mail autoreply settings from the cached user preferences and
# push them to mOS
#
#############################################################################
sub migrateAutoreply($$$$$)
{
    my ($self, $email, $cfg, $log, $mos) = @_;
    my $logprefix = $self->{'logprefix'};
    my $overallSuccess = 1;

    # Check that the values were saved by migrateUserConfiguration earlier
    unless ($self->{'userpref'}{'userpref_loaded'})
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateAutoreply() without previous successful call to '.
                'migrateUserConfiguration().  Cannot continue');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    my $mode = 'none';
    my $message = '';

    if (defined($self->{'userpref'}{'outOfOfficeSettings'}{'autoReplyMode'}))
    {
        if (uc($self->{'userpref'}{'outOfOfficeSettings'}{'autoReplyMode'}) eq 'REPLY_ALWAYS')
        {
            $mode = 'reply';
        }
        elsif (uc($self->{'userpref'}{'outOfOfficeSettings'}{'autoReplyMode'}) eq 'REPLY_WITH_ECHO')
        {
            $mode = 'echo';
        }
        elsif (uc($self->{'userpref'}{'outOfOfficeSettings'}{'autoReplyMode'}) eq 'REPLY_ONCE')
        {
            $mode = 'reply';
            $log->info($logprefix.'MyInbox autoreply mode set to REPLY_ONCE ' .
                'which is not supported in Ux Suite.  Replacing with equivalent ' .
                'of REPLY_ALWAYS')
        }
        elsif (uc($self->{'userpref'}{'outOfOfficeSettings'}{'autoReplyMode'}) ne 'DISABLED')
        {
            # Check that we only get expected values
            $log->error($logprefix.'Unrecognised autoreply mode in SOAP: '.
                $self->{'userpref'}{'outOfOfficeSettings'}{'autoReplyMode'});
            return($com::owm::migTelstraEvents::STATUS_FAIL_VACATION);
        }

        if (defined($self->{'userpref'}{'outOfOfficeSettings'}{'outOfOfficeMessage'}))
        {
            $message = $self->{'userpref'}{'outOfOfficeSettings'}{'outOfOfficeMessage'};
        }
    }
    else
    {
        $log->info($logprefix.'MyInbox has no autoreply mode data in SOAP reply');
    }

    $mos->user($email);
    my $retval = $mos->SetVacation($mode, $message);
    if ($mos->is_success)
    {
        $log->info($logprefix.'Setting vacation complete.  Mode: '.$mode);
        return($com::owm::migTelstraEvents::STATUS_SUCCESS);
    }

    $log->error($logprefix.'Setting vacation mode "'.$mode.'" failed: '.$retval);
    return($com::owm::migTelstraEvents::STATUS_FAIL_VACATION);
}

#############################################################################
#
# Try to figure out the display name for the user
#
#############################################################################
sub migrateName($$$$$)
{
    my ($self, $email, $cfg, $log, $mos) = @_;
    my $logprefix = $self->{'logprefix'};

    # check for required SOAP connection configuration
    unless (defined($self->{'userWsdl'}{'path'}) &&
            defined($self->{'userWsdl'}{'compiled'}))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateUserConfiguration() without UserManagementService '.
                'WSDL loaded.  Cannot continue');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    # The real user email cached by getUserByUsername() is needed
    unless (defined($self->{'user'}) && defined($self->{'user'}{'myinbox_email'}))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateName() without valid user email address. '.
                'Cannot continue');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    my $soap = $self->{'configWsdl'}{'compiled'};
    (my $soapProxy = $self->{'userWsdl'}{'path'}) =~ s/\?wsdl$//;
    my ($answer, $faultData, $failed);
    my $retryCount = 0;
    $soap_call_name = $email.': getUser';
    do
    {
        $failed = 0;
        $retryCount ++;
        sleep($retryCount);
        local $SIG{__DIE__} = 'DEFAULT';
        # Query SOAP service.  Have to build the request by hand
        # as SOAP:Lite has some problem doing it automatically
        $answer =
            $self->{'userWsdl'}{'compiled'}->on_fault(sub
            {
                my $soap = shift;
                my $fault = shift;
                $faultData = $fault;
                $failed = 1;
                return(undef); # else the last variable is returned
            })->proxy($soapProxy)->call('getUser',
                SOAP::Data->name('getUserVO')->value(
                    \SOAP::Data->name('userInfo')->value(
                        \SOAP::Data->name('emailID')->value(
                            $self->{'user'}{'myinbox_email'}
                        )
                    )
                )
            );

            $failed = 1 unless (defined($answer->result));
    } while ($failed && ($retryCount < 3));

    # the server responded with an error, likely a HTTP error (e.g. 500)
    if (defined($faultData))
    {
        if (defined($log))
        {
            if (ref(\$faultData) eq 'SCALAR')
            {
                eval
                {
                    local $SIG{__DIE__} = 'DEFAULT';
                    $log->error($logprefix.'Error from SOAP interface while '.
                        'fetching user name: '.$faultData);
                };
            }
            else
            {
                eval
                {
                    local $SIG{__DIE__} = 'DEFAULT';
                    $log->error($logprefix.'Error message from SOAP server while '.
                        'fetching user name: '.$faultData->faultstring);
                }
            }
        }

        $soap_call_name = $email;
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    if ($failed)
    {
        log->error($logprefix.'Failed getUser call');
        $soap_call_name = $email;
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    my $firstName = ' ';
    my $lastName  = ' ';

    if (defined($answer->result) && defined($answer->result->{'firstname'}))
    {
        $firstName = $answer->result->{'firstname'};
    }

    if (defined($answer->result) && defined($answer->result->{'lastname'}))
    {
        $lastName = $answer->result->{'lastname'};
    }

    $mos->user($email);
    my $retval = $mos->updateAccountBase($email, $firstName, $lastName);
    unless ($mos->is_success)
    {
        chomp($retval);
        $log->error($logprefix.'Failed to set firstname/lastname: firstname '.
            '= "'.$firstName.'", lastname = "'.$lastName.'", response: '.
            $retval);
        if ($self->{'DEBUG'})
        {
            $mos->PrintResponse();
        }
        $soap_call_name = $email;
        return($com::owm::migTelstraEvents::STATUS_FAIL_MOS_SETTING);
    }
    else
    {
        $log->info($logprefix.'Set first name to: "'.$firstName.'"');
        $log->info($logprefix.'Set last name to: "'.$lastName.'"')
    }

    $soap_call_name = $email;
    return($com::owm::migTelstraEvents::STATUS_SUCCESS);
}

#############################################################################
#
# Handle the data retrieved from SOAP and push to mOS
#
#############################################################################
sub processSignature($$$$$$$)
{
    my ($self, $email, $cfg, $log, $mos, $sigName, $sigObject) = @_;
    my $logprefix = $self->{'logprefix'};
    my $mosTarget = $cfg->param('signature_target');  # target should be 'mss' or 'appsuite'

    # If there is only one signature then we don't get a hash with each
    # key as the name of the signature for some reason, we just get the
    # structure for the only signature present
    my $sigDefault = 'no';
    if (lc($sigObject->{'defaultSignature'}) eq 'true')
    {
        $sigDefault = 'yes';
    }
    my $signature = ' ';
    if (ref(\($sigObject->{'content'})) eq 'SCALAR')
    {
        $signature = $sigObject->{'content'};
    }
    my $format = $sigObject->{'format'};
    # The way to signal to Ux Suite that this is a HTML signature is
    # to add the Content-Type header.
    if (uc($format) eq 'RICH_TEXT')
    {
        $signature = 'Content-Type: text/html'."\n".$signature;
    }

    $mos->user($email);
    my $retval = $mos->signatureCreate($mosTarget, $signature, $sigName,
        $sigDefault);
    unless ($mos->is_success)
    {
        chomp($retval);
        $log->error($logprefix.'Failed to add signature called "'.
            $sigName.'": '.$retval);
        return($com::owm::migTelstraEvents::STATUS_FAIL_MOS_SETTING);
    }
    else
    {
        $log->info($logprefix.'Added signature called "'.$sigName.'"');
    }

    return($com::owm::migTelstraEvents::STATUS_SUCCESS);
}

#############################################################################
#
# Read the mail signature settings from the SOAP server and push them to mOS
#
#############################################################################
sub migrateSignature($$$$$)
{
    my ($self, $email, $cfg, $log, $mos) = @_;
    my $logprefix = $self->{'logprefix'};

    # check for required SOAP connection configuration.  This uses data not
    # returned by getUserConfiguration SOAP call in migrateUserConfiguration
    unless (defined($self->{'configWsdl'}{'path'}) &&
            defined($self->{'configWsdl'}{'compiled'}))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateSignature() without ConfigurationService WSDL '.
                'loaded.  Cannot continue');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    # The numeric user ID cached by getUserByUsername() is needed
    unless (defined($self->{'user'}) && defined($self->{'user'}{'userid'}))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateSignature() without valid numeric user ID. '.
                'Cannot continue');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    # Check that we are saving to the mail store (mss) via mOS.  Other targets
    # are not supported by this module
    my $setVia = $cfg->param('signature_set_via');
    my $mosTarget = $cfg->param('signature_target');  # target should be 'mss' or 'appsuite'
    unless ($setVia eq 'mos')
    {
        if (defined($log))
        {
            $log->error($logprefix.'com::owm::migTelstraInbox::'.
                'migrateSignature() can only support writing to mOS '.
                'however migration is configrured for '.$setVia);
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    unless ($mosTarget eq 'mss')
    {
        if (defined($log))
        {
            $log->error($logprefix.'com::owm::migTelstraInbox::'.
                'migrateSignature() can only support writing to mss '.
                'however migration is configrured for '.$mosTarget);
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    $soap_call_name = $email.': getMailSignatures';
    my ($answer, $failed, $parsedAnswer, $soapBody);
    my $retryCount = 0;
    my $xmlparser = XML::Simple->new();
    do
    {
        $failed = 0;
        $retryCount ++;
        sleep($retryCount);
        my $soap = $self->{'configWsdl'}{'compiled'};
        {
            local $SIG{__DIE__} = 'DEFAULT';
            # work around problem where SOAP::Lite can't parse the response properly
            $soap->outputxml('true');
            $answer = $soap->getMailSignatures($self->{'user'}{'userid'});
            # undo workaround
            $soap->outputxml('false');
        }

        # the server responded with an error, likely a HTTP error (e.g. 500)
        if (!defined($answer))
        {
            if (defined($log))
            {
                $log->error($logprefix.'Error from SOAP server while '.
                    'fetching signatures');
            }

            $failed = 1;
        }
        else
        {
            $parsedAnswer = eval { $xmlparser->XMLin($answer, NormaliseSpace => 0); };
            if ($@)
            {
                $log->error($logprefix.'Unable to parse XML response '.
                    'from getMailSignatures: '.$@);
                $failed = 1;
            }
            if (defined($parsedAnswer) && defined($parsedAnswer->{'soapenv:Body'}))
            {
                $soapBody = $parsedAnswer->{'soapenv:Body'};
                if (defined($soapBody->{'soapenv:Fault'}))
                {
                    $failed = 1;
                    my $fault = $soapBody->{'soapenv:Fault'};
                    if (defined($fault->{'faultcode'}) &&
                        defined($fault->{'faultstring'}))
                    {
                        my $error = $fault->{'faultstring'};
                        $log->error($logprefix.'Error message from SOAP server while '.
                                                'fetching signatures: '.$error);
                    }
                    else
                    {
                        $log->error($logprefix.'Unknown error from SOAP server while '.
                                                'fetching signatures');
                    }
                }
            }
            else
            {
                unless($failed)
                {
                    $log->error($logprefix.'No soapenv:Body in response from SOAP server');
                    $failed = 1;
                }
            }
        }
    } while ($failed && $retryCount < 3);
    if ($failed)
    {
        $soap_call_name = $email;
        return($com::owm::migTelstraEvents::STATUS_FAIL_SIGNATURE);
    }

    # Parse the response the hard way rather than relying on the SOAP::Lite
    # parser derived from  the WSDL.  Possibly a problem with the Java module
    # used to add the WS-Security signatures to the SOAP request.
    my $signatures = $soapBody->{'getMailSignaturesResponse'};
    $signatures = $signatures->{'getMailSignaturesReturn'};
    my @sigNames;
    my $singleSignature = 1;
    if (ref($signatures) eq 'HASH')
    {
        @sigNames = keys(%{$signatures});
        if (scalar(@sigNames) == 0)
        {
            $log->info($logprefix.'No signatures returned from SOAP');
            $soap_call_name = $email;
            return($com::owm::migTelstraEvents::STATUS_SUCCESS);
        }

        foreach my $item ('format', 'name', 'content', 'id', 'defaultSignature')
        {
            if (defined($signatures->{$item}))
            {
                print $item.': '.ref(\($signatures->{$item}))."\n";
                if ($item eq 'content')
                {
                    if ((ref(\($signatures->{$item})) ne 'SCALAR') &&
                        (ref(\($signatures->{$item})) ne 'REF'))
                    {
                        print 'no content - not single signature'."\n";
                        $singleSignature = 0;
                        last;
                    }
                }
                elsif ($item eq 'name')
                {
                    if ((ref(\($signatures->{$item})) ne 'SCALAR') &&
                        (ref(\($signatures->{$item})) ne 'REF'))
                    {
                        print 'no name - not single signature'."\n";
                        $singleSignature = 0;
                        last;
                    }
                }
                elsif (ref(\($signatures->{$item})) ne 'SCALAR')
                {
                    $singleSignature = 0;
                    last;
                }
            }
            else
            {
                $singleSignature = 0;
                last;
            }
        }

    }
    elsif (ref($signatures) eq 'ARRAY')
    {
        $singleSignature = 0;
        if (scalar(@{$signatures} == 0))
        {
            $log->info($logprefix.'No signatures returned from SOAP');
            $soap_call_name = $email;
            return($com::owm::migTelstraEvents::STATUS_SUCCESS);
        }
    }
    elsif (!exists($signatures->{'getMailSignaturesReturn'}))
    {
        $log->info($logprefix.'No signatures configured for user');
        $singleSignature = 0;
    }
    else
    {
        $log->error($logprefix.'Unknown format returned from '.
            'getMailSignatures: '.ref($signatures));
        $soap_call_name = $email;
        return($com::owm::migTelstraEvents::STATUS_FAIL_SIGNATURE);
    }

    if ($singleSignature)
    {
        if ((uc($signatures->{'format'}) eq 'TEXT') ||
            (uc($signatures->{'format'}) eq 'RICH_TEXT'))
        {
            my $sigName = ' ';
            if (defined($signatures->{'name'}) && 
                (ref(\($signatures->{'name'})) eq 'SCALAR'))
            {
                $sigName = $signatures->{'name'};
            }
            $soap_call_name = $email;
            return($self->processSignature($email, $cfg, $log, $mos, $sigName,
                                           $signatures));
        }
        else
        {
            $log->error($logprefix.'Error processing signture data - data is '.
                'in an unexpected format - single signature with unknown format');
            $soap_call_name = $email;
            return($com::owm::migTelstraEvents::STATUS_FAIL_SIGNATURE);
        }
    }

    # There can be multiple signatures, so iterate over the array and add
    # each one individually.
    if (ref($signatures) eq 'HASH')
    {
        foreach my $sigName (@sigNames)
        {
            print 'ref(\$signatures->{'.$sigName.'}: '.ref(\($signatures->{$sigName}))."\n";
            print 'ref($signatures->{'.$sigName.'}: '.ref($signatures->{$sigName})."\n";
            if (ref($signatures->{$sigName}) ne 'HASH')
            {
                $log->error($logprefix.'Error processing signture data - data is '.
                    'in an unexpected structure');
                $soap_call_name = $email;
                return($com::owm::migTelstraEvents::STATUS_FAIL_SIGNATURE);
            }
            my $retval = $self->processSignature($email, $cfg, $log, $mos,
                                                 $sigName, $signatures->{$sigName});
            unless($retval == $com::owm::migTelstraEvents::STATUS_SUCCESS)
            {
                $soap_call_name = $email;
                return($retval);
            }
        }
    }
    elsif (ref($signatures) eq 'ARRAY')
    {
        foreach my $signature (@{$signatures})
        {
            my $sigName = ' ';
            if (defined($signature->{'name'}) && 
                (ref(\($signature->{'name'})) eq 'SCALAR'))
            {
                $sigName = $signature->{'name'};
            }
            my $retval = $self->processSignature($email, $cfg, $log, $mos,
                                                 $sigName, $signature);
            unless($retval == $com::owm::migTelstraEvents::STATUS_SUCCESS)
            {
                $soap_call_name = $email;
                return($retval);
            }
        }
    }

    $soap_call_name = $email;
    return($com::owm::migTelstraEvents::STATUS_SUCCESS);
}

#############################################################################
#
# Run a python script in a child process and capture the output so it can
# be logged or gathered for statistics
#
#############################################################################
sub runPythonChild($$$$$$)
{
    my ($email, $puid, $log, $cfg, $processName, $args) = @_;
    my $logprefix = $email.'|'.$puid.':';
    my @args = @{ $args }; # turn arrayref back into array
    my %statistics;
    my %logStats;
    my $ok = 0;

    my ($pid, $child_in, $child_out, $child_err);
    # if this is not done, open3 considers $child_out and $child_err to be
    # the same and redirects stderr to stdout.
    $child_err = gensym();

    # do a fork, but with STDIN, STDOUT and STDERR redirected to the file
    # handles provided.  Needs to be in an eval { } block as it uses Carp
    # to report errors, which kills this process
    eval
    {
        $pid = open3($child_in, $child_out, $child_err, '-');
    };

    if ($@)
    {
        $log->error($logprefix.'Failed to start child for '.$processName.': '.$@);
        return(-1);
    }

    if ($pid == 0)
    {
        # child process

        # get rid of these file handles before they cause a problem.  They
        # should only be used in the parent process
        close($child_in);
        close($child_out);
        close($child_err);

        # Turn off stdio buffering in python
        $ENV{'PYTHONUNBUFFERED'} = 'True';
        # Add local python libraries
        if ($cfg->param('myinbox_python_path') &&
            ($cfg->param('myinbox_python_path') ne ''))
        {
            my $path = $cfg->param('myinbox_python_path');
            $ENV{'PYTHONPATH'} = $path;
        }
        else
        {
            $ENV{'PYTHONPATH'} = $FindBin::Bin.'/python/lib';
        }
        # Force STDIO to be in UTF-8 so trace logs work
        $ENV{'PYTHONIOENCODING'} = 'utf-8';

        # Run the exporter.  Use exec() as it doesn't return.
        exec(@args);
    }
    else
    {
        # parent process

        # Won't be writing any data to the child, so disponse of this file
        # handle before proceeding to be safe
        close($child_in);

        # Hash to hold the status codes for children that have exited and
        # been caught in the SIGCHLD handler.
        my %child_status;

        # I suspect due to being run under nohup, SIGCHLD is set to
        # SIGIGN (ignore) before we get here, so override it with a
        # handler local to this block that stashes the status in the
        # %child_status hash
        local $SIG{CHLD} = sub
        {
            while ((my $child = waitpid(-1, WNOHANG)) > 0)
            {
                $child_status{$child} = $?;
            }
        };

        # would love to use <> here, but need to separate stdout and stderr
        # so the log events can be handled appropriately.  Two file
        # descriptors makes using <> difficult (if not impossible), so
        # use IO::Select instead
        my $sel = IO::Select->new();

        if ($child_out)
        {
            # treat file as UTF-8
            binmode($child_out, ":encoding(utf8)");
            # need to use non-blocking reads else the first 'sysread' will
            # block until the requested buffer size is full. I'd rather not
            # use character at a time reads (i.e. a read of one byte) to
            # reduce cpu usage, so unbuffered it is.
            $child_out->blocking(0);
            $sel->add($child_out);
        }

        if ($child_err)
        {
            # treat file as UTF-8
            binmode($child_err, ":encoding(utf8)");
            # need to use non-blocking reads else the first 'sysread' will
            # block until the requested buffer size is full. I'd rather not
            # use character at a time reads (i.e. a read of one byte) to
            # reduce cpu usage, so unbuffered it is.
            $child_err->blocking(0);
            $sel->add($child_err);
        }

        # cache partial line reads until we get a line feed and can
        # log a complete line
        my %buffer = ( 'error' => '', 'out' => '' );

        my $ucprocessname = ucfirst($processName);
        my $noOutputCount = 0;

        # loop until waitpid says the child process exited.
        while($noOutputCount < 10)
        {
            my $noOutput = 1;
            # 2 is the maximum number of seconds that can_read can block.
            # allows the code to exit the while if there is nothing
            # to read and the SIGCHLD handler has seen the child die
            my @ready = $sel->can_read(2);
            # print 'number ready: '.scalar(@ready)."\n";
            foreach my $file (@ready)
            {
                # handle STDERR having data ready to read
                if ($child_err && ($file == $child_err))
                {
                    # print 'stderr ready'."\n";
                    my $name = 'error';
                    # can't use readline / <> as they buffer input and
                    # can confuse IO::Select.
                    while(sysread($file, my $buffer, 16384))
                    {
                        $buffer{$name} = $buffer{$name} . $buffer;
                        $noOutput = 0;
                    }
                    # print 'buffer: "'.$buffer{$name}.'"'."\n";
                    while($buffer{$name} =~ /\n/)
                    {
                        # splits buffer in two.  The first result is
                        # the first line in the buffer, and the second is
                        # everying after that (even if there are multiple
                        # lines in the buffer).  This is necessary since only
                        # one line can be passed into the logger to prevent
                        # the log format from being broken.
                        my ($line, $remaining) = split(/\n/, $buffer{$name}, 2);
                        # save what was left for the next pass around the loop
                        $buffer{$name} = $remaining;
                        # next if ($line =~ /close failed in file object destructor:/);
                        # next if ($line =~ /IOError: \[Errno 10\] No child processes/);
                        $log->error($logprefix.'Unexpected STDERR output '.
                            'from '.$processName.': '. $line);
                    }
                }

                # handle STDOUT having data ready to read
                if ($child_out && ($file == $child_out))
                {
                    # print 'stdout ready'."\n";
                    my $name = 'out';
                    # can't use readline / <> as they buffer input and
                    # can confuse IO::Select.
                    while(sysread($file, my $buffer, 16384))
                    {
                        $buffer{$name} = $buffer{$name} . $buffer;
                        $noOutput = 0;
                    }

                    while($buffer{$name} =~ /\n/)
                    {
                        # splits buffer in two.  The first result is
                        # the first line in the buffer, and the second is
                        # everying after that (even if there are multiple
                        # lines in the buffer).  This is necessary since only
                        # one line can be passed into the logger to prevent
                        # the log format from being broken.
                        my ($line, $remaining) = split(/\n/, $buffer{$name}, 2);
                        # save what was left for the next pass around the loop
                        $buffer{$name} = $remaining;
                        # All the lines to stdout should have a prefix tag
                        # which indicates the type of log event
                        if ($line =~ /^STAT:/)
                        {
                            # Special tag representing a statistic.  Line format
                            # should be STAT:statname:value
                            if ($line !~ /^STAT:[a-zA-Z]+:[0-9]+$/)
                            {
                                $log->info($logprefix.''.$ucprocessname.
                                           ' produced unrecognised STAT '.
                                           'line: '.$line);
                            }
                            else
                            {
                                my ($tag, $stat, $value) = split(/:/, $line);
                                $statistics{$stat} = $value;
                            }
                        }
                        elsif ($line =~ /^TRACE:/)
                        {
                            $line =~ s/^TRACE://;
                            print $line."\n";
                            $logStats{'trace'} += 1;
                        }
                        elsif ($line =~ /^DEBUG:/)
                        {
                            $line =~ s/^DEBUG://;
                            $log->debug($logprefix.''.$ucprocessname.
                                        ':'.$line);
                            $logStats{'debug'} += 1;
                        }
                        elsif ($line =~ /INFO:/)
                        {
                            $line =~ s/^INFO://;
                            $log->info($logprefix.''.$ucprocessname.
                                       ':'.$line);
                            $logStats{'info'} += 1;
                        }
                        elsif ($line =~ /WARN:/)
                        {
                            $line =~ s/^WARN://;
                            $log->warn($logprefix.''.$ucprocessname.
                                       ':'.$line);
                            $logStats{'warn'} += 1;
                        }
                        elsif ($line =~ /ERROR:/)
                        {
                            $line =~ s/^ERROR://;
                            $log->error($logprefix.''.$ucprocessname.
                                        ':'.$line);
                            $logStats{'error'} += 1;
                        }
                        elsif ($line =~ /FATAL:/)
                        {
                            $line =~ s/^FATAL://;
                            $log->fatal($logprefix.''.$ucprocessname.
                                        ':'.$line);
                            $logStats{'fatal'} += 1;
                        }
                        else
                        {
                            $log->error($logprefix.'Untagged '.$processName.
                                        ' log event:'.$line);
                            $logStats{'untagged'} += 1;
                        }
                    }
                }
            } # foreach my $file (@ready)
            if (defined($child_status{$pid}) && $noOutput)
            {
                # child has exited, wait a few loops with no output
                # before processing the exit so we get all the
                # remaining output
                $noOutputCount += 1;
            }
        } # while(!defined($child_status{$pid}))

        my $child_exit_status = $child_status{$pid} >> 8;
        my $child_signal_status = $child_status{$pid} & 127;

        # check for abnormal exit, either via a non-zero exit
        # code or a signal that killed the process.
        if ($child_exit_status != 0)
        {
            $log->error($logprefix.''.$ucprocessname.
                        ' exited with an error code of '.
                        $child_exit_status);
            $logStats{'error'} += 1;
        }
        elsif ($child_signal_status != 0)
        {
            # don't bother trying to turn the numeric signal
            # in to a signal name.
            $log->error($logprefix.''.$ucprocessname.
                        ' received a signal of '.
                        $child_signal_status);
            $logStats{'error'} += 1;
        }
        else
        {
            $log->info($logprefix.''.$ucprocessname.
                       ' exited normally');
            $logStats{'info'} += 1;
            $ok = 1;
        }
    } # if ($pid == 0)
    return($ok, \%statistics, \%logStats);
}

#############################################################################
#
# Call the python script to export the users calendar and then import it to
# the OWM CAL server.
#
#############################################################################
sub migrateCalendar($$$$$$$$$$)
{
    my ($self, $log, $mos, $cfg, $email, $password, $calstorehost,
        $start_date, $end_date, $SDEBUG) = @_;
    my $logprefix = $self->{'logprefix'};
    my %statistics;
    my $cal_base_url =  $self->{'config'}->param('cal_base_url');
    my ($port, $error) = com::owm::migBellCanada::getPortNumber('http', 'cal', $calstorehost, $log, $cfg);
    if ($error == 1)
    {
        $log->error($logprefix.'Error calculating port number for calendar '.
            'server: '.$port);
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }
    $cal_base_url =~ s/<host>/$calstorehost/;
    $cal_base_url =~ s/<port>/$port/;
    my $url_user_cal = $cal_base_url.'/'.$email.'/';
    $log->debug($logprefix.'User calendar url is '.$url_user_cal);

    # check for required SOAP connection configuration
    unless (defined($self->{'eventWsdl'}{'path'}))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateCalendar() without EventManagementService.wsdl '.
                'loaded.  Cannot migrate calendar');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }

    my $cal_pab_target = $self->{'config'}->param('cal_pab_target');
    # This module only supports migrating to Ux Suite
    if ($cal_pab_target eq 'appsuite')
    {
        if (defined($log))
        {
            $log->error($logprefix.'MyInbox migration code only supports '.
                'UX Suite.  Migration configured for AppSuite. Cannot '.
                'migrate calendar');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }

    # The real user email cached by getUserByUsername() is needed
    unless (defined($self->{'user'}) && defined($self->{'user'}{'myinbox_email'}))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateCalendar() without valid user email address. '.
                'Cannot continue');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }


    my $calendar_endpoint = $self->{'eventWsdl'}{'path'};
    my $tempfile = 'tmp/myinbox.'.$email.'.ics';

    # Make sure that the temporary output file doesn't already exist
    if (-f $tempfile)
    {
        unlink($tempfile);
    }

    # Find the URL for the users primary calendar
    my $cal_href = com::owm::CALClient::findPrimaryCal($url_user_cal,
                                                       $email,
                                                       $password,
                                                       $self->{'DEBUG'});

    unless (defined($cal_href))
    {
        $log->error($logprefix.'Primary calendar does not exist. Migration failed');
        return($com::owm::migTelstraEvents::STATUS_FAIL_AS_SET_CALENDAR,
               $email, %statistics);
    }

    # Turn the relative calendar URL returned by findPrimaryCal to a full URL
    my $cal_url = URI->new_abs($cal_href, $url_user_cal);
    # ensure there is a folder separator at the end of the new URL
    if ($cal_url !~ /\/$/)
    {
        $cal_url .= '/';
    }
    $log->debug($logprefix.'User primary calendar url is '.$url_user_cal);

    $log->info($logprefix.'Migrating calendar with a timezone of "'.
        $self->{'userpref'}{'timeZone'}.'"');

    # use URL application type Base64 encoding to prevent odd characters in
    # passwords from being interpreted by a shell (for example) when running
    # the exporter.  May have issues if people use non-US-ASCII passwords as
    # technically Base64 only encodes 8 bit data.
    my $encoded_password = encode_base64($password, '');
    $encoded_password =~ s/\+/-/g; # Do URL encoding
    $encoded_password =~ s/\//_/g; # Do URL encoding
    # work out the arguments to run the exporter
    my @args = ( $FindBin::Bin.'/myinbox-calendar-export.py',
        '--email='.$self->{'user'}{'myinbox_email'},
        '--password='.$encoded_password,
        '--serviceURL='.$calendar_endpoint,
        '--startdate='.$start_date,
        '--enddate='.$end_date,
        '--output='.$tempfile,
        '--timezone='.$self->{'userpref'}{'timeZone'},
        '--mosHost='.$self->{'config'}->param('mosHost'),
        '--mosPort='.$self->{'config'}->param('mosPort'),
    );

    # If configured to do so, turn off the "Use Client Timezone" option in
    # Ux Suite settings
    my $disableClientTimezone = $self->{'config'}->param('disable_client_timezone');
    if (defined($disableClientTimezone))
    {
        if (lc($disableClientTimezone) eq 'true')
        {
            push(@args, '--disableClientTimezone');
        }
    }

    if (defined($self->{'user'}{'userid'}))
    {
        push(@args, '--userid='.$self->{'user'}{'userid'});
    }

    if ($self->{'DEBUG'})
    {
        push(@args, '--tracelogs');
    }

    push(@args, '--cacheDays='.$self->{'cacheDays'});

    # Run the python process that exports the calendar.  Done in python as
    # SOAP::Lite and XML::Compile::SOAP11 had problems with the SOAP interface
    my ($ok, $statistics, $logStats) =
        runPythonChild($email, 'MYI', $log, $cfg, 'calendar exporter', \@args);
    # Turn the statistics gathered by runPythonChild on the number of
    # individual warning or worse log events into statistics returned to
    # migrateBatch.plx
    if ($logStats->{'warn'})
    {
        $statistics{'calendarExporterWarnings'} = $logStats->{'warn'};
    }
    if ($logStats->{'error'})
    {
        $statistics{'calendarExporterErrors'} = $logStats->{'error'};
    }
    if ($logStats->{'fatal'})
    {
        $statistics{'calendarExporterFatalErrors'} = $logStats->{'fatal'};
    }
    if ($logStats->{'untagged'})
    {
        $statistics{'calendarExporterUntaggedLogs'} = $logStats->{'untagged'};
    }
    %statistics = %{ $statistics };
    unless($ok)
    {
        $log->error($logprefix.'Calendar exporter has exited abnormally. '.
            'Migration failed.');
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }
    $statistics{'eventsLoaded'} = 0;
    $statistics{'eventsImported'} = 0;
    $statistics{'eventsImportError'} = 0;
    $statistics{'eventsImportedPreviously'} = 0;

    unless (-f $tempfile)
    {
        $log->error($logprefix.'Temporary file not found after calendar '.
                    'exporter has exited.  Migration failed.');
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }

    unless (-r $tempfile)
    {
        $log->error($logprefix.'Temporary file not readable after calendar '.
                    'exporter has exited.  Migration failed.');
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }

    # load the exported file.  Data::ICal is used to parse the ics file
    # back into individual events
    my $calendar = Data::ICal->new(filename => $tempfile);
    my @entries = @{ $calendar->entries() };
    my $prefTz = $self->{'userpref'}{'timeZone'};
    # The events saved by the exporter don't have the VTIMEZONE information
    # yet.  MyInbox events don't seem to have a per-event TZ so use the
    # configured TZ
    my $vTimezone = com::owm::OlsonToVTimezone::getVtimezone($prefTz, $cfg, $log);
    unless(defined($vTimezone))
    {
        $log->error($logprefix.'Cannot convert TZ name "'.$prefTz.'" into '.
            'icalendar VTIMEZONE object: TZ not found');
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }
   
    # Generic header for a calendar event.
    my $icalStub = 'BEGIN:VCALENDAR'."\n";
    $icalStub .= 'PRODID://CRITICAL PATH//NONSGML CP Webtop//EN'."\n";
    $icalStub .= 'VERSION:2.0'."\n";
    # iterate over each entry and import it to OWM CAL server individually
    # This is so events already imported are not duplicaated by using the
    # "If-None-Match" header to make sure that the event ID wasn't already
    # present
    foreach my $entry (@entries)
    {
        $statistics{'eventsLoaded'} += 1;

        my @uids = @{ $entry->property('uid') };
        my $uid = $uids[0]->value();
        # Generate a new calendar from the event
        my $eventAsCal = $icalStub;
        $eventAsCal .= $vTimezone;
        $eventAsCal .= $entry->as_string();
        $eventAsCal .= 'END:VCALENDAR'."\n";
        $eventAsCal =~ s/[\r\n]+/\r\n/g;
        if ($self->{'DEBUG'})
        {
            print $eventAsCal;
        }
        # "If-None-Match" header relies on the URL being unique to each
        # event.  Handle this by appending the event UID to the URL.
        my $uid_url = $cal_url . $uid;
        # Push the event to the CAL server
        my $resp = com::owm::CALClient::putString($uid_url,
                                                  $email,
                                                  $password,
                                                  $eventAsCal,
                                                  $self->{'DEBUG'});
        # 412 = event ID already exists
        if ($resp->code == 412)
        {
            $statistics{'eventsImportededPreviously'} += 1;
            $log->debug($logprefix.'Event UID "'.$uid.
                        '" was imported on a previous run');
        }
        elsif (($resp->code == 200) || ($resp->code == 201))
        {
            $statistics{'eventsImporteded'} += 1;
        }
        else
        {
            $log->error($logprefix.'Failed to import event UID "'.$uid.
                        '": '.$resp->message);
            $statistics{'eventsImportError'} += 1;
        }
    }

    if ((not $self->{'DEBUG'}) && ($statistics{'eventsImportError'} == 0))
    {
        unlink($tempfile);
    }

    return($com::owm::migTelstraEvents::STATUS_SUCCESS, $email, %statistics);
}
#############################################################################
#
# Call the python script to export the users contacts and then import it to
# the OWM PAB server.
#
#############################################################################
sub migrateContacts($$$$$$$$)
{
    my ($self, $log, $mos, $cfg, $email, $password, $pabstorehost, $SDEBUG) = @_;
    my $logprefix = $self->{'logprefix'};
    my %statistics;
    # Figure out the URL for the users address book
    my $pab_base_url =  $self->{'config'}->param('pab_base_url');
    my ($port, $error) = com::owm::migBellCanada::getPortNumber('http', 'pab', $pabstorehost, $log, $cfg);
    if ($error == 1)
    {
        $log->error($logprefix.'Error calculating port number for contacts '.
            'server: '.$port);
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }
    $pab_base_url =~ s/<host>/$pabstorehost/;
    $pab_base_url =~ s/<port>/$port/;
    my $url_user_pab = $pab_base_url.'/'.$email.'/Main';
    $log->debug($logprefix.'User contact url is '.$url_user_pab);

    # check for required SOAP connection configuration
    unless (defined($self->{'contactWsdl'}{'path'}))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateContacts() without ContactManagementService.wsdl '.
                'loaded.  Cannot migrate contacts');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }

    # This module only supports migrating to Ux Suite
    my $cal_pab_target = $self->{'config'}->param('cal_pab_target');
    if ($cal_pab_target eq 'appsuite')
    {
        if (defined($log))
        {
            $log->error($logprefix.'MyInbox migration code only supports '.
                'UX Suite.  Migration configured for AppSuite. Cannot '.
                'migrate contacts');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }

    # The real user email cached by getUserByUsername() is needed
    unless (defined($self->{'user'}) && defined($self->{'user'}{'myinbox_email'}))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateContacts() without valid user email address. '.
                'Cannot continue');
        }

        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR);
    }

    my $contact_endpoint = $self->{'contactWsdl'}{'path'};
    my $global_endpoint = $self->{'globalWsdl'}{'path'};
    my $tempfile = 'tmp/myinbox.'.$email.'.vcf';

    # use URL application type Base64 encoding to prevent odd characters in
    # passwords from being interpreted by a shell (for example) when running
    # the exporter.  May have issues if people use non-US-ASCII passwords as
    # technically Base64 only encodes 8 bit data.
    my $encoded_password = encode_base64($password, '');
    $encoded_password =~ s/\+/-/g; # Do URL encoding
    $encoded_password =~ s/\//_/g; # Do URL encoding
    # work out the arguments to run the exporter
    my @args = ( $FindBin::Bin.'/myinbox-addressbook-export.py',
        '--email='.$self->{'user'}{'myinbox_email'},
        '--password='.$encoded_password,
        '--contactServiceURL='.$contact_endpoint,
        '--globalServiceURL='.$global_endpoint,
        '--output='.$tempfile,
        '--timezone='.$self->{'userpref'}{'timeZone'},
    );

    if (defined($self->{'user'}{'userid'}))
    {
        push(@args, '--userid='.$self->{'user'}{'userid'});
    }

    if ($self->{'DEBUG'})
    {
        push(@args, '--tracelogs');
    }

    push(@args, '--cacheDays='.$self->{'cacheDays'});

    # Run the python process that exports the contacts.  Done in python as
    # SOAP::Lite and XML::Compile::SOAP11 had problems with the SOAP interface
    my ($ok, $statistics, $logStats) =
        runPythonChild($email, 'MYI', $log, $cfg, 'contact exporter', \@args);
    %statistics = %{ $statistics };
    # Turn the statistics gathered by runPythonChild on the number of
    # individual warning or worse log events into statistics returned to
    # migrateBatch.plx
    if ($logStats->{'warn'})
    {
        $statistics{'contactExporterWarnings'} = $logStats->{'warn'};
    }
    if ($logStats->{'error'})
    {
        $statistics{'contactExporterErrors'} = $logStats->{'error'};
    }
    if ($logStats->{'fatal'})
    {
        $statistics{'contactExporterFatalErrors'} = $logStats->{'fatal'};
    }
    if ($logStats->{'untagged'})
    {
        $statistics{'contactExporterUntaggedLogs'} = $logStats->{'untagged'};
    }
    unless($ok)
    {
        $log->error($logprefix.'Contact exporter has exited abnormally. '.
            'Migration failed.');
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }
    $statistics{'cardsLoaded'} = 0;
    $statistics{'cardsImported'} = 0;
    $statistics{'cardsImportError'} = 0;
    $statistics{'cardsImportedPreviously'} = 0;

    unless (-f $tempfile)
    {
        $log->error($logprefix.'Temporary file not found after contact '.
                    'exporter has exited.  Migration failed.');
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }

    unless (-r $tempfile)
    {
        $log->error($logprefix.'Temporary file not readable after contact '.
                    'exporter has exited.  Migration failed.');
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }

    unless(open(TMPFILE, '<:encoding(UTF-8)', $tempfile))
    {
        $log->error($logprefix.'Temporary file cannot be opened after '.
                    'contact exporter has exited.  Migration failed. '.
                    'Error: '.$!);
        return($com::owm::migTelstraEvents::STATUS_GENERAL_ERROR,
               $email, %statistics);
    }

    my @vcard;
    my $vcardStarted = 0;
    my $vcardUid = '';
    my $vcardType = 'contact';

    # read the file in and separate out into individual VCARD entries
    WHILE: while(my $line = <TMPFILE>)
    {
        $line =~ s/[\r\n]+$//;
        # is this the start of a new VCARD?
        if ($line =~ /^BEGIN:VCARD$/)
        {
            $vcardStarted = 1;
            push(@vcard, $line);
            next WHILE;
        }

        # should never get here without a BEGIN:VCARD line being seen alread
        unless($vcardStarted)
        {
            $log->warn($logprefix.'Read line from vCard file while not in '.
                'a vCard BEGIN/END sequence: '.$line);
            next WHILE;
        }

        # save the line to the current VCARD
        push(@vcard, $line);

        # need to remember the UID for the import
        if ($line =~ /^UID:/)
        {
            $vcardUid = $line;
            $vcardUid =~ s/^UID://;
        }

        # X-ADDRESSBOOKSERVER-KIND tells what type of card is is - a group
        # or a regular contact.  Only need to remember this so the log event
        # below can be more informative.
        if ($line =~ /^X-ADDRESSBOOKSERVER-KIND:/)
        {
            $vcardType = $line;
            $vcardType =~ s/^X-ADDRESSBOOKSERVER-KIND://;
        }

        # is this the end of the VCARD?  If so, push the entry to the PAB
        # server and reset state variables ready for the next card
        if ($line =~ /^END:VCARD$/)
        {
            $statistics{'cardsLoaded'} += 1;
            my $vcard = join("\n", @vcard);
            my $resp = com::owm::PABClient::put($url_user_pab, $email,
                $password, $vcard, $log);
            if ($resp->code == 201)
            {
                $log->info($logprefix.'put '.$vcardType.' successful for '.
                    'vCard UID "'.$vcardUid.'"');
                $statistics{'cardsImported'} += 1;
            }
            elsif ($resp->code == 412)
            {
                $log->info($logprefix.''.$vcardType.' already exists for '.
                    'vCard UID "'.$vcardUid.'": '.$resp->code);
                $statistics{'cardsImportedPreviously'} += 1;
            }
            elsif ($resp->code == 424)
            {
                $log->error($logprefix.'put '.$vcardType.' failed, check '.
                    'file path location on PAB/CAL '.$resp->code);
                $statistics{'cardsImportError'} += 1;
            }
            else
            {
                $log->error($logprefix.'put '.$vcardType.' failed for '.
                    'vCard UID "'.$vcardUid.'": '.$resp->code);
                $statistics{'cardsImportError'} += 1;
            }
            # set up for next card
            $vcardStarted = 0;
            @vcard = ();
            $vcardUid = '';
            $vcardType = 'contact';
        }
    }

    if ((not $self->{'DEBUG'}) && ($statistics{'cardsImportError'} == 0))
    {
        unlink($tempfile);
    }

    return($com::owm::migTelstraEvents::STATUS_SUCCESS, $email, %statistics);
}

##############################################################################
#
# Take the structure returned from the SOAP call for an individual message
# and turn it into a set of IMAP flags that can be passed to the IMAP
# STORE command
#
#############################################################################
sub processFlagsForMsg($)
{
    my $msg = shift;
    my $flags;
    my @flags;
    if ($msg->{'deleted'})
    {
        push(@flags, '\\Deleted');
    }
    if ($msg->{'draft'})
    {
        push(@flags, '\\Draft');
    }
    if ($msg->{'flagged'})
    {
        push(@flags, '\\Flagged');
    }
    if ($msg->{'forwarded'})
    {
        push(@flags, '$Forwarded');
    }
    if ($msg->{'read'})
    {
        push(@flags, '\\Seen');
    }
    if ($msg->{'hasBeenRespondedTo'})
    {
        push(@flags, '\\Answered');
    }
    $flags = join(' ', @flags);
    $flags = '('.$flags.')';
    return($flags);
}

sub isCfgTrue($$)
{
    my $cfg = shift;
    my $param = shift;

    if (defined($cfg->param($param)))
    {
        my $value = $cfg->param($param);
        if (lc($value) eq 'true')
        {
            return(1);
        }
        else
        {
            return(0);
        }
    }

    return(1);
}

#############################################################################
#
# Make a SOAP request to the getMessagesInFolder API for a given folder,
# page and page size.  If successful, call processFlagsForMsg() and update
# the uidList and folderData structures and the msgCount counter.
#
#############################################################################
sub getMessagesInFolder($$$$$$$$$$)
{
    my ($self, $folderId, $folder, $page, $pageSize, $cfg, $log, $msgCount,
        $folderData, $uidList, $email) = @_;
    my $logprefix = $self->{'logprefix'};
    my ($answer, $faultData);
    my $tryCount = 0;
    my $success = 0;
    my $emptyResponse = 0;

    # if these two config options are set, try to make the code behave as if
    # the SOAP calls failed by returning a failure code.
    my $failFolder = $cfg->param('myinbox_simulate_failures_folder');
    my $failMsgs = $cfg->param('myinbox_simulate_failures_msgs');
    if (defined($failFolder) && (length($failFolder) > 0) &&
        defined($failMsgs) && (length($failMsgs) > 0))
    {
        if (($folder eq $failFolder) && !defined($pageSize))
        {
            $log->debug($logprefix.'Simulating failure for folder "'.$folder.
                        '"');
            return(0, undef)
        }

        $failMsgs =~ s/"//g;
        my @failMsgs = split(/,/, $failMsgs);
        my $start = $page * $pageSize;
        my $end = ($page + 1) * $pageSize;
        foreach my $failMsg (@failMsgs)
        {
            if (($start <= $failMsg) && ($end > $failMsg))
            {
                $log->debug($logprefix.'Simulating failure for folder "'.$folder.
                            '" and message '.$failMsg.' in range '.$start.
                            ' to '.($end-1));
                return(0, undef);
            }
        }
    }
    undef($failFolder);
    undef($failMsgs);
    my $configuredRetryCount = 3;
    if (defined($cfg->param('myi_flags_request_retry_count')))
    {
        my $cnt = $cfg->param('myi_flags_request_retry_count');
        if ($cnt =~ /^[0-9]+$/)
        {
            if ((int($cnt) > 0) && (int($cnt) < 9))
            {
                $configuredRetryCount = int($cnt);
            }
        }
    }

    # make the call to the SOAP API.  Done in a loop to allow for retries
    # of failures on the assumption they are temporary in nature.
    # If the retries are exhausted return directly rather than exiting
    # the loop
    $soap_call_name = $email.': getMessagesInFolder';
    do
    {
        $emptyResponse = 0;
        sleep(1) if ($tryCount > 0);
        $tryCount++;
        {
            local $SIG{__DIE__} = 'DEFAULT';
            $answer =
                $self->{'inboxWsdl'}{'compiled'}->on_fault(sub
                {
                    my $soap = shift;
                    my $fault = shift;
                    $faultData = $fault;
                    return(undef); # else the last variable is returned
                })->getMessagesInFolder($self->{'user'}{'userid'}, $folderId,
                                        $page, $pageSize);
        }

        # the server responded with an error, likely a HTTP error (e.g. 500)
        if ((!defined($answer)) && (defined($faultData)))
        {
            if (defined($log))
            {
                local $SIG{__DIE__} = 'DEFAULT';
                if (ref(\$faultData) eq 'SCALAR')
                {
                    eval
                    {
                        local $SIG{__DIE__} = 'DEFAULT';
                        $log->error($logprefix.'Error from SOAP interface while '.
                            'fetching mail data for folder "'.$folder.'": '.
                            $faultData);
                    }
                }
                else
                {
                    eval
                    {
                        local $SIG{__DIE__} = 'DEFAULT';
                        $log->error($logprefix.'Error message from SOAP server while '.
                            'fetching mail data for folder "'.$folder.'": '.
                            $faultData->faultstring);
                    };
                }
            }
        }
        else
        {
            if (!defined($answer))
            {
                $log->error($logprefix.'No data returned from '.
                            'getMessagesInFolder response for folder "'.$folder.
                            '" - retrying');
                $emptyResponse = 1;
            }
            elsif (isCfgTrue($cfg, 'myi_flags_require_totalMessageCount') && 
                (!exists($answer->{'totalMessageCount'})))
            {
                $log->error($logprefix.'Missing "totalMessageCount" field from '.
                            'getMessagesInFolder response for folder "'.$folder.
                            '" - retrying');
            }
            elsif (isCfgTrue($cfg, 'myi_flags_require_messages') && 
                (!exists($answer->{'messages'})))
            {
                $log->error($logprefix.'Missing "messages" field from '.
                            'getMessagesInFolder response for folder "'.$folder.
                            '" - retrying');
            }
            elsif (isCfgTrue($cfg, 'myi_flags_require_messages_array') && 
                (int($answer->{'totalMessageCount'}) > 0) && 
                (uc(ref($answer->{'messages'})) ne 'ARRAY'))
            {
                $log->error($logprefix.'"messages" field from getMessagesInFolder'.
                            ' response is not a list for folder "'.$folder.
                            '" - retrying');
            }
            else
            {
                $success = 1;
            }
        }

        if ((!$success) && ($tryCount >= $configuredRetryCount))
        {
            $soap_call_name = $email;
            return(0, undef, $emptyResponse);
        }
    } while(!$success && ($tryCount <= 3));

    my $msgsInFolder = $answer->{'totalMessageCount'};
    # for each message in the mailbox turn the true/false values returned
    # from SOAP into a string that could be used to set the correct flags
    # in IMAP
    foreach my $msg (@{$answer->{'messages'}})
    {
        my $uid = $msg->{'UId'};
        if (defined($uidList->{$uid}) && ($uidList->{$uid} > 0))
        {
            $log->error($logprefix.'UID '.$uid.' in folder "'.$folder.
                '" has been fetched multiple times from SOAP server '.
                'for folder "'.$folder.'"');
        }
        else
        {
            my $flags = processFlagsForMsg($msg);
            push(@{$folderData->{$folder}{$flags}}, $uid);
            $$msgCount += 1;
        }
        $uidList->{$uid} += 1;
    }

    $soap_call_name = $email;
    return(1, int($msgsInFolder), $emptyResponse);
}

#############################################################################
#
# The logic to walk through a folder in paged mode rather than all-at-once
# mode.  This function is recursive.  Starts with a given page size, if
# that falls backs down to a page size of 32, then if that fails backs down
# to a page size of 8, and a final size of 1.  Once enough requests at
# the smaller page size are made, it returns so that messages can be
# fetched at the next larger page size, etc, to try and keep the number
# of calls to the SOAP API to a minimum.
#
#############################################################################
sub getPagedMessagesInFolder($$$$$$$$$$$$$$)
{
    my ($self,
        $folderId,
        $folder,
        $cfg,
        $log,
        $msgCount,
        $skippedCount, 
        $emptyResponseCount,
        $folderData,
        $pageSize,
        $startPage,
        $lastPage, 
        $msgsInFolder,
        $uidList,
        $email) = @_;
    my $logprefix = $self->{'logprefix'};

    my $page = $startPage;
    my $finished = 0;
    my $extraLog = 0;
    my $extraLogCfg = $cfg->param('myinbox_flags_extra_logging');
    if (defined($extraLogCfg) && (lc($extraLogCfg) eq 'true'))
    {
        $extraLog = 1;
    }

    my $tryCount = 0;
    my $success = 0;
    my $fail = 0;

    do
    {
        my ($result, $newMsgsInFolder, $emptyResponse) =
            $self->getMessagesInFolder($folderId, $folder, $page,
                                       $pageSize, $cfg, $log, $msgCount,
                                       $folderData, $uidList, $email);

        if ($result)
        {
            if (defined($newMsgsInFolder) && ($newMsgsInFolder != $$msgsInFolder))
            {
                $$msgsInFolder = $newMsgsInFolder;
            }
            my $logmsg = $logprefix.'getMsgsFolder:"'.$folder.'", '.
                'msgsInFolder: '.$$msgsInFolder.'page='.$page.', pageSize='.$pageSize;
            $page += 1;

            $logmsg .= ', next page='.$page.', lastPage='.$lastPage.': success';
            $log->debug($logmsg);
            if ($page == $lastPage)
            {
                $finished = 1;
            }
        }
        else
        {
            $log->info($logprefix.'getMessagesInFolder("'.$folder.'" ,'.
                'page='.$page.', pageSize='.$pageSize.', lastPage='.
                $lastPage.', msgsInFolder='.$$msgsInFolder.'): FAILED');
            my $index = $pageSize * $page;
           
            if ($pageSize == 1)
            {
                $page += 1;
                $$skippedCount += 1;
                $$emptyResponseCount += $emptyResponse;
            }
            # step down to pageSize=1
            if ($pageSize == 8)
            {
                my $restoreCount1 = $pageSize * ($page + 1);
                my $newPage = int($index);
                # $log->debug($logprefix.'lastPage before getPagedMessagesInFolder(1): '.$lastPage) if ($extraLog);
                $self->getPagedMessagesInFolder($folderId,
                                                $folder,
                                                $cfg,
                                                $log,
                                                $msgCount,
                                                $skippedCount,
                                                $emptyResponseCount,
                                                $folderData,
                                                1,
                                                $newPage,
                                                $restoreCount1,
                                                $msgsInFolder,
                                                $uidList,
                                                $email);
                # $log->debug($logprefix.'lastPage after getPagedMessagesInFolder(1): '.$lastPage) if ($extraLog);
                $page += 1;
            }
            # step down to pageSize=8
            if ($pageSize == 32)
            {
                my $restoreCount8 = ($pageSize * ($page + 1)) / 8;
                my $newPage = int($index / 8);
                # $log->debug($logprefix.'lastPage before getPagedMessagesInFolder(8): '.$lastPage) if ($extraLog);
                $self->getPagedMessagesInFolder($folderId,
                                                $folder,
                                                $cfg,
                                                $log,
                                                $msgCount,
                                                $skippedCount,
                                                $emptyResponseCount,
                                                $folderData,
                                                8,
                                                $newPage,
                                                $restoreCount8,
                                                $msgsInFolder,
                                                $uidList,
                                                $email);
                # $log->debug($logprefix.'lastPage after getPagedMessagesInFolder(8): '.$lastPage) if ($extraLog);
                $page += 1;
            }
            # step down to pageSize=32
            if ($pageSize == 128)
            {
                my $restoreCount32 = ($pageSize * ($page + 1)) / 32;
                my $newPage = int($index / 32);
                # $log->debug($logprefix.'lastPage before getPagedMessagesInFolder(32): '.$lastPage) if ($extraLog);
                $self->getPagedMessagesInFolder($folderId,
                                                $folder,
                                                $cfg,
                                                $log,
                                                $msgCount,
                                                $skippedCount,
                                                $emptyResponseCount,
                                                $folderData,
                                                32,
                                                $newPage,
                                                $restoreCount32,
                                                $msgsInFolder,
                                                $uidList,
                                                $email);
                # $log->debug($logprefix.'lastPage after getPagedMessagesInFolder(32): '.$lastPage) if ($extraLog);
                $page += 1;
            }
            if ($page == $lastPage)
            {
                $finished = 1;
            }
            # $log->debug($logprefix.'emptyResponseCount='.$$emptyResponseCount.', msgCount='.$$msgCount.', pageSize='.$pageSize);
            if (($$emptyResponseCount > 100) && ($$msgCount < 1))
            {
                if ($pageSize == 1)
                {
                    $log->error($logprefix.'Marking folder failed as over 100 '.
                        'responses to single message requests have been empty, '.
                        'indicating some issue with the Java code');
                }
                return(0);
            }
            # $log->debug($logprefix.'page='.$page.', lastPage='.$lastPage) if ($extraLog);
        }
        # $log->debug($logprefix.'page='.$page.', lastpage='.$lastPage.', finished='.$finished.', index='.($page * $pageSize).', msgsInFolder='.$$msgsInFolder);
    } while(($page * $pageSize < $$msgsInFolder) && (!$finished));
    return(1);
}

#############################################################################
#
# Try to connect and log in to MX8 IMAP server
#
#############################################################################
sub connectToMx8Imap($$$$$)
{
    my $log = shift;
    my $cfg = shift;
    my $logprefix = shift;
    my $email = shift;
    my $password = shift;

    my $imap;
    my $retryCount = 0;
    my $srcImap = $cfg->param('owm_source_imap_server');
    my $imap_debug = 0;
    if (defined($cfg->param('myi_src_imap_debug')))
    {
        my $imap_debug_cfg = $cfg->param('myi_src_imap_debug');
        if (lc($imap_debug_cfg) eq 'true')
        {
            $imap_debug = 1;
        }
    }

    # try to connect to imap up to 5 times before giving up
    do
    {
        my $connected = 0;
        my $error;
        $imap = Mail::IMAPClient->new(
            Ssl      => 0,
            Uid      => 1,
            Timeout  => 60,
        );

        $imap->Server($srcImap);
        unless ($imap->connect())
        {
            $retryCount ++;
            $log->warn($logprefix.'Cannot connect to MX8 IMAP server: '.
                $imap->LastError);
            $error = $imap->LastError;
            undef($imap);
        }
        else
        {
            $connected = 1;
        }

        if ($connected)
        {
            $imap->User($email);
            $imap->Password($password);

            unless($imap->login)
            {
                $log->warn($logprefix.'Cannot log in to MX8 IMAP server: '.
                    $imap->LastError);
                $error = $imap->LastError;
                undef($imap);
            }
        }

        unless(defined($imap))
        {
            $retryCount ++;

            if ($retryCount > 5)
            {
                if (defined($log))
                {
                    if (defined($error))
                    {
                        if ($connected)
                        {
                            $log->error($logprefix.'Cannot log in to MX8 IMAP '.
                                'server and retries exhausted: '.$error);
                        }
                        else
                        {
                            $log->error($logprefix.'Cannot connect to MX8 IMAP '.
                                'server and retries exhausted: '.$error);
                        }
                    }
                    else
                    {
                        if ($connected)
                        {
                            $log->error($logprefix.'Cannot log in to MX8 IMAP '.
                                'server and retries exhausted');
                        }
                        else
                        {
                            $log->error($logprefix.'Cannot connect to MX8 IMAP '.
                                'server and retries exhausted');
                        }
                    }
                }
                return(undef);
            }
            sleep($retryCount + 1);
        }
    } while (!defined($imap));

    $imap->Debug($imap_debug);

    return($imap);
}

#############################################################################
#
# Call the MyInbox SOAP API to pull what it views the message flags to be
# by iterating over each folder and building up a hash with the appropriate
# data.  Then log in to MX8 and push the flags to IMAP so that they can
# be migrated to MX9
#
#############################################################################
sub migrateImapFlags($$$$)
{
    my ($self, $email, $password, $cfg, $log) = @_;
    my $logprefix = $self->{'logprefix'};

    my $migrateFlags = $cfg->param('myinbox_migrate_imap_flags');
    if (defined($migrateFlags) && (lc($migrateFlags) ne 'true'))
    {
        if (defined($log))
        {
            $log->info($logprefix.'Skipping flags migration as per '.
                'configuration');
        }
        return($com::owm::migTelstraEvents::STATUS_SUCCESS, 0);
    }

    # check for required SOAP connection configuration
    unless (defined($self->{'inboxWsdl'}{'path'}) &&
            defined($self->{'inboxWsdl'}{'compiled'}))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateImapFlags() without InboxService WSDL '.
                'loaded.  Cannot continue');
        }

        return($com::owm::migTelstraEvents::STATUS_FAIL_MAILBOX_MIGRATION, 0);
    }

    unless (defined($self->{'user'}) && defined($self->{'user'}{'userid'}))
    {
        if (defined($log))
        {
            $log->error($logprefix.'Call to com::owm::migTelstraInbox::'.
                'migrateImapFlags() without valid numeric user ID. '.
                'Cannot continue');
        }

        return($com::owm::migTelstraEvents::STATUS_FAIL_MAILBOX_MIGRATION, 0);
    }

    $log->info($logprefix.'Start copying message flag data from SOAP to IMAP');

    my ($answer, $faultData);
    my $tryCount = 0;
    my $success = 0;

    $soap_call_name = $email.': getFolders';
    do
    {
        sleep($tryCount);
        $tryCount++;
        {
            local $SIG{__DIE__} = 'DEFAULT';
            $answer =
                $self->{'inboxWsdl'}{'compiled'}->on_fault(sub
                {
                    my $soap = shift;
                    my $fault = shift;
                    $faultData = $fault;
                    return(undef); # else the last variable is returned
                })->getFolders($self->{'user'}{'userid'});
        }

        # the server responded with an error, likely a HTTP error (e.g. 500)
        if ((!defined($answer)) && (defined($faultData)))
        {
            if (defined($log))
            {
                if (ref(\$faultData) eq 'SCALAR')
                {
                    eval
                    {
                        local $SIG{__DIE__} = 'DEFAULT';
                        $log->error($logprefix.'Error from SOAP interface while '.
                            'fetching mail folders list: '.$faultData);
                    }
                }
                else
                {
                    eval
                    {
                        local $SIG{__DIE__} = 'DEFAULT';
                        $log->error($logprefix.'Error message from SOAP server while '.
                            'fetching mail folders list: '.$faultData->faultstring);
                    };
                }
            }

            if ($tryCount > 5)
            {
                $soap_call_name = $email;
                return($com::owm::migTelstraEvents::STATUS_FAIL_MAILBOX_MIGRATION, 0);
            }
        }
        else
        {
            $success = 1;
        }
    } while (!$success);

    # mapping of folder name to MYI folder ID number
    my %folders;
    # mapping of folder name to number of msgs in folder
    my %folderMsgCnt;
    # Hash of folder name to a hash
    # Second level hash has the flag string as it's key and an array value
    # The array is the list of UIDs with that flag string
    # The idea being that the flags could be updated on multiple messages
    # at once instead of individually.  It should also help reduce the
    # memory footprint by storing lists of messages per flag set instead
    # of a flag set per message.
    my %folderData;
    # total number of skipped emails across all folders
    my $skippedTotal = 0;

    # iterate over the folders and populate the name -> id hash
    # don't add the folder if there is no mail to be updated.
    if (ref($answer) eq 'ARRAY')
    {
        FOLDER: foreach my $folder (@{$answer})
        {
            if (!defined($folder->{'name'}))
            {
                $log->error($logprefix.'Missing "name" data for an entry in '.
                            'getFolders - skipping entry');
            }
            elsif (!defined($folder->{'messageCount'}))
            {
                $log->error($logprefix.'Missing "messageCount" data for an entry in '.
                            'getFolders - skipping entry');
            }
            elsif (!defined($folder->{'id'}))
            {
                $log->error($logprefix.'Missing "id" data for an entry in '.
                            'getFolders - skipping entry');
            }
            else
            {
                my $folderName = $folder->{'name'};
                my $msgCount = $folder->{'messageCount'};
                my $skipEmpty = 1;
                if (defined($cfg->param('myi_skip_empty_folders')))
                {
                    if (lc($cfg->param('myi_skip_empty_folders')) eq 'false')
                    {
                        $skipEmpty = 0;
                    }
                }
                if ($skipEmpty && ($msgCount < 1))
                {
                    $log->info($logprefix.'Skipping mail folder "'.$folderName.
                               '" as it has no messages in it');
                    next FOLDER;
                }
                $folders{$folderName} = $folder->{'id'};
                $folderMsgCnt{$folderName} = int($folder->{'messageCount'});
            }
        }
    }
    else
    {
        $log->error($logprefix.'No data returned from getFolders request');
    }

    # iterate over the folder name -> id hash, fetch the message data for
    # that folder from SOAP and push it into the %folderData hash
    foreach my $folder (sort(keys(%folders)))
    {
        my $msgCount = 0;
        my $skippedCount = 0;
        my $emptyResponseCount = 0;
        my %uidList;
        $tryCount = 0;
        $success = 0;

        my ($result, $msgsInFolder) =
            $self->getMessagesInFolder($folders{$folder}, $folder, undef,
                                       undef, $cfg, $log, \$msgCount,
                                       \%folderData, \%uidList, $email);
        # $log->debug($logprefix.'msgsInFolder='.$msgsInFolder);
        unless ($result) # getMessagesInFolder for entire folder failed
        {
            $log->info($logprefix.'Java exception downloading complete '.
                'message list for folder "'.$folder.'", switching to paged '.
                'mode');
            my $folderMsgCnt = $folderMsgCnt{$folder};
            $result =
                $self->getPagedMessagesInFolder($folders{$folder},
                                                $folder,
                                                $cfg,
                                                $log,
                                                \$msgCount,
                                                \$skippedCount, 
                                                \$emptyResponseCount,
                                                \%folderData,
                                                128,
                                                0,
                                                -1,
                                                \$folderMsgCnt,
                                                \%uidList,
                                                $email);
            # $log->debug($logprefix.'folderMsgCnt='.$folderMsgCnt);
            $msgsInFolder = $folderMsgCnt;
        }

        if ($result)
        {
            my $logmsg = $logprefix.'Received IMAP flag data from SOAP for '.
                    $msgCount.' messages in folder "'.$folder.'"';

            if ($skippedCount > 0)
            {
                $logmsg = $logmsg . ' with '.$skippedCount.' messages '.
                    'skipped due to SOAP API errors';
                $skippedTotal += $skippedCount;
            }

            $logmsg = $logmsg . '. totalMessageCount from getMessagesInFolder '.
                'says to expect '.$msgsInFolder . ' messages';

            $log->info($logmsg);

            if ($skippedCount + $msgCount < $folderMsgCnt{$folder})
            {
                if ($skippedCount + $msgCount < $msgsInFolder)
                {
                    my $missing = $folderMsgCnt{$folder} - $skippedCount - $msgCount;
                    $log->error($logprefix.'Messages missing in folder "'.$folder.
                        '".  Messages in folder according to "getFolders"='.
                        $folderMsgCnt{$folder}.', messages according to '.
                        'totalMessageCount from getMessagesInFolder='.$msgsInFolder.
                        ', total messages read/skipped='.($skippedCount + $msgCount).
                        ', missing='.$missing);
                    if (isCfgTrue($cfg, 'myinbox_flags_missing_msgs_fatal'))
                    {
                        $soap_call_name = $email;
                        return($com::owm::migTelstraEvents::STATUS_FAIL_MAILBOX_MIGRATION, 0);
                    }
                }
            }
        }
        else
        {
            $soap_call_name = $email;
            return($com::owm::migTelstraEvents::STATUS_FAIL_MAILBOX_MIGRATION, 0);
        }
    }

    my $retryCount = 0;
    # a counter for the number of problems so that the function can delcare
    # success or failure at the end.
    # some errors could be transient, so continue updating flags so there
    # is less work next time the user is attempted
    my $missingFolderErrors = 0;
    my $otherFolderErrors = 0;
    my $messageErrors = 0;
    my $readOnlyErrors = 0;
    do
    {
        $retryCount ++;
        $missingFolderErrors = 0;
        $otherFolderErrors = 0;
        $messageErrors = 0;
        $readOnlyErrors = 0;
        my $imap = connectToMx8Imap($log, $cfg, $logprefix, $email, $password);
        unless(defined($imap))
        {
            $soap_call_name = $email;
            return($com::owm::migTelstraEvents::STATUS_FAIL_MAILBOX_MIGRATION, 0);
        }

        FOLDER: foreach my $folder (sort(keys(%folderData)))
        {
            unless($imap->select($folder))
            {
                my $imapError = $@;
                $log->error($logprefix.'Cannot select folder "'.$folder.'": '.
                    $imapError);
                if ($imapError =~ /NO The specified mailbox does not exist/)
                {
                    $missingFolderErrors ++;
                }
                else
                {
                    $otherFolderErrors ++;
                }
                next FOLDER;
            }

            $log->info($logprefix.'Updating flags for folder "'.$folder.'"');

            foreach my $flags (keys%{$folderData{$folder}})
            {
                my @uids = @{$folderData{$folder}{$flags}};
                my @batch;
                # turn the list of UIDs that have the flags into batches
                # of no more than 10 messages at a time to be passed to IMAP.
                # Partly to ensure that the command is not too long to be
                # accepted, and partly to help isolate the cause of any failures
                # Use the Range function from Mail::IMAPClient to turn the list
                # of UIDs into IMAP ranges where possible.
                while(scalar(@uids) > 0)
                {
                    push(@batch, shift(@uids));
                    if ((scalar(@batch) >= 10) || (scalar(@uids) == 0))
                    {
                        my $batch = scalar($imap->Range(\@batch));
                        my $plural = '';
                        $plural = 's' if (scalar(@batch) > 1);
                        unless($imap->store($imap->Range(\@batch), 'FLAGS.SILENT',
                               $flags))
                        {
                            my $imapError = $@;
                            $log->error($logprefix.'Cannot store flags "'.$flags.
                                '" for '.scalar(@batch).' message'.$plural.
                                ' with UID'.$plural.' "'.$batch.'" in folder "'.
                                $folder.'": '.$imapError);
                            if ($imapError =~ /mailbox is read-only/)
                            {
                                $readOnlyErrors ++;
                                last FOLDER;
                            }
                            else
                            {
                                $messageErrors ++;                           
                            }
                        }
                        else
                        {
                            $log->info($logprefix.'Updated flags to "'.$flags.
                                '" for '.scalar(@batch).' message'.$plural.
                                ' with UID'.$plural.' "'.$batch.'" in folder "'.
                                $folder.'"');
                        }
                        undef(@batch);
                    }
                }
            }
        }

        $imap->logout();
        undef($imap);
        if ($readOnlyErrors > 0)
        {
            if ($retryCount <= 5)
            {
                sleep(30);
            }
        }
    } while (($readOnlyErrors > 0) && ($retryCount <= 5));

    $log->info($logprefix.'Finished copying message flag data from SOAP to '.
        'IMAP');

    if (isCfgTrue($cfg, 'myi_folder_read_only_fatal') && ($readOnlyErrors > 0))
    {
        $log->error($logprefix.'IMAP flags could not be updated as folder '.
            'was locked by POP client access on multiple retries');
        $soap_call_name = $email;
        return($com::owm::migTelstraEvents::STATUS_FAIL_MAILBOX_MIGRATION, 0);
    }

    # if errors have happened when writing flag data to IMAP, return
    # a failure code here so the calling function can handle
    # appropriately.
    if (isCfgTrue($cfg, 'myi_folder_no_entry_fatal') && ($missingFolderErrors > 0))
    {
        $log->error($logprefix.'One or more IMAP folders could not be selected'.
            'as it no longer exists, returning failure');
        $soap_call_name = $email;
        return($com::owm::migTelstraEvents::STATUS_FAIL_MAILBOX_MIGRATION, 0);
    }

    if (isCfgTrue($cfg, 'myi_message_no_entry_fatal') && ($messageErrors > 0))
    {
        $log->error($logprefix.'One or more messages could not have the flags'.
            'updated, returning failure');
        $soap_call_name = $email;
        return($com::owm::migTelstraEvents::STATUS_FAIL_MAILBOX_MIGRATION, 0);
    }

    if ($otherFolderErrors > 0)
    {
        $log->error($logprefix.'One or more IMAP folders could not be selected'.
            'due to an error, returning failure');
        $soap_call_name = $email;
        return($com::owm::migTelstraEvents::STATUS_FAIL_MAILBOX_MIGRATION, 0);
    }

    $soap_call_name = $email;
    return($com::owm::migTelstraEvents::STATUS_SUCCESS, $skippedTotal);
}

1;
